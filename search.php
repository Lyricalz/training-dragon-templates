<?php

//Make content type json.

header('Content-Type: application/json');

//Return all courses in an array here, simple select statement

// Mock array

$course = array(
    'Web Design',
    'Graphic Design',
    'Gaming',
    'App Development',
    'IOS',
    'Android',
    'Java',
    'Mobile',
    'Web Development',
    'Web design lorem ipsum',
    'Lorem Web Design ipsum dolor',
    'Initiaterm solos inter mos Web'
    );

    echo json_encode($course);
    
?>