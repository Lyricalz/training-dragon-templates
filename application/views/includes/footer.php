<div id="footer">
    <div id="footerBg">
        <div id="footerBg2">
            <div id="footerContent" class="container">
                <ul id="siteMap" class="visible-desktop">
                    <li>
                        <a href="/3D-courses-training" title="3D Courses Training"><span>3D Training</span></a>
                        <a href="/maya-course-training" title="Maya Training">Maya Training</a>,
                        <a href="/autocad-certification-course-training" title="AutoCad Courses Training">AutoCad</a>,
                        <a href="/3d-studio-max-course-training" title="3D Studio Max courses">3D Max</a>,...
                    </li>
                    <li>
                        <a href="/project-management-courses-training" title="Project Management Courses Training"><span>Management Courses</span></a>
                        <a href="/prince2-foundation-course-training" title="PRINCE2 Foundation training">PRINCE2 Foundation</a>,
                        <a href="/prince2-practitioner-course-training" title="PRINCE2 Practitioner training">Practitioner</a>,...
                    </li>
                    <li>
                        <a href="/programming-courses-training" title="Programming Courses"><span>Programming Courses</span></a>
                        <a href="/app-development-courses-training" title="Application development courses">App Development</a>,...
                    </li>
                    <li>


                        <a href="/web-design-courses-training" title="Web Design Courses Training"><span>Web Design Courses</span></a>
                        <a href="/asp-net-course-training" title="ASP.net Courses Training">ASP.net</a>,
                        <a href="/php-course-training" title="PHP Courses Training">PHP</a>,
                        <a href="/html5-css3-course-training" title="HTML5 courses">HTML5 Course</a>,...
                    </li>
                    <li>
                        <a href="/networking-courses-training" title="Networking Courses Training"><span>Networking Training</span></a>
                        <a href="/mcse-certification-course-training" title="MCSE certification course training">MCSE</a>,
                        <a href="/ms-exchange-server-course-training" title="Microsoft Exchange Server certification">Exchange</a>,
                        <a href="/ccna-certification-course-training" title="CCNA certification course training">CCNA</a>,
                        <a href="/red-hat-linux-rhct-rhce-course-training" title="Red Hat Linux RHCT RHCE course training">Linux</a>,...
                    </li>
                    <li>
                        <a href="/marketing-courses-training" title="Marketing Training"><span>Marketing Training</span></a>
                        <a href="/mcse-certification-course-training" title="SEO Training">SEO</a>,
                        <a href="/ms-exchange-server-course-training" title="PPC Training">PPC</a>,

                        <a href="/google-adwords-course-training" title="Google Adwords training">Adwords</a>,...
                    </li>
                    <li>
                        <a href="/graphic-design-courses-trainings" title="Graphic design Courses Training"><span>Graphic Design</span></a>
                        <a href="/photoshop-course-training" title="Adobe Photoshop course training">Photoshop</a>,
                        <a href="/illustrator-course-training" title="Illustrator Courses Training">Illustrator</a>,
                        <a href="/indesign-course-training" title="Indesign courses">Indesign</a>,...
                    </li>
                    <li>
                        <a href="/database-courses-training" title="Database Courses Training"><span>Database Training</span></a>
                        <a href="/oracle-database-courses-training" title="Oracle courses">Oracle</a>,
                        <a href="/ms-microsoft-sql-server-courses-training" title="Microsoft SQL Server courses">SQL Server</a>,
                        <a href="/java-courses-training" title="Java Web Developer training">Java</a>,...
                    </li>
                    <li>
                        <a href="/computer-it-training-courses" title="Upcoming IT Courses"><span>IT Courses</span></a>
                        <a href="/course-location" title="Training Dragon Location">Our Location</a>,
                        <a href="/jobs" title="Training Dragon Jobs and Careers">Jobs</a>,
                        <a href="/reviews" title="Review and Feedback">Reviews</a>,...
                    </li>
                </ul>
                <ul id="social" class="clearfix">
                    <li style="padding-left: 55px;"><a class="sprite-twitter-link" href="http://twitter.com/TrainingDragon" title="Twitter" rel="nofollow" target="_blank">Twitter</a></li>
                    <li><a class="sprite-facebook-link" href="http://www.facebook.com/TrainingDragon" title="Facebook" rel="nofollow" target="_blank">Facebook</a></li>
                    <li><a class="sprite-linkedin-link" href="http://www.linkedin.com/company/training-dragon" title="LinkedIn" rel="nofollow" target="_blank">LinkedIn</a></li>
                    <li><a class="sprite-gplus-link" href="http://plus.google.com/u/1/104781035220853942048/posts" title="Google Plus" rel="nofollow" target="_blank">Google+</a></li>
                </ul>
                <p class="copy">Copyright © Training Dragon, all rights reserved.
                    <br><a href="http://www.dotpeak.com" target="_blank">web design company</a> DotPeak.</p>
            </div> <!-- /footerContent -->
        </div> <!-- /footerBg -->
    </div>
</div>