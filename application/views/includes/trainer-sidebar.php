<aside id="sidebar">

    <ul id="account_courses" class="side">
        <h3 class="sideh3">Message</h3>
        <li ><a href="http://www.trainingdragon.co.uk/message/my_messages">My Messages <span style="font-size:12px; font-weight:bold;">(4 new)</span></a></li>
        <li ><a href="http://www.trainingdragon.co.uk/message/all">All Messages</a></li>
    </ul>

    <ul id="account_courses" class="side">
        <h3 class="sideh3">Courses</h3>
        <li ><a href="http://www.trainingdragon.co.uk/course/courses">Upcoming courses</a></li>
        <li class = 'selected'><a href="http://www.trainingdragon.co.uk/course/all_courses">All courses</a></li>
        <li ><a href="http://www.trainingdragon.co.uk/course/calendar">Course calendar</a></li>
        <li ><a href="http://www.trainingdragon.co.uk/course/trainer_listings">Course Email Template</a></li>
    </ul>

    <ul id="account_feedback" class="side">
        <h3 class="sideh3">Feedback</h3>
        <li ><a href="http://www.trainingdragon.co.uk/course/all_reviews">View feedback</a></li>
        <li ><a href="http://www.trainingdragon.co.uk/ratecourse/stars">I am a Star</a></li>
    </ul>

    <ul id="account_cfiles" class="side">
        <h3 class="sideh3">Course Files</h3>
        <li ><a href="http://www.trainingdragon.co.uk/file/trainer_files">View files</a></li>
        <li ><a href="http://www.trainingdragon.co.uk/file/upload/">Upload file</a></li>
    </ul>

    <!--
     <ul id="account_messages" class="side">
        <h3 class="sideh3">Messages</h3>
        <li><a href="#">View Messages</a></li>
        <li><a href="#">New Messages</a></li>
    </ul>
    -->
    <ul id="account_profile" class="side">
        <h3 class="sideh3">Profile</h3>
        <li ><a href="http://www.trainingdragon.co.uk/account/edit">Edit my profile</a></li>
    </ul> 


</aside>