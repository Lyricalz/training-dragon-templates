<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span>About Us           
</div>
<div id="text-wrap">
    <h1> About Training Dragon</h1>
    <div class="text">
        <p>What makes a great learning experience? Many say it’s down to the teachers. At Training Dragon, our experience and qualifications have enabled us to work with renowned companies like Adobe, Microsoft and Apple to bring you the most integrated, practical approaches to learning in the industry.</p>
        <p>Training dragon provide a wide variety of courses which are classroom based in a range of popular subjects such as web design, animation, marketing, management and much more.</p>
        <p>Our training centre is located in the vibrant London Kings Cross. We know our students lead busy lives so we like to make life a bit easier and add flexible course times which run during the weekdays, weekends and evenings.</p>
        <p>Gaining certifications that are official and recognised within the industry can open many doors in the working world. The courses are designed in a goal orientated approach so it is clear exactly where you are heading and how you will get there. This allows our classes to remain focus driven and deliver great courses using renowned software’s.&nbsp;</p>
        <p>To find out more information, please <a href="http://www.trainingdragon.co.uk/contact/">contact us here</a>. We look forward to hearing from you!</p>
        <br>                
    </div>
</div>

<?php if(!empty($social)) : ?>
    <div id="subfooter" class="visible-desktop clearfix">
    <span class="socialf">
        <a class="sprite-tweet addthis_button_twitter" href="http://www.addthis.com/bookmark.php" title="Twitter"></a>
        <a class="sprite-fb addthis_button_facebook" href="http://www.addthis.com/bookmark.php" title="Facebook"></a>
        <a class="sprite-gplusx addthis_button_google_plusone_share" href="http://www.addthis.com/bookmark.php" title="Google Plus"></a>
    </span>
    <span class="socialf">
        <a class="sprite-mail addthis_button_email" href="http://www.addthis.com/bookmark.php" title="Email us"></a>
    </span>
    <span class="socialf">
        <a class="sprite-print addthis_button_print" href="http://www.addthis.com/bookmark.php" title="Print this page!"></a>
    </span>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4fa9530b2e23c56a"></script>
<?php endif; ?>