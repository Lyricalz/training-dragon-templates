<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span><a href="/courses-courses-training" title="Courses">Courses</a><span class="sprite-blueArrow"></span><a href="/database-courses-training" title="Database">Database</a><span class="sprite-blueArrow"></span>Web Design (HTML &amp; CSS)            
</div>
<div id="coursesList">
    <h1>Web Design Training Courses</h1>

    <div id="courses_list_description">
        <div id="courses_list_text">
            <p>Web design is a fantastically broad field with many opportunities  such as becoming a multimedia programmer, Website Developer/Engineer,  Project Manager, Website Architect and much more.</p>
            <p>The process of designing a website normally involves several tasks  from identifying the nature of the content that will be on the website  right down to writing the programming codes to meet client&rsquo;s needs. But  of course it encompasses a lot more. You can click into the web  designers <b><u>Job role</u></b> to discover more information such as salary, daily tasks and future prospects.</p>
            <p>Some of the dominant vendors in the web design industry, of which you  may already be familiar with are: Microsoft and Adobe.<br />
                <br />
                Some of the courses that can give you a good start in web design are graphic design, web design, jQuery &amp; javascript, PHP Web programming, ASP.net with C-Sharp, WordPress and HTML5.</p>
            <ul>
                <li>You can learn how to design graphics for print and web in our graphic design course. We use software like Photoshop, InDesign and Illustrator in this course and also help you to create logo, business cards, leaflet, newsletter and website design etc</li>
                <li>Our web design course will provide you a complete insight into web design. You will learn HTML, CSS etc (required to build website) and also create a couple of website projects in classroom yourself. The course is for complete beginners and taught by a website designer.</li>
                <li>jQuery and JavaScript is required to create animation and effects on websites.</li>
                <li>The PHP and MySQL or ASP.net and Ms SQL Server courses are mainly for delegates who want to learn PHP &amp; ASP.net to create dynamic websites with features like online payment, shopping basket, membership forms, content management etc. In these courses (after learning PHP, MySQL &amp; ASP.net) you will get a chance to work on 4 practical programming projects.</li>
                <li>WordPress is one of the most famous content management platforms that will help you to maintain websites yourself or your customers. You can learn how to install, create and maintain a WordPress website on our WordPress course.</li>
                <li>If you would ever be interested in building modern websites, website for mobile phones, iPad and other tablets then consider HTML5 &amp; mobile web design courses.</li>
            </ul>
            Our trainers are professionals from industry who bring their industry experience to the classroom to make training completely hands-on and practical.<br />
            <br />
            <br />                        </div>
        <a href="javascript:;" class="read_more_js">Read More</a>

    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/web-design-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-web-design-1-437.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/web-design-course-training"><h3>Web Design (HTML & CSS) Course</h3></a>
            <p>Learn Web Design in classroom based training course in London with the help of an expert teacher. Co...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/web-design-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/html5-css3-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-html5-css3-19-905.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/html5-css3-course-training"><h3>HTML5 & CSS3 Training</h3></a>
            <p>Learn HTML5 & CSS3 in classroom based training course in London with the help of an expert teacher. ...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/html5-css3-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/mobile-web-design-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-mobile-web-design-818-162.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/mobile-web-design-course-training"><h3>Mobile Web Design Course</h3></a>
            <p>Learn Mobile Web Design in classroom based training course in London with the help of an expert teac...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/mobile-web-design-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/html-email-newsletter-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-html-email-newsletter-863-798.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/html-email-newsletter-course-training"><h3>HTML Email Newsletter Training</h3></a>
            <p>Learn HTML Email Newsletter design in classroom based training course in London with the help of an ...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/html-email-newsletter-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/jquery-javascript-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-jquery-8-692.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/jquery-javascript-course-training"><h3>jQuery & JavaScript Course</h3></a>
            <p>Learn jQuery & JavaScript in classroom based training course in London with the help of an expert te...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/jquery-javascript-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/wordpress-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-wordpress-5-360.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/wordpress-course-training"><h3>WordPress Training</h3></a>
            <p>Learn WordPress in classroom based training course in London with the help of an expert teacher. Con...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/wordpress-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/dreamweaver-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-dreamweaver-63-652.png" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/dreamweaver-course-training"><h3>Adobe Dreamweaver Course</h3></a>
            <p>Learn Adobe Dreamweaver in classroom based training course in London with the help of an expert teac...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/dreamweaver-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/jquery-mobile-development-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-jquery-mobile-development-866-501.png" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/jquery-mobile-development-course-training"><h3>jQuery Mobile Development Training</h3></a>
            <p>Learn jQuery Mobile Development in classroom based training course in London with the help of an exp...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/jquery-mobile-development-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/responsive-web-design-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-responsive-web-design-873-53.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/responsive-web-design-course-training"><h3>Responsive Web Design Course</h3></a>
            <p>Learn Responsive Web Design in classroom based training course in London with the help of an expert ...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/responsive-web-design-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/rapidweaver-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-rapidweaver-885-598.png" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/rapidweaver-course-training"><h3>RapidWeaver Training</h3></a>
            <p>Learn Realmac RapidWeaver in classroom based training course in London with the help of an expert te...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/rapidweaver-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/moodle-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-moodle-871-327.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/moodle-course-training"><h3>Moodle Course</h3></a>
            <p>Learn Moodle in classroom based training course in London with the help of an expert teacher. Contac...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/moodle-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/html-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-html-833-160.gif" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/html-course-training"><h3>HTML Training</h3></a>
            <p>Learn HTML in classroom based training course in London with the help of an expert teacher. Contact ...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/html-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/joomla-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-joomla-828-533.png" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/joomla-course-training"><h3>Joomla! Course</h3></a>
            <p>Learn Joomla in classroom based training course in London with the help of an expert teacher. Contac...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/joomla-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/mambo-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-mambo-829-484.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/mambo-course-training"><h3>Mambo Training</h3></a>
            <p>Learn Mambo in classroom based training course in London with the help of an expert teacher. Contact...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/mambo-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/oscommerce-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-oscommerce-832-814.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/oscommerce-course-training"><h3>osCommerce Course</h3></a>
            <p>Learn osCommerce in classroom based training course in London with the help of an expert teacher. Co...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/oscommerce-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/css-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-css-834-412.gif" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/css-course-training"><h3>CSS Training</h3></a>
            <p>Learn CSS in classroom based training course in London with the help of an expert teacher. Contact T...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/css-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/javascript-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-javascript-852-722.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/javascript-course-training"><h3>JavaScript Course</h3></a>
            <p>Learn avaScript in classroom based training course in London with the help of an expert JavaScript t...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/javascript-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/salesforce-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-salesforce-903-761.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/salesforce-course-training"><h3>Salesforce Training</h3></a>
            <p>Learn Salesforce in classroom based training course in London with the help of an expert teacher. Co...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/salesforce-course-training" class="read_more">More info</a>                        </div>
    </div>

    <div class="course clearfix">
        <div class="thumbnail">
            <a href="/zen-cart-course-training"><img src="http://www.trainingdragon.co.uk/content_files/image/listing_images/thumb3/courses-zen-cart-831-350.jpg" alt="" /></a>
        </div>
        <div class="course_descr col clearfix"><a href="/zen-cart-course-training"><h3>Zen Cart Course</h3></a>
            <p>Learn Zen Cart in classroom based training course in London with the help of an expert teacher. Cont...</p> 
            <p class="location">Location: <span class="course_address">King's Cross, London</span></p>
            <a href="http://www.trainingdragon.co.uk/zen-cart-course-training" class="read_more">More info</a>                        </div>
    </div>


</div>