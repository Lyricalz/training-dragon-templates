<div class="trainerWrap">
    

    <form id="content" method="post" action="/course/courses">

        <div class="courseInfo">
            <?php $this->load->view('includes/account-search'); ?>
            <!--<div class="top clearfix">
                <label>From: </label>
                <input type="text" name="datea" id="datea" value="" autocomplete = "off" />
                <label>To: </label>
                <input type="text" name="dateb" id="dateb" value="" autocomplete = "off" />
                <input type="text" name="search" id="search" value="" placeholder ="Search keyword"/>
                <input type="hidden" name="filter" value="1"/>
                <input type="submit" class="tdButton" value="Search"/>
            </div>  -->     
            <table class="managing" id="personal">
                <thead>
                    <tr>
                        <th scope="col" class="check">
                            <input id="checkAll" name="testing" type="checkbox" class="checkbox masterCheck" /><label for="checkAll"></label>
                        </th>
                        <th scope="col"><span>Course Name</span></th>
                        <th scope="col"><span>Classes</span></th>
                        <th scope="col"><span>Session</span></th>
                        <th scope="col">Seats</th>
                        <th scope="col" style="width: 70px;">Start Date</th>
                        <th scope="col">Availability</th>
                        <th scope="col">View Users</th>


                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox1" class="checkbox" /><label for="checkbox1"></label></td>
                        <td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-301)</td>
                        <td>4</td>
                        <td>10:00 - 17:00 Sun</td>
                        <td>0</td>
                        <td>07 Jul 2013</td>
                        <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1725);" style="font-size:12px"> 
                                <option value="Not_Decided">Not Decided</option>
                                <option value="Available">Available</option>
                                <option value="Not_Available" >Not Available</option>
                            </select></td>
                        <td><a href="/course/bookings/1321">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="931" name="items[]" id="checkbox2" class="checkbox" /><label for="checkbox2"></label></td>
                    <td><a href="/axure-rp-course-training">Axure RP</a>(KK-305)</td>
                    <td>2</td>
                    <td>18:30 - 21:30 Mon, Thu</td>
                    <td>1</td>
                    <td>08 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1647);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/1249">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox3" class="checkbox" /><label for="checkbox3"></label></td>
                    <td><a href="/android-programming-course-training">Android Programming</a>(KK-305)</td>
                    <td>4</td>
                    <td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
                    <td>10</td>
                    <td>08 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1724);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/1319">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="816" name="items[]" id="checkbox4" class="checkbox" /><label for="checkbox4"></label></td>
                    <td><a href="/oracle-java-se7-fundamentals-oca-certification-course-training">Java SE7 Fundamentals</a>(KK-301)</td>
                    <td>4</td>
                    <td>10:00 - 17:00 Sun</td>
                    <td>1</td>
                    <td>14 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1287);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/898">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox5" class="checkbox" /><label for="checkbox5"></label></td>
                    <td><a href="/security-certification-course-training">Security+ Certification</a>(KK-302)</td>
                    <td>5</td>
                    <td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
                    <td>1</td>
                    <td>15 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1516);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/1120">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="817" name="items[]" id="checkbox6" class="checkbox" /><label for="checkbox6"></label></td>
                    <td><a href="/oracle-java-se7-programming-ocp-certification-course-training">Java SE7 Programming</a>(KK-303)</td>
                    <td>4</td>
                    <td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
                    <td>1</td>
                    <td>29 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1290);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/901">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>
                <tr>
                    <td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox7" class="checkbox" /><label for="checkbox7"></label></td>
                    <td><a href="/advanced-php-course-training">Advanced PHP</a>(KK-303)</td>
                    <td>4</td>
                    <td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
                    <td>2</td>
                    <td>29 Jul 2013</td>
                    <td> <select name="available" class="selectBox" onchange="getStatus(this.value, 1740);" style="font-size:12px"> 
                            <option value="Not_Decided">Not Decided</option>
                            <option value="Available">Available</option>
                            <option value="Not_Available" >Not Available</option>
                        </select></td>
                    <td><a href="/course/bookings/1334">View</a></td>

                <input type="hidden" name="page" value="courses" />
                </tr>                   
                </tbody>

            </table>
        </div>
        <div class="courseInfoMob hide">
            <div id="courseSessions">
                <div class="courseSession">
                    
                </div>
            </div>
        </div>
    </form>    
    
    <?php $this->load->view('includes/trainer-sidebar.php'); ?>
    
</div>