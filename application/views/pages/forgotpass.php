<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span>Login
</div>

<div id="text-wrap" class="clearfix">


    <section class="forgotpass">
        <header>
            <h3>Login</h3>
            <p>Please enter your e-mail address and password to log in.</p>
            <div class="error">
            </div>
        </header>

        <form action="<?= base_url(); ?>account/sendlink" method="post" class="clearfix">

            <label for="email">E-mail:</label>
            <input type="email" name="email" id="email" value="" />
            
            <input type="hidden" name="forward_to" value="myaccount" />
            <input type="submit" value="Submit" class="tdButton" />
        </form>        </section>
    <section class="register hidden-phone">
        <header>
            <h3>Register</h3>
            <p>Create a new account.</p>
        </header>

        <h2>Not Registered yet?</h2>
        <a href="<?= base_url(); ?>account/" class="startHere tdButton">Start here</a>
    </section>
</div>