<div id="textHighlightWrap" class="visible-desktop">
    <h1 class="sideh3">Contact us</h1>
    <div id="textHighlight">
        <p>
            To help us to answer your questions about Training Dragon courses please complete the form below. We will respond as soon as possible.
            <br>
            Please note that fields marked with a * are compulsory.
        </p>
    </div>
</div>
<div id="contact-form-wrap" class="content">
    <h2 class="sideh3">Contact form</h2>
    <div id="contact-form">
        <form action="http://www.trainingdragon.co.uk/contact/send" method="post">
            <div style="display: none; width:300px; color:#cc0000; font-size:12px; font-weight:normal; float:left; font-weight:bold; margin:0 0 30px 200px;">
            </div>

            <label for="title">Title:</label>
            <select name="title" id="title" class="selectBox title">
                <option value="Mr." >Mr.</option>
                <option value="Ms." >Ms.</option>
                <option value="Mrs." >Mrs.</option>
            </select>
            <label for="firstName">First Name: <span>*</span></label>
            <input type="text" name="firstName" id="firstName" value="" />

            <label for="lastName">Last Name: <span>*</span></label>
            <input type="text" id="lastName" name="lastName" value="" />

            <label for="email">Email: <span>*</span></label>
            <input type="email" id="email" name="email" value="" />

            <label for="company">Company: </label>
            <input type="text" id="company" name="company" value="" />

            <label for="mobile">Mobile: <span>*</span> </label>
            <input type="tel" id="mobile" name="mobile" value="" />

            <label for="telephone">Telephone:</label>
            <input type="tel" id="tel" name="tel" value="" />

            <label for="address1">Address: </label>
            <input type="text" id="address1" name="address1" value="" />
            <input type="text" id="address2" name="address2" value="" />

            <label for="town">Town: </label>
            <input type="text" id="town" name="town" value="" />

            <label for="postcode">Postcode: </label>
            <input type="text" id="postcode" name="postcode" value="" />

            <label for="country">Country: </label>
            <select name="country" id="country" class="selectBox">
                <option value="">Select</option>
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="East Timor">East Timor</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="France, Metropolitan">France, Metropolitan</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-bissau">Guinea-bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                <option value="Korea, Republic of">Korea, Republic of</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macau">Macau</option>
                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                <option value="Moldova, Republic of">Moldova, Republic of</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">Netherlands Antilles</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russian Federation">Russian Federation</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="St. Helena">St. Helena</option>
                <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                <option value="Thailand">Thailand</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Viet Nam">Viet Nam</option>
                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Yugoslavia">Yugoslavia</option>
                <option value="Zaire">Zaire</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
            </select>


            <label for="source">How did you hear about us: </label>
            <select name="source" id="source" class="selectBox">
                <option value="Select one" selected="selected">Select one</option>
                <option value="Google" >Google</option>
                <option value="Other search engines" >Other search engines</option>
                <option value="Link from other website" >Link from other website</option>
                <option value="Word of mouth" >Word of mouth</option>
                <option value="Other" >Other</option>
            </select>

            <label for="enquiry">Enquiry type: </label>
            <select name="enquiry" id="enquiry" class="selectBox">
                <option value="">Select</option>
                <option value="Company enquiry" >Company enquiry</option>
                <option value="Personal enquiry" >Personal enquiry</option>
            </select>

            <label for="course" style="vertical-align:top;">Select Course: </label>
            <select name="course[]" id="course" multiple="multiple" class="selectBox">
                <!-- <option value="">Not related to any course</option>-->
                <option value="39">3d Studio Max</option><option value="43">A+ Certification</option><option value="67">Actionscript</option><option value="63">Adobe Dreamweaver</option><option value="883">Adobe Fireworks</option><option value="64">Adobe Flash</option><option value="68">Adobe Flex</option><option value="69">Adobe Illustrator</option><option value="66">Adobe InDesign</option><option value="65">Adobe Photoshop</option><option value="836">Adobe Premiere Pro</option><option value="886">Advanced Drupal</option><option value="891">Advanced PHP</option><option value="58">Affiliate Marketing</option><option value="823">After Effects</option><option value="87">Agile</option><option value="11">AJAX</option><option value="811">Android Programming</option><option value="4">ASP.NET</option><option value="38">AutoCAD 2D</option><option value="848">AutoCAD 3D</option><option value="931">Axure RP</option><option value="865">BlackBerry Application Development</option><option value="857">Blogging</option><option value="877">C</option><option value="878">C#</option><option value="876">C++</option><option value="47">CCNA Certification</option><option value="910">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</option><option value="48">CCNP Certification</option><option value="92">Certified Scrum Developer</option><option value="89">Certified Scrum Master</option><option value="91">Certified Scrum Product Owner</option><option value="90">Certified Scrum Professional</option><option value="850">CIW E-Commerce Specialist</option><option value="851">CIW Web Design Professional</option><option value="849">CIW Web Design Specialist</option><option value="825">CIW Web Foundations</option><option value="86">Conversion Optimisation</option><option value="858">Copywriting</option><option value="834">CSS</option><option value="71">CSS3</option><option value="82">Digital Marketing</option><option value="872">Drupal</option><option value="887">Drupal Module Programming</option><option value="888">Drupal Theme Development</option><option value="84">Email Marketing</option><option value="60">Enterprise Messaging Administrator</option><option value="912">Exam 642-813: Implementing Cisco IP Switched Networks (SWITCH)</option><option value="913">Exam 642-832: Troubleshooting and Maintaining Cisco IP Networks (TSHOOT)</option><option value="911">Exam 642-902: Implementing Cisco IP Routing (ROUTE)</option><option value="835">Exam 70-270: Installing, Configuring, and Administering Microsoft Windows XP Professional</option><option value="837">Exam 70-290: Managing and Maintaining a Microsoft Windows Server 2003 Environment</option><option value="838">Exam 70-291: Implementing, Managing, and Maintaining a Windows Server 2003 Network Infrastructure</option><option value="839">Exam 70-293: Planning and Maintaining a Microsoft Windows Server 2003 Network Infrastructure</option><option value="840">Exam 70-294: Planning, Implementing, and Maintaining a Microsoft Windows Server 2003 Active Directory Infrastructure</option><option value="841">Exam 70-298: Designing Security for a Microsoft Windows Server 2003 Network</option><option value="907">Exam 70-410: Installing and Configuring Windows Server 2012</option><option value="914">Exam 70-411: Administering Windows Server 2012</option><option value="915">Exam 70-412: Configuring Advanced Windows Server 2012 Services</option><option value="925">Exam 70-413: Designing and Implementing a Server Infrastructure</option><option value="926">Exam 70-414: Implementing an Advanced Server Infrastructure</option><option value="898">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</option><option value="899">Exam 70-486: Developing ASP.NET MVC 4 Web Applications</option><option value="900">Exam 70-487: Developing Windows Azure and Web Services</option><option value="842">Exam 70-649: Updating Your Windows Server 2003 Technology Skills to Windows Server 2008</option><option value="918">Exam 98-364: MTA Database Administration Fundamentals</option><option value="919">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</option><option value="921">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</option><option value="920">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</option><option value="917">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</option><option value="853">Facebook</option><option value="824">Final Cut Pro</option><option value="815">Game Art & Animation</option><option value="813">Game Design</option><option value="812">Game Development</option><option value="814">Game QA Tester</option><option value="72">Google Adwords</option><option value="85">Google Analytics</option><option value="855">Google Plus</option><option value="884">Google SketchUp</option><option value="2">Graphic Design</option><option value="901">Grasshopper 3d</option><option value="833">HTML</option><option value="863">HTML Email Newsletter</option><option value="70">HTML5</option><option value="19">HTML5 & CSS3</option><option value="938">Introduction to Graphic Design</option><option value="94">Introduction to Java Programming</option><option value="93">Introduction to Objective-C</option><option value="924">Introduction to Programming</option><option value="896">iOS</option><option value="846">iPad app development</option><option value="9">iPhone App Development</option><option value="868">Java EE 6 Developing Web Services Using Java Technology</option><option value="867">Java EE 6 Web Component Development with Servlets & JSPs</option><option value="816">Java SE7 Fundamentals</option><option value="817">Java SE7 Programming</option><option value="852">JavaScript</option><option value="828">Joomla!</option><option value="8">jQuery & JavaScript</option><option value="866">jQuery Mobile Development</option><option value="827">Magento</option><option value="905">Magento development</option><option value="906">Magento Theme Design</option><option value="829">Mambo</option><option value="826">Managing and Maintaining a Microsoft Windows Server 2003 Environment</option><option value="821">Maxon Cinema 4d</option><option value="51">Maya</option><option value="34">MCITP SQL Server 2008</option><option value="46">MCITP Windows</option><option value="121">MCPD SharePoint Developer</option><option value="13">MCPD Web Developer</option><option value="937">MCSA SQL Server 2012 Certification</option><option value="894">MCSA Windows Server 2012 Certification</option><option value="897">MCSD: Web Applications Certification</option><option value="889">MCSE 2003 Certification</option><option value="890">MCSE Certification</option><option value="10">MCSE Server Infrastructure 2012 Certification</option><option value="936">MCSE SQL Server 2012 Data Platform Certification</option><option value="111">MCTS Accessing Data</option><option value="916">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</option><option value="101">MCTS Silverlight Development</option><option value="7">MCTS SQL Server 2008</option><option value="14">MCTS WCFD</option><option value="16">MCTS Web Applications</option><option value="15">MCTS Windows Applications</option><option value="895">MCTS Windows Server 2008 Network Infrastructure, Configuring Certification</option><option value="73">Microsoft Excel 2010</option><option value="79">Microsoft InfoPath 2010</option><option value="859">Microsoft Office for Beginners</option><option value="37">Microsoft Office Specialist</option><option value="80">Microsoft OneNote 2010</option><option value="74">Microsoft Outlook 2010</option><option value="75">Microsoft PowerPoint 2010</option><option value="76">Microsoft Project 2010</option><option value="77">Microsoft SharePoint Workspace 2010</option><option value="78">Microsoft Visio 2010</option><option value="62">Microsoft Word 2010</option><option value="845">Microsoft Word Beginners 2010</option><option value="847">Mobile App User Interface Design</option><option value="81">Mobile Marketing</option><option value="862">Mobile Site & App Design</option><option value="860">Mobile User Experience Design</option><option value="818">Mobile Web Design</option><option value="871">Moodle</option><option value="45">Ms Exchange Server</option><option value="934">Mudbox</option><option value="104">MySQL Cluster (SQL-4302)</option><option value="100">MySQL for Beginners SQL-4401</option><option value="103">MySQL for Database Administrators (SQL-4502)</option><option value="102">MySQL for Developers (SQL-4501)</option><option value="106">MySQL High Availability (SQL-4301)</option><option value="105">MySQL Performance Tuning</option><option value="44">Network+ (N+) Certification</option><option value="31">OCA Oracle Certified Associate 11g Database</option><option value="18">OCP Java Programming</option><option value="17">OCP Java Web Developer</option><option value="32">Oracle Certified Professional (OCP) 11g Database</option><option value="96">Oracle Database 11g: Administration Workshop I DBA Release 2</option><option value="99">Oracle Database 11g: Administration Workshop II DBA Release 2</option><option value="97">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</option><option value="909">Oracle Database: SQL Certified Expert Certification</option><option value="98">Oracle Database: SQL Fundamentals I</option><option value="832">osCommerce</option><option value="874">PhoneGap</option><option value="3">PHP</option><option value="57">PPC</option><option value="42">PRINCE2</option><option value="40">PRINCE2 Foundation</option><option value="41">PRINCE2 Practitioner</option><option value="819">Pro: Designing and Developing Windows Phone Applications</option><option value="875">Python</option><option value="53">Quark XPress</option><option value="885">RapidWeaver</option><option value="59">Red Hat Linux RHCT & RHCE</option><option value="928">Red Hat System Administration I (RH124)</option><option value="929">Red Hat System Administration II (RH135)</option><option value="930">Red Hat System Administration III (RH255)</option><option value="873">Responsive Web Design</option><option value="869">Revit</option><option value="927">RHCSA Certification</option><option value="880">Rhino</option><option value="870">Ruby</option><option value="55">Ruby on Rails</option><option value="903">Salesforce</option><option value="904">Salesforce for Administrators</option><option value="88">Scrum</option><option value="843">Security+ Certification</option><option value="6">SEO</option><option value="844">Server+ Certification</option><option value="83">Social Media Marketing</option><option value="881">SolidWorks</option><option value="935">SQL for Beginners</option><option value="830">SugarCRM</option><option value="902">SugarCRM for Administrators</option><option value="61">Sun Solaris Certification</option><option value="932">Toon Boom Animate</option><option value="854">Twitter</option><option value="108">Use XML DB (Oracle Database 11g)</option><option value="879">VB.NET</option><option value="882">Vectorworks</option><option value="56">Web Accessibility & Usability</option><option value="1">Web Design (HTML & CSS)</option><option value="864">Windows Phone Application Development</option><option value="5">WordPress</option><option value="922">WordPress Plugin Development</option><option value="923">WordPress Theme Development</option><option value="107">XML Fundamentals (Oracle 11g)</option><option value="856">YouTube</option><option value="933">Zbrush</option><option value="831">Zen Cart</option>                                </select>

            <label for="message">Message: <span>*</span> </label>
            <textarea id="message" name="message"></textarea>

            <p class="form-help">
                <span class="star">*</span><span>Mandatory fields</span>
                <br/>
                Your contact details will not be passed on to any third-party organisations.
            </p>
            <input type="hidden" name="course_id" value="" />
            <input type="hidden" name="code" value="" />
            <input type="submit" name="submit" value="Send" />
        </form>
    </div>
</div>
<div id="sidebar" class="contact-sidebar">
    <div id="where-we-are-wrap">
        <h3 class="sideh3">Where we are</h3>
        <div id="where-we-are">
            <img src="<?= assets_url(); ?>img/contact-side-logo.png" alt="Contact Side Logo" height="32" width="193">
            <address>
                Kings Cross Business Centre<br>
                180-186 Kings Cross Road<br>
                London<br>
                WC1X 9DE
            </address>
            <a href="http://www.trainingdragon.co.uk/course-location"><img src="<?= assets_url(); ?>img/contact-map.png" alt=""></a>

        </div>
        <div id="get-directions" class="sideh3 hidden-phone">
            <h3>Get Directions</h3>
            <a href="http://www.trainingdragon.co.uk/course-location" class="qtooltip"><span class="sprite-contact-car"></span></a>
            <a href="http://www.trainingdragon.co.uk/course-location" class="qtooltip"><span class="sprite-contact-walk"></span></a>
        </div>
    </div>
    <div id="contact-no" class="hidden-phone">
        <span class="sprite-phone-grey"></span>020 3544 5785
    </div>
    <div id="contact-email" class="hidden-phone">
        <span class="sprite-mail-hover"></span><a href="mailto:info@trainingdragon.co.uk" class="qtooltip" oldtitle="Send us an email!">info@trainingdragon.co.uk</a>
    </div>
    <div id="jobs" class="hidden-phone">
        <div>Do you want to work with us?</div>
        <a href="http://www.trainingdragon.co.uk/jobs" class="qtooltip">Click here to find our <span>job offers</span></a>
    </div>
</div>