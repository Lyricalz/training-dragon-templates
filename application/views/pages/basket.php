<div id="basketWrap">
    <div class="colA">
        <h2>Your selected courses</h2>
        <ol>
            <div class="courseWrap" id="565">
                <a href="http://www.trainingdragon.co.uk/cart/remove/cbcb58ac2e496207586df2854b17995f" class="del_course">x</a>
                <li class="course">
                    <h3>Web Design (HTML &amp; CSS)</h3>
                    <div>Course session: 10:00 - 17:00 Mon, Tue, Wed, Thu</div>
                    <div>Starting date: 01 Jul 2013</div>
                    <div class="price">£550.00</div>
                </li>			
            </div>
            <div class="courseWrap" id="717">
                <a href="http://www.trainingdragon.co.uk/cart/remove/788d986905533aba051261497ecffcbb" class="del_course">x</a>
                <li class="course">
                    <h3>HTML5 &amp; CSS3</h3>
                    <div>Course session: 10:00 - 17:00 Mon, Tue, Wed, Thu</div>
                    <div>Starting date: 22 Jul 2013</div>
                    <div class="price">£850.00</div>
                </li>			
            </div>
            <div class="courseWrap" id="715">
                <a href="http://www.trainingdragon.co.uk/cart/remove/8df707a948fac1b4a0f97aa554886ec8" class="del_course">x</a>
                <li class="course">
                    <h3>AJAX</h3>
                    <div>Course session: 11:00 - 17:00 Wed, Thu</div>
                    <div>Starting date: 10 Jul 2013</div>
                    <div class="price">£500.00</div>
                </li>			
            </div>
            <a class="add_courses" href="/" title="Add other courses">Add other courses</a>
            <div id="finalPrice">
                <div class="priceTotal">Sub Total: <span>£1900.00</span></div>
                <div class="discountTotal">Discounted Total:<span>£1615.00</span></div>
            </div>
        </ol>
    </div>
    <!-- /colA -->
    <div class="colB">
        <h2>Please enter student details</h2>
        <form action="http://www.trainingdragon.co.uk/cart/proceed" method="post" class="clearfix">
            <label for="fname">First name:</label>
            <input type="text" name="fname" id="fname" class="textfield" value="">
            <label for="lname">Last name:</label>
            <input type="text" name="lname" id="lname" class="textfield" value="">
            <label for="email">E-mail:</label>
            <input type="text" name="email" id="email" class="textfield" value="">
            <label for="company">Company:</label>
            <input type="text" name="company" id="company" class="textfield" value="">
            <label for="address">Address:</label>
            <input type="text" name="address" id="address" class="textfield" value="">
            <label for="city">City:</label>
            <input type="text" name="city" id="city" class="textfield" value="">
            <label for="pcode">Postcode:</label>
            <input type="text" name="pcode" id="pcode" class="textfield" value="">
            <label for="phone">Telephone:</label>
            <input type="text" name="phone" id="phone" class="textfield" value="">
            <label for="mobile">Mobile:</label>
            <input type="text" name="mobile" id="mobile" class="textfield" value="">
            <label for="mobile">Who pays:</label>
            <select name="type" id="type" class="selectBox" style="display: none;">
                <option value="Individual">Myself (individual)</option>
                <option value="Company">My Company (Employer)</option>
            </select>
            <button type="submit" onclick="return validate('email');" name="submit" id="submit">Proceed to
                Checkout
            </button>
        </form> 
    </div>
    <!-- /colB -->
</div>