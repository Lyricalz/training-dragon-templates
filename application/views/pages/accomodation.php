<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span>Accommodation				
</div>
<div id="text-wrap">
    <h1> Accommodation</h1>
    <div class="text">

        <p>Effective learning is accompanied by a good night’s sleep and is highly beneficial to intellectual performance. I know we are techies but we have insiders in science and research labs. Therefore we want to make sure you have a comfortable stay when you study with Training Dragon so have suggested some great hotels near Kings Cross that might appeal to you:</p>
        <h3>Kings Cross Inn</h3>
        <p>Located within a stone throw of Kings Cross/St Pancras Rail Station, this recently refurbished hotel has immediate access to the International Eurostar service, National Rail and the London underground. Euston Railway Station is also closeby. For more details <a rel="noFollow" target="_blank" href="http://www.kingscrossinnhotel.co.uk/">click here</a>.</p>
        <h3>Comfort Inn<b><br>
            </b></h3>
        <p>is a friendly, budget hotel which is perfect for an overnight stay. Offering comfortable accommodation and just five minutes away from the station, the Inn offers you a convenient place to relax whilst you study with us. To find out more <a rel="noFollow" target="_blank" href="http://www.comfortinnkingscross.co.uk/">click here</a>.</p>
        <h3>Crestfield Hotel</h3>
        <p>Offers you value for money and convenience being less than 300 metres from London King's Cross Railway Station and St Pancras International Railway Station. The rooms are perfect for one person. For more information about prices and bookings <a rel="noFollow" target="_blank" href="http://www.crestfieldhotel.co.uk/">please click here</a><u>. </u></p>
        <h3>Alhambra Hotel</h3>
        <p>also on Argyle Street offers a stylish and affordable accommodation. The Alhambra gives you a chance to explore the area as it is 1.5 miles from Regent’s Park and London Zoo is only a 10-minute drive away. You can even take a 20 minute stroll and end up at the British Museum! If this sounds like the place for you then <a rel="noFollow" target="_blank" href="http://www.alhambrahotel.com/">click here</a>.&nbsp;</p>
        <h3>Belgrove Hotel</h3>
        <p>is a B&amp;B only a few minutes away from Kings Cross station and the Eurostar that offers fantastic rates! The Belgrove offer a 24 hour reception, payphones and a complimentary full English buffet breakfast for those with a hearty appetite. For more information <a rel="noFollow" target="_blank" href="http://www.belgrovehotel.com/">please click here</a>.</p>
        <h3>Premier Inn</h3>
        <p>Within a 20 minute walk from kings Cross station it is a great place to say not only to be close to Training Dragon but all of London’s attractions.  It has a diverse range of services to keep guests happy such as its Thyme restaurant which serves a mix of traditional and contemporary meals on top of the much loved Costa.  To find out more about rates please <a rel="noFollow" target="_blank" href="http:// http://www.premierinn.com/en/hotel/KINPTI/london-kings-cross">click here</a>. <br>
        </p>
        <h3>Travelodge</h3>
        <p>For a decent-quality but low budget stay, Travelodge is your place. Situated right in the centre of London’s main attractions, it is only a short walk from our training centre.  If you would like to find out more details <a rel="noFollow" target="_blank" href="http://www.travelodge.co.uk/hotels/258/London-Central-Kings-Cross-hotel">please click here</a><br>
            &nbsp;</p>
        <h3>Holiday Inn</h3>
        <p>From its friendly staff right down to its vibrant and stylish fitness club, Holiday Inn is a fantastic place to stay. It is only 15 minutes away from Kings Cross when walking.  Some great features of staying at the Holiday Inn include its connection to the wireless Internet throughout the hotel, it’s a short walk away from the British Museum and countless other attractions. Sound like the hotel for you? Then <a rel="noFollow" target="_blank" href="http://www.holidayinn.com/hotels/us/en/london/lonkc/hoteldetail/directions">please click here</a> for more details.<br>
            &nbsp;</p>
        <br>                </div>
</div>