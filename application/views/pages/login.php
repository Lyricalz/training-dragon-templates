<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span>Login
</div>

<div id="text-wrap" class="clearfix">


    <section class="login">
        <header>
            <h3>Login</h3>
            <p>Please enter your e-mail address and password to log in.</p>
            <div class="error">
            </div>
        </header>

        <form action="http://www.trainingdragon.co.uk/account/signin" method="post" class="clearfix">

            <label for="email">E-mail:</label>
            <input type="email" name="email" id="email" value="" autofocus required/>
            <div class="help-inline">Need an account? <a href="http://www.trainingdragon.co.uk/account/">Register here.</a></div>
            <label for="pass">Password:</label>
            <input type="password" name="password" id="pass" required/>
            <a class="forgotpass" href="http://www.trainingdragon.co.uk/account/forgetpass">Forgot your Password?</a>
            <input type="hidden" name="forward_to" value="myaccount" />
            <input type="submit" value="Log in" class="tdButton" />
        </form>        </section>
    <section class="register hidden-phone">
        <header>
            <h3>Register</h3>
            <p>Create a new account.</p>
        </header>

        <h2>Not Registered yet?</h2>
        <a href="http://www.trainingdragon.co.uk/account/" class="startHere tdButton">Start here</a>
    </section>
</div>