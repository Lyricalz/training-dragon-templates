<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span><a href="/jobs/" title="Trainingdragon home">Jobs</a><span class="sprite-blueArrow"></span>LAMP PHP & MySQL Web Developer and Programmer            
</div>
<div id="text-wrap">
    <h1>Job - LAMP PHP & MySQL Web Developer and Programmer</h1>
    <div class="text">
        Are you an expert in LAMP? Can you use your LAMP experience and teach others how to become an expert in LAMP? If your answer is yes, continue reading. We are looking for an expert in LAMP who has  excellent communication skills to provide training in LAMP.<br />
        <br />
        We welcome applications from candidates who are interested in full-time, part-time or graduate role.<br />
        <br />
        <strong>Contract type:</strong> Permanent<br />
        <br />
        <strong>Salary:</strong> Negotiable<br />
        <br />
        <strong>Candidate requirements:</strong><br />
        1. Previous experience of OOP PHP &amp; MySQL is.<br />
        2. Some previous knowledge of MVC frameworks (like CodeIgniter), AJAX/jQuery would be good to have.<br />
        3. Very good communication skills.<br />                    
    </div>


    <!--<div style="width:300px; color:#cc0000; font-size:11px; font-weight:normal; float:left; margin:30px 0 30px 200px;">
                                                                                        </div>-->


    <div class="portion" id="job-detail">
        <h2>Apply for this job</h2>
        <form method="post" action="/jobs/send" enctype="multipart/form-data" class="text clearfix">
            <label for="fname">First Name: <span>*</span></label>
            <input type="text" name="fname" id="fname" value="" />

            <label for="lname">Last Name:</label>
            <input type="text" name="lname" id="lname" />

            <label for="email">Email: <span>*</span></label>
            <input type="email" name="email" id="email" value="" />                                       

            <label for="mobile">Mobile:</label>
            <input type="tel" name="mobile" id="mobile" value="" />

            <label for="phone">Phone: <span>*</span></label>
            <input type="tel" name="phone" id="phone" />

            <label for="cv">CV:</label>
            <input type="file" name="cv" id="cv" multiple />
            <input type="hidden" name="url" value="lamp-php-mysql-web-developer-trainer" />

            <label for="keyword">Areas Of Expertise:</label>
            <input type="text" name="keyword" id="keyword" placeholder="C#, PHP, Photoshop, Maya, Oracle, HTML5" />

            <label for="keyword">Certification:</label>
            <input type="text" name="certification" id="certification" placeholder="MCSE, MCPD, OCP, CCNA, ACA, Prince2" />

            <input type="hidden" name="job_id" value="1" />

            <label for="portfolio">Portfolio:</label>
            <input type="file" name="portfolio" id="portfolio" multiple />

            <label for="source">Source:</label>
            <select name="source" id="source">
                <option value="Search engine" >Search engine</option>
                <option value="Drupal Groups" >Drupal Groups</option>
                <option value="CrunchBoard" >CrunchBoard</option>
                <option value="Gumtree" >Gumtree</option>
                <option value="Other" >Other</option>
            </select>

            <label for="cover">Covering Letter: <span>*</span></label>
            <textarea name="cover" id="cover"></textarea>
            <p class="form-help">
                <span class="star">*</span><span>Mandatory fields</span>
                <br/>
                <br/>
                Your contact details will not be passed on to any third-party organisations.
            </p>
            <input type="submit" value="Send" />



        </form> 
    </div>
</div>