<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a> <span class="sprite-blueArrow"></span> Reviews &amp; Feedback          
</div>
<h1>Reviews</h1>
<div id="sidebar">
    <h2>Some of our Clients</h2>
    <div class="intro">
        Training Dragon is proud to have successfully delivered training for some of the world's best-known brands, including
    </div>
    <ul>
        <li>Age UK</li>
        <li>AMREF UK</li>
        <li>Apoteket AB</li>
        <li>Bournemouth University</li>
        <li>BP</li>
        <li>Brunel University </li>
        <li>City University</li>
        <li>Comic Relief</li>
        <li>Harvey Nichols</li>
        <li>Hays Recruitment</li>
        <li>HMV</li>
        <li>IMG Media</li>
        <li>Kings College School</li>
        <li>Lewisham Homes</li>
        <li>Liverpool Football Club</li>
        <li>MTV</li>
        <li>NHS</li>
        <li>OPEC</li>
        <li>PA Consulting Group</li>
        <li>Pearson plc</li>
        <li>PokerStars</li>
        <li>Queen Elizabeth's Academy</li>
        <li>Ramblers</li>
        <li>Ramsay Health Care</li>
        <li>Shopzilla Europe </li>
        <li>Siemens</li>
        <li>Symbian Foundation</li>
        <li>Taylor Wessing LLP</li>
        <li>Tesco</li>
        <li>The Learning Trust</li>
        <li>UBS</li>
        <li>United Nations</li>
        <li>University of Portsmouth</li>
        <li>Wellcome Trust</li>
        <li>Worcestershire County Council</li>
        <li>Zara</li>
        <li>Zurich Financial Services</li>


    </ul>
</div>
<div id="content">
    <div class="review-intro">
        Good quality training is infectious. Once the word spreads, everyone wants a slice of the action. We have been told from past clients and a handful of renowned brands that our training courses are worth talking about. Why not carry on reading to find out why....<br>                    </div>


    <section class="advlisting">
        <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>It was nice to have a class cover so many ...</h2>
                    <span class="review-date">13 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Alyssa<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-01"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was nice to have a class cover so many programs. Just wish there was more time. Anil was a great teacher and had a lot of patience with the millions of questions daily.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Anil did great being thorough and was welcome to answer questions. Liked hearing his work experience and his advice and shortcuts. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I hope to pursue something in graphic designs after this class. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-01">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very imformative</h2>
                    <span class="review-date">13 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Jade at White Lime Creative<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-04"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very imformative, learnt lots! Alot of great information relevant to getting you started in graphic design.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Lots of experience that was shared. Communication very clear and very easy to follow and understand. Really great!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help in the setting up of my new business with my husband</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-04">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>I found the course covered exactly what I ...</h2>
                    <span class="review-date">13 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Matthew Watson, Senior Digital Cinema Engineer at Arqiva<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-06"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course covered exactly what I was looking to achieve out of it.  The content was great and the way the classes were set out make a lot of sense.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Our trainer was very knowledgeable of the subject and was able to answer all questions thrown at him through out the course.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able know now build the front ends that I have been planning to certain projects.  And hopefully expand my knowledge base further with other programming languages now that I have an understanding of PHP.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-06">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Great - very informative  ...</h2>
                    <span class="review-date">13 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Vicky Nesbitt, Software Developer at TVT Ltd<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-08"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Great - very informative and it's filled all the gaps that I wanted it to in my php knowledge and provided a structured platform of learning for me to build on.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Great - very knowledgeable, good communication skills, explained things in way that made it easy to understand.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hugely! With a bit of practice and "playing" I can utilise the skills I've learnt in my current job and it's given me another tool in my box :)</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-08">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Really good</h2>
                    <span class="review-date">13 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Anton at Havas Media<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-010"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really good, from the first aspects of PHP, to producing a functioning site was enjoyable to do and awarding. It's great to be working through real world examples, instead of having a power point overload :-)</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Really good as always.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>We are building more and more solutions using XAMPP, so this was a really important course for me to carry on working.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-010">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very nice!</h2>
                    <span class="review-date">06 Jun 2013</span></div>
                <div>
                    <div class="review-author">By Aspasia<br>on Joomla! Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-014"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very nice!!! amazing!!! learn a lot of things</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Experienced and professional</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Help me create joomla websites and manage existing ones</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-014">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Comprehensive course with the right balance ...</h2>
                    <span class="review-date">28 May 2013</span></div>
                <div>
                    <div class="review-author">By Morgane<br>on SEO Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-027"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Comprehensive course with the right balance between detailed and general principles.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer knew her subject and would help seeing the essential vs the non essential components of the course.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me with 1. my job 2. my side activity 3. my entrepreneurial project. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-027">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very clear methods that will help me achieve ...</h2>
                    <span class="review-date">28 May 2013</span></div>
                <div>
                    <div class="review-author">By Sarah at Effective Outcomes Ltd.<br>on SEO Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-028"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very clear methods that will help me achieve SEO.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Andra was very good. Clear, answered all of our questions patiently. Gave enough detail and provided notes! </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me improve the SEO of my existing sites and help me create better website designs when doing so for others.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-028">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>I have enjoyed the course very much</h2>
                    <span class="review-date">25 May 2013</span></div>
                <div>
                    <div class="review-author">By Gavin<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-033"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I have enjoyed the course very much. It was very well designed and very easy to follow. It gave me a good foundation on which I can build on with confidence.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>My trainer had very good communication skills. His instructions were very clear and the pace of the lessons was just right. He was able to give each student personal attention, and always happy to answer any questions.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge gained during this course will assist me greatly in preparing my for my PHP and Ruby On Rails courses.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-033">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>The course covered a wide amount of information. ...</h2>
                    <span class="review-date">25 May 2013</span></div>
                <div>
                    <div class="review-author">By LARISA<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-034"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course covered a wide amount of information. The information has been explained thoroughly and a lot of support has been offered. I was content with the course. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was experienced, the information given was understandable and helpful. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Potentially thinking of working in the industry of graphic or web design, also for personal future projects. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-034">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very useful course</h2>
                    <span class="review-date">23 May 2013</span></div>
                <div>
                    <div class="review-author">By Eleanor at Fusionworkshop<br>on Mobile Web Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-036"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very useful course. Will help me in my practical implementation. Many useful resources to takeaway</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good knowledge and communication</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very useful in furthering my knowledge for new projects and team training</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-036">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>The course was excellent  ...</h2>
                    <span class="review-date">17 May 2013</span></div>
                <div>
                    <div class="review-author">By Eve<br>on HTML Email Newsletter Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-037"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was excellent and more than met my expectations. It was great to spend time learning to code, hear about email best practise and find out about the best tools available.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano was very professional and extremely knowledgeable and answered my every question with ease. He had prepared everything ready and had various suggestions for helpful tools to use.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am constantly dealing with campaigns and coding emails so this course will be very handy for me in my day-to-day work life.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-037">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>As someone with some basic knowledge of both ...</h2>
                    <span class="review-date">14 May 2013</span></div>
                <div>
                    <div class="review-author">By Anton at Havas Media<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-043"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>As someone with some basic knowledge of both jQuery and JavaScript (mainly debugging) it was really good that the course covered both the first principles and the finer details of both aspects. This was really handy as I know the right way in which things should be done.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Clearly very knowledgeable and able to explain both simple and more complex problems that might arise. A clear direction in coding the right way instead of the easy way; which is good to see.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I have already sent team members on this course, this will allow myself both the ability to be extra capacity within the team and also to set a coding standard for the dev team.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-043">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>From my position of a very basic awareness/knowledge ...</h2>
                    <span class="review-date">14 May 2013</span></div>
                <div>
                    <div class="review-author">By Matt<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-044"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>From my position of a very basic awareness/knowledge of javascript, I found the course very enlightening and interesting. The course moved along at a good pace to cover a wide range of areas.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil is clearly very knowledgeable on the subject matter and made the learning fun and engaging.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge will be put to use immediately when I get back to the office as javascript is used extensively within a marketing automation tool that I use daily.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-044">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very good course</h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Roberto<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-049"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course, it's well built and planned, It helped me understing better about php</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Great trainer, friendly and most important prepared and professional</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge I have got will help me to have a better understanding about php, I don't think to be ready to define myself as a developer but at least I know much more then before :)</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-049">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>The course is very well structured  ...</h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Usman<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-050"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is very well structured and i enjoyed learning it</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Our trainer is a master in his field and he has all the knowledge to deliver in an appropriate manner.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p></p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-050">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Well designed </h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Ghiles<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-053"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Well designed , I will recomand it to anyone interested in web design </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent , very helpful , the best trainer I have never had</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Brilliant experience , I think with more practice it will be fine.
                </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-053">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Fantastic way to learn the basics of HTML  ...</h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Oscar at PaymentSense<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-055"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Fantastic way to learn the basics of HTML and CSS.
                    Really advisable for Graphic Designers interested in get new online skills.
                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano teaches in a helpful and clear way, that everybody will understand.
                    Feel confident to ask him about related subjects, he will answer you any questions.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>A good solid base of knowledge about HTML and CSS good practise, i will use in any project from now.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-055">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Very good course</h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Louisa<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-058"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course. I highly recommend this to anyone who wants to learn about HTML and CCS, who do not have any background knowledge. You will learn a lot in only four days, and as long as you keep practicing you will keep your knowledge.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was exceptionally good, very patient with the pupils and made it easy to understand complex things. He used outside examples to explain details which makes it easy to remember and the trainer was also very good at communication. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge I have gained will be my starting point. I got a lot of useful information, tips, tricks and knowledge, if I use it correctly I will be able to make good websites. With the knowledge I have gained from this course I am able to set up websites and work with them.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-058">Read more</a></div> <div class="review-wrap clearfix page1" style="display: block;"><header>
                <div><h2>Really great with loads of information to ...</h2>
                    <span class="review-date">10 May 2013</span></div>
                <div>
                    <div class="review-author">By Chris at Kcom<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-059"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really great with loads of information to improve my skills and knowledge in future projects.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Tutor was superb and very patient with all of us and our limited knowledge</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>AS an online communnications executive this will help me with the current and new websites that I will be maintaining and improving and hopefully will allow me the first steps towards my goal of wanting to become a web designer for a career choice.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-059">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Another great course here at Trainign Dragon, ...</h2>
                    <span class="review-date">02 May 2013</span></div>
                <div>
                    <div class="review-author">By Tariq Daley at Trainign Dragon<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-060"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Another great course here at Trainign Dragon, got the help that I needed to help me get through the course, and learnt some very interesting things slong the way.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very good, and experienced in the field. Any problem at all he could sort it out quickly and efficiently which made for smooth runing classses.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me gain valuable work experience for my CV and portfolio, and give me the confidence to design websites for other people using the Javascript and jQuery skills I have learnt on the course.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-060">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>For me the class was well structured  ...</h2>
                    <span class="review-date">27 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Mitesh Mistry, Graphic Designer at DoubleGold Enterprise<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-064"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>For me the class was well structured and privided the basic knowledge promised from the start.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer had really good knowledge of the working practice with industry standerds in mind. He explained the reasonings behind the do's and do not's, and was helpful with any questions asked. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Now I have a better understanding of the core elements of a website, I can use this to create simple websites for clients and now have the option to train further and be able to creative fully functional responsive websites. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-064">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>The course was extremely informative</h2>
                    <span class="review-date">27 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Zora<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-067"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was extremely informative. Really enjoyed it and learned alot</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good and efficient </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Help to design basic websites personally and within a company.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-067">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Trainer is quite knowledgable</h2>
                    <span class="review-date">27 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Darius<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-068"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Trainer is quite knowledgable, knows his stuff. Well presented</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>All very well, would recomend and look for him in my other courses</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The first step toward changing careers.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-068">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>It was extraordinary helpful to me</h2>
                    <span class="review-date">18 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Claudio, Software Engineer at eCommera <br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-072"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was extraordinary helpful to me. I learned more than expected.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shabeer is very good both as an engineer and as a teacher. He's always available and open to every request.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This course boosted my knowledge on iOS, I'll benefit from this for my personal career and my company as well.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-072">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>The course was extremely helpful  ...</h2>
                    <span class="review-date">18 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Vicky Hull, IT Applications Support Officer at Wandsworth Council<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-073"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was extremely helpful and informative. I came as a complete beginner to asp.net, and with limited experience in html/css. After the training, I feel confident about creating a basic e-commerce site, and I have picked up a lot of invaluable techniques and tips. It was great to have hands-on experience where I was given a task and had to work out solutions for myself - this has really helped to leave me feeling confident.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was a real expert. He had a great deal of real world experience, and was careful to try and give me the benefit of that. He clearly understood asp.net inside out. He was very thorough and the training was well planned. He was also patient and encouraging, which was just what I needed as a beginner!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will allow me to complete projects in my current position, and has hopefully given me a good foundation on which to begin a career in web development.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-073">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Overall, the course is designed very well</h2>
                    <span class="review-date">15 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Usman<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-074"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall, the course is designed very well. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I consider Our trainer (Mr. Emiliano) a very experienced person. He has the ability and knowledge to teach in a proper professional way. I would love to learn more from him, if i can or will get chance.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes, im pretty much happy with what i have learned in this course and it gave me the knowledge which would be beneficial in my future.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-074">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>A very informative course</h2>
                    <span class="review-date">11 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Keran<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-077"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A very informative course, progressed through the fundamentals in a short space of time. Helps a lot in laying down the foundations, as well as confidence, giving you the kick start you will need.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Anil explained everything with patience, and clear communication. He moved steadily through each stage, ensuring we understood what was being asked of us. Repeated things if we didn't. I didn't feel any hesitance to ask for help and advice, and felt liberated walking away from the class each day, because the software and techniques began to click in my mind!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me gain confidence in using Adobe software program to enhance my creative potential.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-077">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Excellent course  ...</h2>
                    <span class="review-date">11 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Peter at Deloitte<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-082"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent course </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Great trainer, clear teaching, very knowledgable</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Useful for dealing with developers</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-082">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Good overview of PHP provided ...</h2>
                    <span class="review-date">11 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Graeme at NetNames Operations Ltd<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-083"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good overview of PHP provided</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very helpful trainer. very clear explanations to questions</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will allow me to start supporting and maintaining PHP websites developed for my company</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-083">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Good structure of the course - enjoyed the ...</h2>
                    <span class="review-date">11 Apr 2013</span></div>
                <div>
                    <div class="review-author">By natalia piechowiak, business analyst at deloitte<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-084"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good structure of the course - enjoyed the project bit, where learner ends up with finished product </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good communication skills - great troubleshooting and organisation (repeating exercise when few of us didn't catch it), very clear simple language</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully enable independent modifications of existing websites</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-084">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>I went on this course in order to learn how ...</h2>
                    <span class="review-date">11 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Andrew at Lombard Vehicle Management<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-085"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I went on this course in order to learn how to make PHP website. I now feel as if I can do this.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good. Makes sure everyone is up to speed.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>My ambition is to become a web developer so this course will definitely help that.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-085">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Quiet detail</h2>
                    <span class="review-date">07 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Olawale<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-087"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Quiet detail, very practical and industry application</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Explicit and explanatory</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Industry related, hands on</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-087">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>The course was very useful to give a solid ...</h2>
                    <span class="review-date">03 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Janet Kenealy, Digital Media Manager at A4e<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-088"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very useful to give a solid general overview of the componants of wordpress, how to set up and manage a wordpress site with a enphasis on key aspects such as SEO and security.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent. Very friendly, professional, patient and approachable. Moved through material at a really good pace.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will allow me to make take more control of our company site in-house.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-088">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>The course is good</h2>
                    <span class="review-date">03 Apr 2013</span></div>
                <div>
                    <div class="review-author">By Alex, Junior Webmaster at Betfair<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-089"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is good, took me to a point with wordpress that I perhaps would not have had the patience to reach. Clearly explained.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good, has a broad knowledge, good communicator.
                </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Its made me aware of the restrictions and possibilities of using wordpress as a front end designer/ developer. I'll have too look into the limits of what can be done.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-089">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Very good course in that it covers all the ...</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By John at ONS<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-090"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course in that it covers all the right ground to build upon. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer certainly knows the subject very well and can draw from experiences and help with opinions on the future of web trends. Very easy to work with, helps and encourages. Nothing is too much trouble and I like the way he keeps us on our toes.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Our teams production is moving from AS to HTML5 and this will help the our organisation keep up with current trends and meet users needs, so will certainly help my career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-090">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Very pleased, time well spent</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Peter<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-091"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very pleased, time well spent. Never boring, fast paced &amp; challenging, well presented, thoroughly enjoyable!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Top notch. Very comprehensive knowledge, nice presentation with a sense of humour, as above: never boring.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I'll put it to immediate use to improve our company website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-091">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Very impressed-the course was well organised ...</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Zoe Hartland, Research Officer at ONS<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-092"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very impressed-the course was well organised and the level it was pitched at was about right. There was a good mix of slides, practical sessions and discussions. Although the course was fast paced and covered a lot of material I feel that I have taken it all in and have reference information to take away and refer to later. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano was very knowledgeable and helpful during the training. He was patient when explaining difficult concepts and even though there was a range of experience amongst delegates he managed this well.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I have just joined a new team in the office and attended the course to get me up to speed with the work they are doing. I think that many of the aspects taught in the course with benefit me in my career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-092">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>Really enjoyed the course</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Tom<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-093"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really enjoyed the course. Covered a lot of areas in a limited amount of time but did so in an efficient way. 
                    CSS section was extremely interesting and certainly triggered lots of plans and ideas for me!
                    I came on the course to get an understanding of the capabilities of the new standards and I would say that this aim has been achieved.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very informative, certainly knows his stuff!
                    Was very patient and approachable when I was having trouble with certain areas (java script)
                    All around nice chap!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>As a designer, it is important that I have full knowledge of the capabilities of programming languages.
                    With this course I will be able to design websites and applications to the full extent of the web, as well as offer my hand when it comes to the coding elements too.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-093">Read more</a></div> <div class="review-wrap clearfix page2" style="display: none;"><header>
                <div><h2>I am really impressed with the content given ...</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Jacobo at maku travelo ltd<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-094"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I am really impressed with the content given along this course. After the completion of this course I am more aware of the capabilities of HTML5 helping to enhance my knowledge and expertise as a front web developer.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer is very professional. He explains very clear the content making easy the understanding and acknowledge of every section. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>My knowledge in HTML and CSS have been considerably improved. I believe now that I have a wider perspective to cope with the implementation of front-ends.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-094">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Very good - covered a lot of content</h2>
                    <span class="review-date">28 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Robert, Senior Research Officer - Data Visualization Centre at Office for National Statistics<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-095"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good - covered a lot of content, brief on each bit of content but enough to go away and explore the relevant bits further.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good-obviously very knowledgable, very clear and patient!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will allow me to improve the features and styling of the content we create - better products = better reputation (!= career progression!)</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-095">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>I found the course very useful</h2>
                    <span class="review-date">23 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Larisa<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-097"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course very useful. Each class was well structured and the information given was throughly explained. I was generally very content with the course.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Well experienced and patient. The trainer has given me a wide variety of information and feedback for the future. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am a filmmaker, so learning graphic design was of great benefit for me in terms of helping me put my ideas into practice by other means than filming and editing. I am confident on making book covers, business cards and posters now.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-097">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Really enjoyed this course</h2>
                    <span class="review-date">23 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Christopher<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-098"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really enjoyed this course.  I learnt so much.  I had a lot of fun and stretch my old mind a bit too.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent.  Very funny.  Great communicator.  Very patient with the class.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I enrolled on this and the web design course as I am looking for a new career.  Both courses have given me so many ideas which I am looking to investigate in Easter.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-098">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Complete  ...</h2>
                    <span class="review-date">23 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Roberta at Perfect Management London Limited<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-099"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Complete and clear</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>He was really good and happy to help all the time</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Give me the chance to help my company with graphic stuff</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-099">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>A very useful course on PHP  ...</h2>
                    <span class="review-date">23 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Matthew<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0100"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A very useful course on PHP and I'd highly recommended it to others. The practical examples (building a CMS, shopping basket, contact form etc) really help you to understand the course content.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent, very knowledgeable.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0100">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Interesting</h2>
                    <span class="review-date">22 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Marco<br>on MCSA Windows Server 2012 Certification Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0101"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Interesting. many good features and ideas on new version of server 2012. helped understand new versions of software and networking.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good..friendly. able to answer all required questions.. well presented.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able to apply for a higher positioned job. more knowledge on new technology and features on servers. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0101">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>The course was very interesting  ...</h2>
                    <span class="review-date">19 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Natasha at National Children's Bureau<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0105"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very interesting and the tutor went at the pace of the slowest person which was very helpful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was excellent often stopping to explain in different ways for those who weren't grasping the technical terminology.  He also repeated things in breaks and went over the script to look for errors.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I now understand dynamic content such as banners and will be able to apply them and fix errors when things go wrong.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0105">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>It was very informative  ...</h2>
                    <span class="review-date">19 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Bernadette<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0107"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was very informative and well presented</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent, he was also very patient with those students who were not up to speed.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Has improved my knowledge and will enable me to improve my company's website</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0107">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>I really liked the course</h2>
                    <span class="review-date">19 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Shahzain at The Institute of Ismaili Studies<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0108"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I really liked the course. The instructor was great in keeping the pace and answering questions. Began from the very basics and took us to some interesting stuff. Really enjoyed it.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>He is a very intelligent and experience trainer. Communication is excellent. Answered all questions. Skills are good.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will enable me to create own JS scripts, and to extend the capabilities of APIs.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0108">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Again impressed by the course</h2>
                    <span class="review-date">19 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Martin Gray<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0110"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Again impressed by the course, its content and how the tutor teaches</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I can find no faults with Emiliano</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Make me study harder....</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0110">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>SEO course is well organized with lots of ...</h2>
                    <span class="review-date">13 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Nicolas<br>on SEO Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0111"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>SEO course is well organized with lots of details. I found the course very useful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I found the trainer helpful and encouraging.Very Good. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very beneficial</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0111">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Great hands on projects from an industry ...</h2>
                    <span class="review-date">09 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Andy at RDP<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0112"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Great hands on projects from an industry expert.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent trainer who mentors you through the course and gives you advice from an experienced developer view.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will have new skills to put on my CV and prepare for my Microsoft certification.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0112">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>I have got a very good impression of the ...</h2>
                    <span class="review-date">09 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Daniel<br>on AutoCAD 3D Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0113"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I have got a very good impression of the training, the place is located in central London (very easy to reach), all the facilities you need are there and it is very comfortable to learn there.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>My trainer has got more than 10 years of experience, so he knows everything about Autocad. Everything I asked he could solve fast and efficient.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will be easier to find a better job after complete the training</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0113">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Very good, very comprehensive</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Debbie Wheeler, Graphic Designer at Skanska<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0114"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good, very comprehensive. It's a difficult subject but he eased us in well and the offer of support at the end is a comfort!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced and lovely. Always happy to answer questions and tried to make things as clear as possible. He had a great technique asking different people different questions, putting everybody on the spot, trying to make us all remember what we'd learnt.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me eventually but the topic is so vast that it will take a few months/years for me to fully understand everything. Once this happens then I'm sure it will help my career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0114">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>This is a very informative course with a ...</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Ting-Kai Chang, Designer at Skanska<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0115"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This is a very informative course with a great support from basic level to semi-advanced level. Also the princle of web design/codeing process from the trainer is really following a proper professional structure, which is extremely benefitial to the course, the beginners and the industry. It suggests this structure is not only educating the skills, but also trying to raise the quality of any work in future digital world.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Really experienced and helpful</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This will be a refreshment to my ages-ago-trained-knowledge to make me reach the current standard, very helpful and correct-direction-pointing.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0115">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Course was good</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Neil<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0116"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Course was good. learned a lot about a subject I previously knew nothing about. although training was intense had the opportunity to question anything i was unsure about.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good. positive when providing feedback and clearly explained answers to problems/difficulties I had.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This course has given me an insight into a field which I knew nothing about, from this I will develop/practice such knowledge to build my skills as a web designer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0116">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Very good coverage</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Martin<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0118"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good coverage, the tutor is very well informed.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer is like wikipedia!!! He knows tons about the subject and explains everything very well.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge that I gained from this course will help me build my own websites as well as offer my skills to other people.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0118">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Good coverage  ...</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By Jamie O'Donnell, Developer at Vamasoft<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0119"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good coverage and good explanation of all topics. Theory was well explained.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very competent trainer with concise answers to every question asked.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I feel I now have the knowledge to build a website properly rather than in a way that I knew wasn't quite right.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0119">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Really good</h2>
                    <span class="review-date">08 Mar 2013</span></div>
                <div>
                    <div class="review-author">By fred Letts, Account Manager at McCann Erickson<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0120"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really good, I knew nothing about HTML or CSS before I started this course. Now I feel that I have the basic grounding in theory ans practical experience to go on and learn more for myself.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Hes a legend, great teacher, obviously knows his stuff</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>At work it will help me dealing with developers as well as allowing me to start my own ventures</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0120">Read more</a></div> <div class="review-wrap clearfix page3" style="display: none;"><header>
                <div><h2>Excellent course</h2>
                    <span class="review-date">23 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Christopher<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0121"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent course.  Very intense at times but the more you put in the more you get out of the course.  Very rewarding.  Had to push my myself at times but worth the effort.  Learnt so much.  After four Saturdays I have built my own very simple web site.  Very rewarding.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very funny and patient.  If you don't understand something he will try to explain with easy to follow diagrams.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very much so.  After 25 years writing computer programs for a travel agency I am using this course and the graphic design course next week to investigate new career ideas.  This course will act as a stepping stone into a new career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0121">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Recommended to anybody who wants to start ...</h2>
                    <span class="review-date">23 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Andy at RDP<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0122"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Recommended to anybody who wants to start in a career in web design.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very clear, accurate and very helpful throughout the course.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The course will help me in my.net development course.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0122">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Good</h2>
                    <span class="review-date">22 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Joby Brigden, Senior web designer at vielife<br>on Mobile Site &amp; App Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0127"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good. The site has given e a good starting off point to be able to think thouroughly through the needs and start planning my mobile project.

                    I feel i am in a much better starting off point now.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer had a good knowledge and understanding of the subject and was able to help with any questions</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will definitely help me as my company moves forward into offering more mobile solutions. I feel that i am now more prepared to start tackling the project and future projects</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0127">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>The course is very good</h2>
                    <span class="review-date">21 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Maris <br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0130"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is very good. Before this course I didn't know nothing about developing iPhone Apps, but at the end of course I can say, that now I understand, how Apps can be build and I can make my own App. This course is very good. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer is very experienced. I loved that he can explain all the things that at first seems very hard to understand in simple language and all is getting clear after that.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Now I can make iPhone Apps, that my company wants to develop.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0130">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>I really enjoyed the course</h2>
                    <span class="review-date">21 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Sara<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0131"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I really enjoyed the course, we got to work on a variety of applications which as quite useful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer had lots of skills in the topic and made sure everyone is happy with each topic before moving to the next one.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>If I get to work on iPhone projects very soon (before I have forgotten what I have learned here), it could help me significantly when I write an application for iPhone or iPad.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0131">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Course covers a lot of different topics</h2>
                    <span class="review-date">21 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Maksims Moisja, Programmer  at TestDevLab<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0132"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Course covers a lot of different topics. Course gives basics knowledge, so you can pick up pretty easy whichever topic you want later on. Very good overall.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer has very good experience which can be easy seen, a very good course structure prepared by trainer, very helpful, helps everyone who has a question, answers all kind of questions.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will benefit a lot, because work will be associated with building apps for iPhone and iPad.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0132">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Before the course</h2>
                    <span class="review-date">15 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Maris <br>on Introduction to Objective-C Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0135"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Before the course, I was thinking that learn Objective-C will be not so easy, cause it seem'd like very hard-learning language. 
                    But when the course started and after first day, this language seemed so easy to learn and I was happy. I liked it!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer was very knowing and very patient. If someone of us didn't understand something he always stopped and explain the situation in more details. I am very satisfied with him.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>As I am working for company, I now have the knowledge to build applications in Objective-C language. That is very big plus for me.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0135">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>The course is very well structured</h2>
                    <span class="review-date">15 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Maksims Moisja, Programmer at TestDevLab<br>on Introduction to Objective-C Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0136"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is very well structured. First I thought it will be much harder to understand, but it is quite easy. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I think the trainer has a lot of experience in this field, it's very good that we type code very nice, very helpful, answers any questions, if u don;t understand something, the trainer will try to explain to you until you get the idea. Very good in communication, casual atmosphere in classroom.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I think I will benefit a lot, since this course was chosen by my employer, and it's very good to know and learn as many things as you can while you have such opportunity</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0136">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Very well presented - Anil is extremely knowledgeable ...</h2>
                    <span class="review-date">14 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Robin Lee, Exec Director IT at PPD<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0140"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very well presented - Anil is extremely knowledgeable and very enthusiastic.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>All good, and willing to share his industry experience and real world knowledge</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Immensely!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0140">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>The course was very well run  ...</h2>
                    <span class="review-date">14 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Aleksandra<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0142"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very well run and in professional maneer.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very knowledgeable and helpful. He was very easy to follow and his communication skills were excellent.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me with my day to day work.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0142">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Very good and thorough course</h2>
                    <span class="review-date">14 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Claudia, Front-end Web Co-ordinator at Cambridge Assessment<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0143"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good and thorough course.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Quite knowledgeable trainer. Everything was clear and well explained.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Should now be able to give maintenance/support of our wordpress and FB apps that are being developed externally.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0143">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">14 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Stu at Think Jam<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0144"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good...although an extra day to delve a little bit deeper (maybe an overview of classes etc) would have made it even better.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Massively. This has been a good stepping stone between front-end and back-end programming.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0144">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Good</h2>
                    <span class="review-date">07 Feb 2013</span></div>
                <div>
                    <div class="review-author">By James Bayliss, Research Officer at Office for National Statistics<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0148"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good. Reasonably fast paced. Seemed to cover the key basics, of JQuery, new to me at start of course, but also did absolute basics of JavaScript so all were up to speed before the more complex elements.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good. Happy to come and check code problems if needed. Did flick between coding files quite quickly on occasions while students were copying coding, but have raised this as a concern. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Vastly, I hope. Having just moved into a new team, doing this course so early on, will help e get up to speed quicker with colleagues, and team/personal expectations. Would also hope to apply new knowledge to studies outside of work.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0148">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>It helped cement my current understanding ...</h2>
                    <span class="review-date">07 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Andrew<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0149"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It helped cement my current understanding of Javascript and jQuery, and gave me a basis to build on in my own studies and usage of it.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil was insghtful, entertaining, and careful to make sure everyone understood the exercises before moving on.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>- Having a better command of Javascript and jQuery to manipulate the company's website without access to the source code

                    - Using JS/jQ to build more dynamic websites in future roles and my own personal use</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0149">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Good introduction - confirmed previous experience, ...</h2>
                    <span class="review-date">07 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Robert, Senior Research Officer - Data Visualization Centre at Office for National Statistics<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0150"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good introduction - confirmed previous experience, gave a much better grounding rather than self-taught.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good communication - I personally welcomed the good fast pace it was delivered at. Didn't feel like I was wasting time.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Give a better grounding for building from scratch with Javascript - cleaner code and more efficient visualizations could be built in the future</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0150">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Covered pretty much everything I needed to ...</h2>
                    <span class="review-date">07 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Alan at Office for National Statistics<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0151"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Covered pretty much everything I needed to cover - comprehensive and </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Knowledgable, flexible and adaptable. Pleased that we will have Emil for the follow-on course (HTML5/css3)</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very relevant - hands on with the technology Monday morning - now with more confidence that I have 'good practice' and eliminated bad habits</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0151">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Very good, easy to understand</h2>
                    <span class="review-date">05 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Joe Clark, Database And Reporting Analyst at Integrated Financial Arrangements<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0152"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good, easy to understand. A lot of subjects learned.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good and very helpful. Willing to help for your own websites.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will improve my confidence in wordpress for our own company's website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0152">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Excellent</h2>
                    <span class="review-date">05 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Joe edwards, Communications Officer at NHS Protect<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0153"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent, gives a good introduction to Wordpress and enables you to get up and running with a site that includes many standard features.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent, clear, direct and helpful.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It is a starting point for me to start building sites at my work using WP along with my other knowledge such as CSS. HOpefully I can get more advanced after this start.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0153">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Not too complex</h2>
                    <span class="review-date">05 Feb 2013</span></div>
                <div>
                    <div class="review-author">By Deloris Sterling<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0154"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Not too complex. Which was perfect for me.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Nice chap. He was very happy to be flexible.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>My work uses WordPress...so this will be very useful.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0154">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>The course is well covered</h2>
                    <span class="review-date">28 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Pavel<br>on SEO Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0156"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is well covered.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was well skilled.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0156">Read more</a></div> <div class="review-wrap clearfix page4" style="display: none;"><header>
                <div><h2>Excellent</h2>
                    <span class="review-date">17 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Raymond Belone, .Net Developer<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0157"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent. I wasn't sure about what I was doing before and the instructor taught myself the proper way to go about it. Showed areas that I had yet to delve into, not relising how powerful actually ajax is. I would recommend this course to all developers that are new to or have some experience with Ajax. As this course fills in the blanks.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I cannot fault the trainer at all. Instructions are clear, precise and to the point. Will not move on until he is sure that you understand.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It has already helped, I have made changes  / improvements to my current project that I was unable to comprehend before. This will definitly help my productivity.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0157">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Very good course</h2>
                    <span class="review-date">17 Jan 2013</span></div>
                <div>
                    <div class="review-author">By David<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0158"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course. Broke down the essentials of the AJAX XMLHttpRequest object using native JavaScript, as well as via JQuery $.AJAX. The class size was very small, unlike with other training organizations, so we were able to ask and have questions answered regularly.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano the trainer was excellent. Very professional, conscientious and he always provided concise answers. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I'm hoping to be able to use AJAX more in my front-end code, rather than leave it to back-end engineers.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0158">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Despite having a years worth of teaching ...</h2>
                    <span class="review-date">15 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Raymond Belone, .Net Developer<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0159"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Despite having a years worth of teaching myself. There were still bits and pieces that I was struggling with. Since being on this course alot of subject matters that I did not know, that I did not know to know appeared, which has vastly increased my understanding.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I now have a much better idea on how to solve some current javascript / jquery issues at work. And should speed up my development work and increase / improve the look of the intranet site that I am currently working on.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0159">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>The course was very useful</h2>
                    <span class="review-date">15 Jan 2013</span></div>
                <div>
                    <div class="review-author">By ieva<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0160"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very useful. It made me realize that JavaScript and jQuery can make a huge difference in the websites appearance and functionality. I was very pleased with the course.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer seems to be a professional, easy going guy always willing to help.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I hope I will enhance the websites I'm working on a lot. The websites will be more user orientated, flexible and nice.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0160">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Very structured and interactive</h2>
                    <span class="review-date">15 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Dimitra Dimitropoulou, Junior Web Developer at A + E Networks / Sky<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0161"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very structured and interactive.

                    Good explanation of the basics but also loads of resources to create advanced material.

                    Size of group pretty good, enough time to explain things one-to-one but also move relatively fast.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good communication skills, even javascript was almost fun during this course.
                    A lot of patience but also good ability of making things move along. 
                    Loads of experience and also had personal interest in the subject, which made the class excited about the tools we were learning about.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Improve my abilities to assist my team in re-structuring our websites; as a front end developer who also works on some back end building, it is extremely useful to have a good foundation in scripting languages. </p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0161">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>It was a perfect introduction to the subject, ...</h2>
                    <span class="review-date">15 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Nigel Messenger, IT &amp; Database Manager at ICRT<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0162"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was a perfect introduction to the subject, and I found the course fun as well as useful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced, clear communication and understanding, and great disposition.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will now be able to better understand my colleague's work for support/maintenance, and start to produce my own.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0162">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Overall the course has been very useful for ...</h2>
                    <span class="review-date">11 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Lisa<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0165"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall the course has been very useful for learning how to build your own website from scratch and understanding the processes involved using html, CSS and some basic Javascript. 

                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil is a great tutor. He is very patient, helpful and encouraging. 
                </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I now have a better understanding of the whole process involved in building a website so that I can apply my knowledge when designing pages. I will also be able to design my own website for my own portfolio.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0165">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Course helps provide a good overview of how ...</h2>
                    <span class="review-date">11 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Katie O'Neill<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0167"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Course helps provide a good overview of how websites are built and how the different elements work together as layers. Also covers well the basics of HTML, CSS and illustrate JS.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil is a great teacher, very clear, good constructive advice and nice (a bit like the Michel Roux of web design!).</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I hope to use it to understand better any code changes I might need to make at work when updating our website. Also for email.

                    Thank you for a great course! :)</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0167">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Great course</h2>
                    <span class="review-date">11 Jan 2013</span></div>
                <div>
                    <div class="review-author">By Hamid<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0168"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Great course. Content was intensive, but manageable. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer (Emiliano) was great. Overall the experience was very good. His communication skills are very good. He was always attentive and responded to all queries asked of him. Professional, friendly and very knowledgeable.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This course has made web-design a little less daunting and therefore will help me improve my career prospects. I would very much like to register for another course with the trainer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0168">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2></h2>
                    <span class="review-date">23 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Simon, Graphic Designer at DCI<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0172"><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0172">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>It is a good course</h2>
                    <span class="review-date">23 Dec 2012</span></div>
                <div>
                    <div class="review-author">By susmita chatterjee<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0173"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It is a good course.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>He is good and very helpful. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p></p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0173">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>I found the course very informative</h2>
                    <span class="review-date">20 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Terry at Tinsomiac Limited<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0176"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course very informative,exciting and enjoyable, cant wait to use my new found skills in future projects.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano is a complete joy, very knowledgable and skilled and makes learning fun. I would not hestitate to recommend this course to former/furture colleague.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge gain will help be turn plain bland website that I have done in the pass in exciting, engaging sites.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0176">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>The course very good because it provides ...</h2>
                    <span class="review-date">14 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Dan Collison, Designer at Metia / Mircosoft<br>on Mobile Web Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0177"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course very good because it provides a blanket overview of the subject in question. Questions and queries that arise from outside the course content can be asked and they are quickly answered with a helpful response.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano has good communication skills, in that he makes sure that you as a student understand the course content and that the tutorial is moving at the right pace.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able to start designing and developing responsive and mobile sites with more confidence now. Also, I have picked up a number of skills and techniques that I can pass onto colleagues - this will benefit my team and help us move forward with mobile technology. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0177">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>I found the course to be very useful</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Pete Savory, Web Developer at The Virtual Forge<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0180"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course to be very useful. Prior to the course I had some basic understanding of PHP and this really helped to fill in all the missing holes as well as advance my knowledge. Though I feel the skill level of some of the other participants slowed my learning a bit and was frustrated with having to stop for others to catch up at times.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>EXCELLENT! Great communicator, clearly very knowledgable and very enthusiastic about the subject matter.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This is a great base for me to push on from and learn more to enable me to complete more complex tasks in my present company. I now feel much more confident with PHP but am very aware this is still a LOT left to learn!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0180">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Very good course</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Nathan<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0182"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course, for the starting block of php.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil, Is a very good teacher and explains things very well, and is always able to help you through something and give you a good understanding of what is going on.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me widen my skills, as a developer to find more projects and get a ob.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0182">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Thats really a good course for understanding ...</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By ozerk onder, IT Manager at Arketipo Design<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0183"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Thats really a good course for understanding logic of PHP.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p></p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0183">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Edward at Deloitte<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0185"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent, personable, adjusts the material really well, able to answer all questions, lots of real world experience.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Lots.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0185">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Brilliant course (As long as you've completed ...</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Liam at Deloitte<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0188"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Brilliant course (As long as you've completed some sort of introduction to Objective C)

                    Well organised, could even be stretched out to 5 days, or a 9am start to include more in-depth tutorials.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Legend! Explained everything well and a pleasure to spend the week with.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will be able to take on iOS projects.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0188">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Really in depth course</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Brent, Web Developer at Big Wave Media<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0189"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really in depth course, have learnt a lot more then I could of ever learnt by my self.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Professional and very well spoken, he takes the time to explain all the code and is always there to help</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be building apps for the company i work with as soon as I get home, using the skills i have learnt. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0189">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>The course was a well organised  ...</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Andrew at Deloitte<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0190"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was a well organised and enjoyable experience.   </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>This was the second course with the same trainer, and he continued to show impressive skills managing different levels of skills in the class as well as tailoring the course for our requests</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0190">Read more</a></div> <div class="review-wrap clearfix page5" style="display: none;"><header>
                <div><h2>Excellent ...</h2>
                    <span class="review-date">13 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Andy Pellew, Software Engineer at MITS<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0191"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very expericied, helpful, good at communicating.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I'll be able to do in-house App development</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0191">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Very useful</h2>
                    <span class="review-date">08 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Allaina<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0193"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very useful, I learnt the basics to help me go on and learn some more about JavaScript and Jquery.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good, he is very knowledgeable and helpful.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will now be able go create better dynamic websites and applications</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0193">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>I enjoyed the course very much  ...</h2>
                    <span class="review-date">08 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Terry at Tinsomiac Limited<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0194"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I enjoyed the course very much and up have a clearer understanding of jQuery and Javascript</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I found that the trainer was very knowledgeable about the subject, who's skills and communication was fantastic.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The additional knowledge will am sure help me in any future roles, and give me a clearer understanding. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0194">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>The course was very good</h2>
                    <span class="review-date">08 Dec 2012</span></div>
                <div>
                    <div class="review-author">By Janina, e-commerce executive at hand picked hotels<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0195"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very good, well constructed and really gave the tools to start using.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good exercises, hand outs and always answers questions so you fully understand</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>A lot i shall be able to start using in my work and develop new projects after a lot of practice!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0195">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Course is well organised</h2>
                    <span class="review-date">26 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Sam Richards, Snr Account Manager at Expedia<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0246"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Course is well organised, lots of details and some great real examples.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Great trainer, really patient especially as I was a complete beginner. Thank Emil!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Definately</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0246">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>During the course I saw everything that I ...</h2>
                    <span class="review-date">12 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Maria Isabel<br>on Maya Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0266"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>During the course I saw everything that I wanted and needed to start using this software in my projects, I think that it is complete.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I think that my trainer was perfect, he knows how to explain every question that I had during the classes, I noticed that he has experience with the software, so it was perfect for me.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I attended this course because is the area where I am going to work</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0266">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>I found the course very useful  ...</h2>
                    <span class="review-date">12 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Meera<br>on Adobe Illustrator Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0267"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course very useful and the trainer helpful and encouraging. The small class size meant that the training was keyed into our own personal projects more effectively</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very skilled and a great teacher. Easy to engage with</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Great for the CV and very useful skills to have</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0267">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>A good course to understand the principles</h2>
                    <span class="review-date">12 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Robin Lee, Exec Director IT at PPD<br>on Introduction to Objective-C Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0268"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A good course to understand the principles. Assumes quite a lot about programming knowledge in general, but with only 2 of us in the class this was fine.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shaheed was very knowledgeable and used good examples of real life work to explain examples. A nice manner and presentation plus he was patient!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Given me the fundamentals to app development</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0268">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>The course is set out in a good way</h2>
                    <span class="review-date">12 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Niles Loops, Intern at Riverrun Systems Limited<br>on Introduction to Objective-C Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0269"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course is set out in a good way, and moves at an adaptable speed. It gave me a good understanding of the basics of Objective-C.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very good at answering any queries that i had and could expand on certain things that i wanted to know more about.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge i have gained will help me work better on current freelance projects.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0269">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Good</h2>
                    <span class="review-date">05 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Vibha Agarwal, Graphic and Website Designer at Zurich Finacial<br>on Mobile Web Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0270"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good. Informative. Structured</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good. Well versed with the subject &amp; helpful.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be using it in my current job and in future</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0270">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Being the only student who enrolled on the ...</h2>
                    <span class="review-date">04 Oct 2012</span></div>
                <div>
                    <div class="review-author">By Daniele Manca<br>on Adobe Photoshop Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0271"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Being the only student who enrolled on the evening Photoshop course, for the whole duration of the course, the tasks I learned were tailored to my needs and I've learn a great deal.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Anil has been amazing and always available to go through everything I have asked him for, so everything has been great</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will benefit me greatly, because I intend to work with front end development in the future and Photoshop is an important part of it.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0271">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Excellent, well scoped and thought out</h2>
                    <span class="review-date">18 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Daryl Start, Director at SCS Ltd<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0284"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent, well scoped and thought out. All key foundation aspects of wordpress covered</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent. very patient and extremely knowledgeable
                </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It was not really a course taken to aid my career as such as I will not be doing enough web design to warrant risking it on my business. This was a personal course. however, I do now know how difficult it would be to build a functioning website so when an agency quotes me for a new site I can tell them how much I think it is worth!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0284">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>My impression is very good as I could see ...</h2>
                    <span class="review-date">13 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Antonio Alonso, .net Developer at BDA - British Dental Association<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0286"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>My impression is very good as I could see all the aspects of HTML5 and CSS3 as well as plugins and additional elements. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer is an experienced web developer and he handles perfectly the matter.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me massively as I can face the new web design requirements</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0286">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Very enjoyable</h2>
                    <span class="review-date">07 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Simon<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0287"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very enjoyable.
                    Could see the power of it straight away. Examples well thought through. Being given the files at the beginning really helped to minimise wasted time, and maximise the focus on training topics.
                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>EXCELLENT!
                    Very good communicator, took time to cover basics before moving to difficult topics.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>As a consultant my clients often have quite specific requirements. Using what I have learnt I will be able to create widgets to cover these requirements.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0287">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>The course was very intense  ...</h2>
                    <span class="review-date">06 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Stephen Ray at None<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0289"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very intense and covered various topics essential to .NET development in a commercial background. Over a 4 day period a vast amount of knowledge was gained on the ASP.NET subject.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer had excellent communication skills and made sure all delegates were comfortable with a specific subject before proceeding with the next one.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully help me gain employment in  the IT sector again after a break of approx 2 years.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0289">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Practicial delivery over theory based is ...</h2>
                    <span class="review-date">06 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Brendan<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0290"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Practicial delivery over theory based is always my preference. Days and sesions broken down well enough to remember.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent, clearly very knowledgable on all subjects and areas and comfortable and tollerent when training all skill levels.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will be used as a step ladder on to next levels.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0290">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Very insightful!</h2>
                    <span class="review-date">06 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Jessica Wearn, Digital Executive at AMS Media Group Ltd<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0291"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very insightful! This has expanded my knowledge in both ASP.NET and programming as a whole. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good experience to draw comparison between different programming languages. Good communication and answers questions well. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will use it to understand external development work better, and project manage more efficiently; as well as applying these skills practically.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0291">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Very Good</h2>
                    <span class="review-date">06 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Haden<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0292"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very Good. Up-to-date equipment and software provided. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very Good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Increased job opportunities and salary.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0292">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Really good course</h2>
                    <span class="review-date">06 Sep 2012</span></div>
                <div>
                    <div class="review-author">By Abbas Ahmed, IT at Intelok<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0293"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Really good course. Very practical and beneficial</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shahid is a top trainer. He explains everything in a clear and concise manner and I would have no hesitation in working with him again. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Absolutely - Yes</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0293">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>The course was very thorough  ...</h2>
                    <span class="review-date">25 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Emma <br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0316"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very thorough and the training materials were easy to understand and follow. It was extremely beneficial for me. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was excellent, extremely helpful and obviously very passionate and knowledgable about the subject matter. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will give me a better understanding of the possibilities of the new tags and the challenges that my development team are up against. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0316">Read more</a></div> <div class="review-wrap clearfix page6" style="display: none;"><header>
                <div><h2>Very good course</h2>
                    <span class="review-date">23 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Kenny Richards, Business Analyst at Hays<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0320"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course, decent structure to introduce relatively experienced programmers to the iOS environment and the xcode SDK.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Really nice guy. Knowledgable coder. Still relatively new to the training by the looks of it. This was a positive aspect though, as his enthusiasm and willingness to help more than made up for any lack of experience. I would recommend him to anyone in future.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Maintain my company\'s current iOS app, making code tweaks and content updates, rereleasing on the App Store etc.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0320">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>It\'s quite ok</h2>
                    <span class="review-date">23 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Slawek Trybus, developer at Roche<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0321"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It\'s quite ok. The course program is adapted to dedicated time frame. Most important things are covered. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Energetic, experienced and ready to help. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The new knowledge will help me to become an iOS developer, what\'s my new duty.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0321">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>I found this course extremely helpful</h2>
                    <span class="review-date">17 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Kristelle Kamini, Service Development Specialist  at Hackney Learning Trust<br>on Adobe InDesign Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0322"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found this course extremely helpful, best course I have been on so far. It was very practical which I really enjoyed. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Anil was a very approachable, understanding trainer. He explained everything I needed to do in detail. He answered my questions and is highly skilled at what he does.</p><h3 class="review-impression">What do you think can be improved (facilities, teaching, course contents)?</h3><p>Nothing, I was happy with everything. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am now confident I can go back into work and start creating using InDesign. In fact, I can\'t wait to get stuck in again. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0322">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>As a total beginner to JavaScript I found ...</h2>
                    <span class="review-date">08 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Andrew<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0323"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>As a total beginner to JavaScript I found this to be the perfect course. It taught all the basics first and slowly built on them until we were able to perform complicated examples on our own.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>You could not ask for a better trainer. His experience not only with coding but with teaching was clear to see. He always made sure everyone understood each topic before we moved onto the next one.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Although the course (coding) does not correlate to my career it is a handy skill to know. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0323">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Overall I found the sessions very helpful</h2>
                    <span class="review-date">08 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Melanie<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0324"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall I found the sessions very helpful. The content was clearly laid out with useful analogies and examples.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil was very helpful and happy to stop and go over things if we were unsure. He seemed very knowledgeable and made the sessions interesting.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I need to use jQuery on a daily basis in my role. This course has given me a thorough grounding to base my work on.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0324">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Before starting this course I felt that PHP ...</h2>
                    <span class="review-date">05 Aug 2012</span></div>
                <div>
                    <div class="review-author">By Yalcin<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0326"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Before starting this course I felt that PHP was this beast of a product. However I now feel like I have the knowledge to understand that beast. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emil has created a relaxed atmosphere. I felt that I could ask him for help when I was stuck on an issue.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me hire great stuff because I think want to create a team of \'A\' players. This will help me to understand which developers are the good ones :)</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0326">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>It is a very intense, hands on course</h2>
                    <span class="review-date">28 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Anna<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0329"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It is a very intense, hands on course. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I like Emiliano\'s teaching style. He was always well prepared to the lessons and knew how to get the students involved, made it interactive.\n</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will benefit me greatly. I am going to use the content of this course to gain the skills taht will help me to start a new career as a graphic designer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0329">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>I have learned a insite about the Dreamweaver ...</h2>
                    <span class="review-date">28 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Sameer<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0331"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I have learned a insite about the Dreamweaver which will be helpful for me while I learn more about it online.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano is experienced instructor who is willing to help whihc any queries. I had received a reply email even 10 PM whihc I was struggling.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Well I want to become a IPhone App Developer and when I contacted Dotpeak I was told to start from basic like Dreamwever so that I will have some knowledge of hand coding. In nutshell this course has given me a enough insight of coding which I have start getting confident.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0331">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Good ...</h2>
                    <span class="review-date">28 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Guynamio<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0332"><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I have to keep practicing what i learned. it is very good</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0332">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Well presented  ...</h2>
                    <span class="review-date">28 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Andrew<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0333"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Well presented and covered all the content</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good communication and presented all the information well</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes absolutely </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0333">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Good practical knowledge  ...</h2>
                    <span class="review-date">28 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Dhanvin N<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0334"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good practical knowledge and practice available.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Will say overall good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Got confidence and even more knowledge. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0334">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Very useful and interesting  course!</h2>
                    <span class="review-date">26 Jul 2012</span></div>
                <div>
                    <div class="review-author">By N Miri, VLE Web Technician at Queen Elizabeth's Academy<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0339"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very useful and interesting  course! it will teach you about the latest and most up to date web Technic!(HTML5 CSS3 and JavaScript.)  </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano is an excellent tutor with great knowledge.he is very patient!!!!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>As a web designer we need to make sure we are always up to date with the new staff. </p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0339">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Very informative, a lot of good content</h2>
                    <span class="review-date">26 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Dominic<br>on HTML5 &amp; CSS3 Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0341"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very informative, a lot of good content. Well explained. Friendly. Highly educational.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Brilliant, clearly well experienced, answered all questions with good clear explanation(s).</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hugely, it filled a lot of gaps I wanted to fill for a long time. It was great to have someone to ask specifically rather than looking online or watching hours of video</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0341">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>This was a wow course - an eye opener to ...</h2>
                    <span class="review-date">17 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Gloria<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0344"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This was a wow course - an eye opener to the many functionalities of word press</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent - well resourced - extremely useful links and patient</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This will help in designing websites for charities</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0344">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Trainers have good knowledge  ...</h2>
                    <span class="review-date">17 Jul 2012</span></div>
                <div>
                    <div class="review-author">By Ciske<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0346"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Trainers have good knowledge and overall the course follows a good structure and pace. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good communication, very knowledgeable and not scared to share tips and hints within the classroom. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Definitely going away and having a good go at creating a WordPress site or two.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0346">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>It has given me a better understanding about ...</h2>
                    <span class="review-date">21 Jun 2012</span></div>
                <div>
                    <div class="review-author">By Antonio Silva, Web Designer at T1 communications<br>on Java SE7 Fundamentals Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0355"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It has given me a better understanding about JAVA and what goes on in regards to OCP Certification so I have a better idea about what I would need to do in OCA exam</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very knowledgeable, and could answer all questions. He clearly has a lot of experience in Java. Plus he had good humour (!!) and I felt comfortable to ask any queries </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will give me a very solid base in Java to build my new career as a java developer. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0355">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Very useful and interesting course!</h2>
                    <span class="review-date">21 Jun 2012</span></div>
                <div>
                    <div class="review-author">By Cathryn maddison, Developer at Freelancer<br>on Java SE7 Fundamentals Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0356"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very useful and interesting course! I didn\\\'t only prepared for OCA Java certification but also got a practical understanding of many concepts. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>John made learning simple and easy with good communication and was also approachable with queries. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am a PHP developer and this was my second course with Training Dragon. My next job required me to have Java experience so this course have given me a kick start in Java. I am looking forward to OCP Java course next year. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0356">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>This course is excellent for those who want ...</h2>
                    <span class="review-date">10 Jun 2012</span></div>
                <div>
                    <div class="review-author">By Mohammed<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0372"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This course is excellent for those who want to learn how to create websites with databases. During this course I became more confident in developing databases websites which helped me achieving my targets.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shesh is an outstanding trainer, he helped us to learn everything about ASP.Net in just 4 days! He has covered all the training objectives and I had the chance to ask additional questions about other things that are not included in the course. Shesh is very professional and experienced trainer and I will defiantly keep in touch with him in the feature.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am an ICT teacher and I am planning to teach my pupils how to create websites with database. I will try to teach all the stuff that I have learnt during the course.</p><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0372">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>I was really impressed with the course as ...</h2>
                    <span class="review-date">22 May 2012</span></div>
                <div>
                    <div class="review-author">By Jermaine <br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0393"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I was really impressed with the course as I learnt things I didnt know before and it would help my development in the future.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very professional, makes everything understandable and goes through the process at a good speed.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It help create and manage websites much easier, also its a aspects that I can incorpate in my business elements.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0393">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>Easy and fast learning process</h2>
                    <span class="review-date">22 May 2012</span></div>
                <div>
                    <div class="review-author">By lauro<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0397"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Easy and fast learning process..:)</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Skillful.. great person n teacher..</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>In so many ways.. as i want to become  a professional web designer..</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0397">Read more</a></div> <div class="review-wrap clearfix page7" style="display: none;"><header>
                <div><h2>That wordpress is very simple anbd user  ...</h2>
                    <span class="review-date">22 May 2012</span></div>
                <div>
                    <div class="review-author">By Romaine<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0398"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>That wordpress is very simple anbd user firendly</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Was very comfortable with the trainer and trusted that he knew what he ws talking about</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will enable me to build websites for people and make money from it. Has also enabled me to create personal websites that I have had in mind for a while</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0398">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Course was great</h2>
                    <span class="review-date">08 Jan 2012</span></div>
                <div>
                    <div class="review-author">By ANDERSON<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0508"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Course was great, lots of information, lots of tips given by lecturer.  I feel that I have learnt a great deal and now I need to practise, practise and practise.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano is great, an expert with lots of patience. He makes sure you understand it and doesn't mind repeating it as many times as needed, extremely patient with all my predictable questions too.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Emiliano has given  us lots of tips, books and future prospects. I feel have a long way to go but the course has made me feel very positive and enthusiastic about it.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0508">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Was very good course will definitely look ...</h2>
                    <span class="review-date">08 Jan 2012</span></div>
                <div>
                    <div class="review-author">By Steve<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0509"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Was very good course will definitely look to take further training with Dot Peak.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer was very good at expaining the course work.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Skills learnt will be very useful for project work currently being undertaken</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0509">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Lots of information  ...</h2>
                    <span class="review-date">08 Jan 2012</span></div>
                <div>
                    <div class="review-author">By Louise<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0510"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Lots of information and new skills taught for a good price. value for money.  It was good to be able to carry out projects as they course went along rather than just taking notes or watching. very kinesthetic which is excellent. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent communication, knowledge and willingness to help even outside of the course time via email. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will be extreemly beneficial as I am hoping to set up my own business, I can now confidently design my website and not have to pay other people to do it for me. </p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0510">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>The course was really helpful</h2>
                    <span class="review-date">08 Jan 2012</span></div>
                <div>
                    <div class="review-author">By mohamad<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0511"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was really helpful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emilano was really helpful and patient. very clear. and he knew what to give us</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Emilano gave us enough knowledge about the subject&lt; but we need to go further, and implement what we did to improve.. but im so happy with what we get from this course</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0511">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>The Graphic design course was great</h2>
                    <span class="review-date">07 Jan 2012</span></div>
                <div>
                    <div class="review-author">By ANDERSON<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0512"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The Graphic design course was great.
                    I have learnt lots of new things, the course was delivered by Michael and he is simply great</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Michael is great,extremely patient and eager to give as much as he can. I will be eternally greatful. </p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0512">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">22 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Owen<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0517"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced and presents in a useful, relateable way.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will be very useful in understanding the JQuery aspects of our system.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0517">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>The course overall was good</h2>
                    <span class="review-date">22 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Bilaal <br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0518"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course overall was good. I have been given an insight into SQL and ASP.NET using C#. I have understood the fundamentals of ASP.NET and gained a better understanding of the .NET framework and other technologies. 

                    What I expected showed an end to end process.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was good, explained the different definitions and explained with both examples and diagrams. Took the time to explain errors and used good senarios. Showed an end to end process with accurate descriptions. 

                    Knowledge of ASP.NET and SQL was great.  </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledged gained will allow me to atleast attempt to create my own ASP.NET site connecting to a SQL backend. 

                    Also will help me to provide support when incidents are raised for application that I currently support at work.

                    This has given me a fundamental understanding of the technology. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/3_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0518">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Good introduction level content to JavaScript ...</h2>
                    <span class="review-date">20 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Russell<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0520"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good introduction level content to JavaScript and some JQuery. The days could have been longer in order to fit more in.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very knowledgable and friendly</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Base level understanding of what can be achieved through JavaScript and JQuery which I can build on going forwards with self study</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0520">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>A fast dash through the basics of javascript ...</h2>
                    <span class="review-date">20 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Owen<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0521"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A fast dash through the basics of javascript and fun of JQuery. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It'll help me understand the javascript/JQuery we have better, read it faster and offer better options to our users.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0521">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>It was good</h2>
                    <span class="review-date">20 Dec 2011</span></div>
                <div>
                    <div class="review-author">By David<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0522"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was good. Seemed fairly thorough and Emiliano was helpful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good. Nice guy and lots of patience.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Help me better critique work that is put in front of me and suggest fixes.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0522">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Overall impressions
                        I have learnt a lot  ...</h2>
                    <span class="review-date">08 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Crosley<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0527"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall impressions
                    I have learnt a lot and I like the fact that the tutor helps you
                    when you make mistakes with your syntax.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good, examples were detailed. Communications was excellent. And I have definately learnt something new.
                    Much better that reading a book.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am not going to use the skills straight away but the couse will help me to understand how to support existing Website in the Company and help me to write my own.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0527">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Good</h2>
                    <span class="review-date">08 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Adrian<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0528"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent - without question.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Has given insight &amp; direction.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0528">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Lots of information in a short amount of ...</h2>
                    <span class="review-date">08 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Emma<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0529"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Lots of information in a short amount of time.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent. Good at explaining and constantly checking everyone understood.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Has taught me hopefully enough to use in the future</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0529">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>It was a little difficult to move from the ...</h2>
                    <span class="review-date">01 Dec 2011</span></div>
                <div>
                    <div class="review-author">By Clive<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0536"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was a little difficult to move from the previous JAVASCRIPT course to AJAX as the foucs is very different.  None the less the Teacher did an excellent job in steering me on how to understand and use AJAX in web development and it role in bootstrapping Javascript.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The Trainer was superb at getting across the main and important details of AJAX.  Both his communication and presentation skills were top notch.  It was a pleasure to develop my skills in such an interactive and collaborative way.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Already, I have something in the work that needs AJAX and Javascript so after doing both course, I am not only primed to do this web project but highly motivated.  All this is a great thanks to Dotpeak and my excellent trainer. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0536">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>This course was perfect for my level of programming, ...</h2>
                    <span class="review-date">29 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Mark<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0537"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This course was perfect for my level of programming, and has set me up for a future within this industry! It was clear to me that Dotpeak prides its self on the fact that you are passing on this knowledge. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer 'Ciprian' was nothing but excellent. He show a huge experience within this subject and i felt that any queries i had were more than answer, they were explained. He is a very good teacher, shown with his communication and expertise. I hope to work with him in the future.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I now have a path for my life and carer. Thank you. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0537">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>The course was excellent-covered a lot ...</h2>
                    <span class="review-date">29 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Yasmin<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0539"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was excellent-covered a lot</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was excellent-very knowledgable and interested. Great communication</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will benefit my hugely</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0539">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>Very good introduction to php</h2>
                    <span class="review-date">29 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Jolyon Hedges, IT Manager<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0540"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good introduction to php, cover all I expected.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced knew all the problems and clearly explained and communicated knowledge</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Not looking for it for my career but for personal website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0540">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>An excellent introduction to the world of ...</h2>
                    <span class="review-date">27 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Julian<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0542"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>An excellent introduction to the world of web design.  It gave a good overall knowledge of the building blocks you need to start building your own web projects.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was excellent, very professional and explained things in a structured and easy to follow way.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It's a good starting point to move into web design, away from the more traditional programming that I do now.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0542">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>It was a good way to approach the web design ...</h2>
                    <span class="review-date">27 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Monika<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0544"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was a good way to approach the web design basics. I really appreciated the fact that we could learn so much in so little time. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very professional, experienced and straightforward.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I do not know yet.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0544">Read more</a></div> <div class="review-wrap clearfix page8" style="display: none;"><header>
                <div><h2>The course has given me a much better understanding ...</h2>
                    <span class="review-date">27 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Mathew<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0545"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course has given me a much better understanding of html and css. This will help me in the future as I play around with more web based projuects.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I thought the trainer was very skilled, he knows his stuff and how to make a website in an effective way. But sometimes I feel as if he went a little too fast.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able to work with the web designers at work now with the knowledge that I have been given. This will be good for me as I can gain experience at work with desiging for web and web design as well.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0545">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Clear  ...</h2>
                    <span class="review-date">26 Nov 2011</span></div>
                <div>
                    <div class="review-author">By SIMON<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0547"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Clear and concise introduction to PHP programming. Lots of useful examples. Has been really interesting. I feel now I can become a php developer</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very helpful, at each stage will always check that everyone is ok. Everytime I had a problem, Ciprian would show me the answer. He always knows how to solve the problem, so that I can see it working correctly on my screen.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Now I can develop in my company and move from designer to developer</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0547">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Good course, well delivered by the trainer</h2>
                    <span class="review-date">26 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Rob<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0548"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good course, well delivered by the trainer.  Went away with a lot of useful scripts that will help me get started with some real projects</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Ciprian was an engaging trainer.  I enjoyed the days studies and the worked examples.
                </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Broadens my experience and gives me confidence to do some study myself.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0548">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>The course was well delivered</h2>
                    <span class="review-date">26 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Jason<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0549"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was well delivered. Kept me interested.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>He's very good, enthusiastic and willing to help and answer any question.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Help me to do more backend work.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0549">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Good explanation of the subject matter</h2>
                    <span class="review-date">26 Nov 2011</span></div>
                <div>
                    <div class="review-author">By To Coates, Contracts Development Executive at Advance Catering<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0550"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good explanation of the subject matter, i now have a basic knowledge and understanding of PHP coding</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good descriptions and examples, able to help all members of the course and happily does so.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will help me be able to create an ecommerce website for my company and understanding this coding to be able to progress to asp.net and iphone development course</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0550">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>A great course for beginners</h2>
                    <span class="review-date">26 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Daniel<br>on ASP.NET Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0552"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A great course for beginners. It was very informative and covered a lot of topics.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shesh was excellent and very knowledgeable on all ASP.NET topics.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully I will be able to secure a junior ASP.NET course thanks to what we've learned over the four weeks.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0552">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>The course was very informative</h2>
                    <span class="review-date">24 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Inioluwa<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0554"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very informative, and encouraging in that it showed me that some of the apps that I thought were impossible to build were not as difficult or far fetched as I thought so its given me more confidence.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>It is obvious that the trainer had a lot of experience with designing and creating apps, I have to say he was very patient with all of us when we didn't quite understand certain concepts which put less pressure on us in the learning experience. He was also very fun to work with.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>For me it will enable me to think bigger in terms of the robustness of apps I will create in the future and make me more confident as a developer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0554">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>The course was great  ...</h2>
                    <span class="review-date">24 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Ole<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0555"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was great and at the technical level that I expected.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very skilled and had good communication skills.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able to join or assist our companys newly started iPhone/iPad development team.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0555">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Amazing</h2>
                    <span class="review-date">24 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Robert Damiano, Office Technology Coordinater at F.A.O. of the United Nations<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0558"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Amazing...probably the best course that you offer.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Joel is the trainer here. young, speaks well, has good patience and know his stuff</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Well i hope it will change it completely within time.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0558">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>The course was very useful at taking me from ...</h2>
                    <span class="review-date">17 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Clive<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0559"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very useful at taking me from someone with no clue of Javascript - to someone who understands and appreciates the benefits of the language.   It was an excellent walk through and comes highly recommended.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The Trainer was friendly informative and more importantly very helpful.  It was great to have someone who clearly knows his stuff.  </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It is going to assist me with development of my MBA research  and in my consulting projects.  I would expect to get tons of benefits out of what I have learnt.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0559">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Good</h2>
                    <span class="review-date">17 Nov 2011</span></div>
                <div>
                    <div class="review-author">By james<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0560"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good, has taught me some interesting tricks i can apply directly to my work</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano is a good teacher, i have found he makes it easy to understand, goes at your pace instead of his and creates an easy learning enviroment</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I am just starting out in web design so i hope this along with the HTML and CSS courses which Emiliano also taught will be the stepping stones i need</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0560">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>I know understand jQuery</h2>
                    <span class="review-date">17 Nov 2011</span></div>
                <div>
                    <div class="review-author">By Jayden Maddison, Web Designer / Front end developer at Lime Media<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0561"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I know understand jQuery. Within four lesson that didn't last that long I learnt everything I needed to know about jQuery.
                    To be honest I didn't think I would be able to grasp it fully in that amount of time but I have.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano was very good, he paid full attention to every student and made sure they understood each bit of what he was teaching us before he moved on.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I work in a webdesign agency and I will be using it in my day to day work life as starting literally tomorrow.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0561">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Very benifical</h2>
                    <span class="review-date">13 Oct 2011</span></div>
                <div>
                    <div class="review-author">By peter rawlings, Director at Wss Media<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0570"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very benifical. This will help me in the future to resolve php issues. It has given me a taste to learn more and brush up on other skill that I have. ie html and css.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer is very good. The communication skill were excellent. I do however wish I has off booked a block of courses. A smaller class of 4-5 was benifical. I would of found it difficult.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will now be able to understand the php language. It will allow me to up/modify php code and read what is happening on the page. Wish I could do another week!!!!!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0570">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Overall, very good</h2>
                    <span class="review-date">13 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Alan Harris, Director at Ashtree Solutions Ltd<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0572"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall, very good.  Perhaps there could have been more on MYSQL.  I think we left the topic of objects until too late, as well.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Generally, very good.  Sometimes he could have improved our ability to understand the details by giving us more context at the start of an activity (for example, drawing a diagram of how the pages will fit together, and how the user will navigate from page to page).</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>SHOULD help with Wordpress, though I ma not sure I am ready to read through the WP PHP files yet!
                    WILL help with creating my own website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0572">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>The course itself is quite difficult considering ...</h2>
                    <span class="review-date">13 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Robert Damiano, Office Technology Coordinater at F.A.O. of the United Nations<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0573"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course itself is quite difficult considering that i do not have much programming backfground. but it certainly opens your views as to what it is and how it is used. above all how important it is.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer. considering is 24 years is very good at what he does. he know his php probably more than anyone. he should probably (considering young age) mature a little more on organization. but this is not a bad review!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Well i certainly hope this contributes to app development course that i will be taking in november</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/3_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0573">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>As a starting point to learning web design ...</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Martin Gannon, Graphic designer at freelance designer<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0576"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>As a starting point to learning web design , I have found it very useful. It has been a lot to take in but has given me a good basis for further learning.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I thought that he went through the stuff as many times as we needed, which sometimes was quite a few to fully understand what we were doing. Had a full understand of the subject, and I believe he will be helpful in the future.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I believe that it will help me lose my fear of designing web pages for myself and others. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0576">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Great course</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Magalie, Client Success Manager at Shopzilla<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0577"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Great course. 
                    good overview of webdesign</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good knowledge, great knowledge and communaction skills.
                    He took the time to answer all our question and gave few options for each way to excecute stuff.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I work in ecommerce and have to manage some of my clients' site content so this was most definitely helpful</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0577">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>This course has left me very impressed at ...</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Jane- Frances Mbanefo, Freelance Graphic Designer at n/a<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0578"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This course has left me very impressed at how fast I grasped the teaching and practical work. This has made me want to learn more and more about web design. Thank you very much :)</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent trainners. His knowledge came out in what he was teaching us and the way he worked with us step by step untill we understood.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This will add a new dimension to my skills set. I am going to practice with what i have learnt and hopefully will soon be able to design websites for clients</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0578">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Very good, overall</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Alan Harris, Director at Ashtree Solutions Ltd<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0579"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good, overall.  Well-taught, covering (generally) a good set of skills.  Good size of class (6 people) any more would have been too cramped for the room.  I wish there could have been some training on JavaScript - we only covered it on the final afternoon.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent - always on time, really knew his material well, had done a lot of preparation in advance.  Very good at explaining topics when we had questions.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Should be really useful for my own website, and also for work.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0579">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Very detailed</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Jonathan Chamberlain<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0580"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very detailed. A lot to cover but good to do and it equips you with a good ability to start writing your own web site</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Not sure it will make much difference but adds to mu skills.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0580">Read more</a></div> <div class="review-wrap clearfix page9" style="display: none;"><header>
                <div><h2>Very well presented</h2>
                    <span class="review-date">06 Oct 2011</span></div>
                <div>
                    <div class="review-author">By Robert Damiano, Office Technology Coordinater at F.A.O. of the United Nations<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0581"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very well presented. 
                    There is alot to learn, and hope we will have some good handouts to practice from home.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very professional. know everything about everything
                    always prepared and patient with everyone.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Well i hope it will be help me alot with my next course here on PHP, ASP.NET and most importantly, APP Development</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0581">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>This course was hands on  ...</h2>
                    <span class="review-date">29 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Debbie Roux, Manager at bezant.uk.net<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0582"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>This course was hands on and very informative. The course was good value to money.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was competent and able to explain the concepts well.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I work with a developer and before the course was over I was able to help him alter some information on an Illustrator document. I will also be looking to market my new skills to my existing client base. And look for new clients.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0582">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Very impressed with the course</h2>
                    <span class="review-date">25 Sep 2011</span></div>
                <div>
                    <div class="review-author">By David Cole<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0586"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very impressed with the course. Learnt alot about html and css.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer was very helpful and offered guidance when needed. Was very experienced in web design.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Will benifit me alot because alot was learnt. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0586">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>The course was good although the pace was ...</h2>
                    <span class="review-date">25 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Dominic Du-Prat<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0588"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was good although the pace was a little too fast at times but with the amount content i do understand.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very knowledgeable and helpful with any problems I had.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will give me a very solid base in which to build my new career as a web designer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0588">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>It was very challenging (in a good way!)</h2>
                    <span class="review-date">25 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Joe Friel, Content Co-ordinator at Folder Media<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0589"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was very challenging (in a good way!), moving at a really good pace and very informative. It's given me a great awareness of the fundamentals of PHP and a solid platform to further develop these new skills.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Ciprian was really great. He was highly knowledgeable, and very kind and understanding when we had problems. He led the course at a good pace that seemed to suit all of us and was constantly willing to give new examples and ways to approach the topic. What I was really impressed about was how he not only taught us about PHP but also taught us how to go about learning and dealing with problems on our own, which is going to be incredibly useful if I am to develop my skills in this area.</p><h3 class="review-impression">What do you think can be improved (facilities, teaching, course contents)?</h3><p>Not anything really. My only main issue is being able to continue my development and being able to understand everything when we're not in the class with someone helping us. 
                    What may be really useful is being set a project at the end of the course. Obviously it's up to us to complete, but this would give us a good focus on what the next steps are to continue learning etc.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The radio industry I work in is increasingly multi-platform so this will give me a good knowledge to approach new ideas for technical developments. In an immediate sense I can now hopefully create a submission system for our events section on one of my radio station's websites.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0589">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>The course has been REALLY interesting</h2>
                    <span class="review-date">25 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Kirt Redden, N/A at N/A<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0590"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course has been REALLY interesting, it is just what I needed!! I feel confident in being able to create my own PHP projects.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Ciprian is a brilliant teacher. He has kept me interested in the course the whole way through and he is always checking that we understand what he is teaching us. He is always asking us to ask him questions and think of different PHP challenges which I feel is a very good quality.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will be able to apply the knowledge gained from the PHP course in most if not all of my projects to create contact forms, website search and even a e-commerce website. I'm really looking forward to starting a personal project.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0590">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>A useful course with a lot of useful information ...</h2>
                    <span class="review-date">15 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Josh, Server Side Developer at WebShopApps Ltd<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0591"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A useful course with a lot of useful information that will certainly come in handy in future software development.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Communications and patience were excellent. Good knowledge of the subject area.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I can now implement this into my software which should make it easier for out end users.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0591">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>A very good course</h2>
                    <span class="review-date">15 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Stephanie David, Developer at Legal Services Commission<br>on AJAX Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0592"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A very good course, with interesting examples.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very well presented. Personable, understandable communication.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It may but this was not the reason I took the course.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0592">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Very good course, full of knowledge</h2>
                    <span class="review-date">14 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Paula Freitas<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0593"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good course, full of knowledge. I was a complete newbie and had never used the programs before. Now I am more confident in using it. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>She really knows what she is doing, we can see that she is a good professional in the field. I would, if possible, improve a little bit the communication skills in a more structured way. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I believe that it will benefit a lot. I intend in having my own website and the course really helped with the design area. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0593">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Well structured  ...</h2>
                    <span class="review-date">13 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Peter Langley, Web Developer at n/a<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0594"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Well structured and contains current industry examples of JavaScript and jQuery usage. it is a fast paced course, so make sure you've read up/used some JavaScript/jQuery before. And your HTML and CSS skills should be fluent to keep up with the tasks.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very knowledgable, and could answer all questions. He clearly has a lot of experience in Web Development. Plus he had good humour (!!) and I felt comfortable to ask any queries</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Points me in the right direction for further learning. Sets me up with industry standards and 'the best way to approach things'. e.g. how to debug &amp; test scripts and a good list of resources were provided.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0594">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Good review of JavaScript and jQuery</h2>
                    <span class="review-date">13 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Stephanie David, Developer at Legal Services Commission<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0596"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good review of JavaScript and jQuery. Up to date.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Knowledgable instructor. Very helpful.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It might. But I didn't take it for this reason. It is something I want to do anyway.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0596">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Very good and professional</h2>
                    <span class="review-date">13 Sep 2011</span></div>
                <div>
                    <div class="review-author">By Josh, Server Side Developer at WebShopApps Ltd<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0597"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good and professional. Fast paced but wasn't too fast at all.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Good communications skills, well presented and interactive.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Can now confidently use Jquery and JS in my software which in turn should help my customers.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0597">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>I have really enjoyed the course</h2>
                    <span class="review-date">25 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Md Rezaul Alam<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0604"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I have really enjoyed the course. Though it was to first for me to pick the code up. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer was very good in communicating and skills.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This course will defenitely help me upto the ultimate goal of  my career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0604">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>A brilliant course for beginners</h2>
                    <span class="review-date">25 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Kirt Redden, N/A at N/A<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0605"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>A brilliant course for beginners, it has really helped me understand HTML and CSS and i feel confident with taking on personal projects on my own.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Emiliano was an excellent tutor! Really easy to get on with and very helpful. Every lesson was fun and I left feeling that I had learnt a large amount.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I feel that this course has been a valuable start to my web design career and I feel confident that I will be able to design my own website.
                    This is a brilliant start and now I understand HTML + CSS I can develop my skills further by studying further at home.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0605">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Good ...</h2>
                    <span class="review-date">25 Aug 2011</span></div>
                <div>
                    <div class="review-author">By james<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0606"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Its going to make me rich!!! ha</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0606">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Good introduction to HTML &amp; CSS ...</h2>
                    <span class="review-date">25 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Jolyon Hedges, IT Manager<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0607"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good introduction to HTML &amp; CSS</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Knew HTML &amp; CSS very was was able to communicate ideas and principles clearly</p><h3 class="review-impression">What do you think can be improved (facilities, teaching, course contents)?</h3><p>Should not need to bring your own paper &amp; pens - should be provided.  Could be improved by using SMART interactive white boards would enable tutor to save and send information written on the screen.  </p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0607">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Very well structured course</h2>
                    <span class="review-date">11 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Yohan Maryn, Integration Engineer at Shopzilla<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0611"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very well structured course. I came to this course with no knowledge of php but after just 4 days of training I have now strong basics knowledge to develop my php skills.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shahid's knowledge of PHP was really helpful and he was always ready to come and help.
                    His training was very well structured and well explained.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I've just moved to a more technical role in my company and now work on a js and php based IT solution. This training will enable me to be of more help for our clients.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0611">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>I found the course a useful insight into ...</h2>
                    <span class="review-date">11 Aug 2011</span></div>
                <div>
                    <div class="review-author">By David Tanner, Head of IT and New Media at Fabric Life ltd<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0612"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I found the course a useful insight into PHP</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I thought the trainer was very knowledgeable, occasionally we rushed through some subjects.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very much, it will be a stepping stone to learning more php and development</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0612">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>I think the course was very userful  ...</h2>
                    <span class="review-date">11 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Chris Burrows, Owner at POMU4<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0613"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I think the course was very userful and very well run. It has given me a starting point which I believe I can now build on. It covered everything it set out to cover and was suitably hands on.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Clearly very experienced and knowledgable. Good communications skills and very easy to get on with. Perhaps the pace varied more than it should have but it is only a very minor thing.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully allow me to turn an idea into a money making reality.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0613">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>Overall the course was very imformative  ...</h2>
                    <span class="review-date">11 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Cameron Davidson, Marketing &amp; Design Manager at Excess Baggage Company<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0614"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall the course was very imformative and helpful.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very good and easy to follow</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Give me a grounding and starting point in programing php websites</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0614">Read more</a></div> <div class="review-wrap clearfix page10" style="display: none;"><header>
                <div><h2>The course manages to balance the right amount ...</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Trystan Williams, n/a at n/a<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0616"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course manages to balance the right amount of information with the right pace. The high amount of information is given to you in an understandable and practical format, at a speed that allows you to take it all in but with out giving you the option of being bored.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer took the alien concept of Web design and layed it out so I could finaly see html and css not as a jumble of symbols but a practical puzzle that I can now fit together. The trainer took what is a different language and made it understandable.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will now be able to effectivley carry out projects that I do for myself at home and for my university on to the internet. This new found skill will give me a new platform in which I can display my work and reach new audiences. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0616">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very professional and didactic</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Nina Drakou, TRUSTEE at MIST Foundation<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0617"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very professional and didactic.
                    VERY VERY HELPFULL!!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>I would rate both Sooji and Emiliano very skilfull and helpful.
                    Communocation skills exellent.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I will design my own web sites ,both the professional and the charity one.It is of great value to understand the concept of web designing and SEO even if you are not a professional designer, because you realise how the others are looking at you AND YOU CAN PROMOTE YOURSELF in real life.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0617">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Fast pace  ...</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Vicki Clarke, Junior Designer at Taylor Wessing<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0619"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Fast pace and everything covered in detail with great support if you get stuck!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Both trainer and assistant obviously know the subject very well and helped resolve issues if students made any errors. Trainer made sure everyone was up to pace.

                    Those that were ahead were set further tasks so they weren't bored whilst waiting for everyone to catch up.

                    Communication was brilliant - if you didn't understand how something worked, we were provided analagies until everyone understood.

                    Made us think and problem-solve for ourselves to test and ensure we had understood, but was on hand if we got stuck.

                    Overall - brilliant!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I understand more about coding (more importantly "clean" coding!) and am now able to find things in code a lot better. This should hopefully mean I can help our webmaster with her job and pushes me one step further to working as a web designer rather than just graphic designer.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0619">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Jeremy Hill, Business Information Analyst at Siemens Enterprise Communications L<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0620"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good. Good pace.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer and assistant had the experience and communicated well.  It helped to have 2 trainers.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help me development an internal website with a colleague for the company we work for.

                    Give me opportunities.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0620">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>I have found the course very good</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Matteo Iori, Student at Student<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0621"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I have found the course very good. I've learned a loto of new things on the web design field.
                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was very good, I liked very much the ability of teaching.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>A lot, in 4 days I learned how do a website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0621">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Overall very good</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Daniel Gillespie, n/a at n/a<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0622"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Overall very good, a good introduction to the topics which I will further through study away from class</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Overall very good</p><h3 class="review-impression">What do you think can be improved (facilities, teaching, course contents)?</h3><p>More documentation</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Greater knowledge and technical gargon</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0622">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very informative  ...</h2>
                    <span class="review-date">04 Aug 2011</span></div>
                <div>
                    <div class="review-author">By Chris Burrows, Owner at POMU4<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0624"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very informative and gave me what I needed to know. Could have been more challenging.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Seemed very experienced however I'm not sure she wants to be training people all day.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully make me rich.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0624">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>It was very good</h2>
                    <span class="review-date">30 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Jayden Maddison, Web Designer / Front end developer at Lime Media<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0625"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It was very good, I learnt quite alot considering the small amount of time we had on the course. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p> Sooji was a great teacher, she helped out when we had problems and explained why that problem was occuring.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Well, I work for a web design and development agency and I can start using my new PHP skills straight away. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0625">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Before the start of the course I was not ...</h2>
                    <span class="review-date">30 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Prem Shrivastava, Senior Analyst at Webtrajectory<br>on PHP Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0626"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Before the start of the course I was not sure whether I would be able to cover all the contents of this course within this short span of time, but thanks to the trainer who taught this complex course such an engaging style and command that we enjoyed  while learning.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>

                    Excellent</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Right now I am not working on the PHP in my company but whenever a project comes that suits php I will recommend using PHP as programming language.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0626">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very fast track!</h2>
                    <span class="review-date">24 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Paula Freitas<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0628"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very fast track! It is a good thing to have at least some knowledge of it otherwise it is very easy to get lost. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>She is very good and full of knowledge. There was no question without answer. However, because of the fast pace of the course, she really goes fast and it is very easy to get lost. She has patience though to explain one by one. 
                    In this booklet, we would find the technical explanation of why do you use certain tag and not another, what is a tag for etc etc. In a very easy to understand kind of way... </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Probably. I am going to masters and I was using this course as a basic learning and do not feel lost in the major course.

                    I would recommend this course to someone, however, I would let clear that it is important to have some small knowledge in HTML / CSS to have a better good user. </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0628">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>I enjoyed the course very much</h2>
                    <span class="review-date">24 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Delawer Hussain, ADVISOR at NATWEST<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0629"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I enjoyed the course very much. It has given me the insight and confidence to build my own website. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The tutor was very helpful with teaching everyone according to their own level and pace of learning. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I shall apply my knowledge of web design into my free lancing career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0629">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very informative  ...</h2>
                    <span class="review-date">24 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Joe Friel, Content Co-ordinator at Folder Media<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0632"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very informative and helped teach me the basics of HTML and CSS, pretty much did exactly as was promised so that was great. Sometimes it moved a bit slowly, although I think most the class didn't feel the same way. Also, the course mentioned about learning Javascript and I felt that we could have done with having more time in the course to look at this as we only really touched on it.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was great. She was always very helpful, patient and clearly communicated how everything worked. She was very useful when it came to extra work we could do and was quickly able to solve the problems we encountered when designing out sites.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Has given a great foundation in understanding HTML and CSS which will now allow me to confidently add further skills and knowledge. As  I work in radio and radio is increasingly multi-platform, an understanding of this is incredibly useful. Plus it's fun!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0632">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Fast past but extremely useful ...</h2>
                    <span class="review-date">24 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Alex Henderson, Owner at SquareNet Media<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0633"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Fast past but extremely useful</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Excellent knowledge of webdesign which showed when giving external examples of where to fnd help on the internet from outside the class.

                    Patient and extremely helpful when explaining. Friendly and very approachable. A credit to the company

                </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Allow me to build a portofilio of website to allow me to apply for juniro jobs ot get me into the IT sector</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0633">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very good fundementals</h2>
                    <span class="review-date">24 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Terence Gill, Director at TeVi Web Design &amp; Development<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0634"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good fundementals, has prepared me to take further php, jquery courses</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Great knowledge... very friendly, helpful and imformative!</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Has helped  me to understand the core elements of web design</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0634">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>I liked course and learned a lot</h2>
                    <span class="review-date">21 Jul 2011</span></div>
                <div>
                    <div class="review-author">By zivile<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0635"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I liked course and learned a lot.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Im web/graphic designer, so I think it should help.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0635">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">21 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Kathryn Stowell, Head of ICT at Charlton School<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0636"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good, have enough skill and information to go away and use...I hope! </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good, experienced and knowledgeable...fast paced but exercises helped to embed the skills and apply with support. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Use with school website and own projects</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0636">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>I really enjoyed the course-I learned a  ...</h2>
                    <span class="review-date">21 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Yasmin<br>on jQuery &amp; JavaScript Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0637"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I really enjoyed the course-I learned a lot!</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>SooJi is great :-) </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The course is already helping me.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0637">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Excellent</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Sophie Thorpe<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0640"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Excellent, very informative, fast and detailed. it certainly showed me that it is possible to save a lot of money by creating it myself.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very good. able to provide indepth explanations in a concise manner. Clear and enthusiastic and obviously has a wealth of knowledge on the subject.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will change my career totally and free me from working for others and create independance for myself.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0640">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>The course was very intense</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Yasmin<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0641"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very intense, I learned a lot of new techniques.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Gavin is amazing- great teacher, clear &amp; concise, very knowledgable.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It will help a lot in my web design work.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0641">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>The length of the course allows is just nice ...</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Jin Wei Foo<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0642"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The length of the course allows is just nice for a starter without any knowledge of web design. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer has extensive, in depth knowledge knowledge about the course and is very helpful in solving the problems I encounter. Sometimes can be a bit fast but most of the time can still catch up. Overall very good.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Very helpful for me to start my own website.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0642">Read more</a></div> <div class="review-wrap clearfix page11" style="display: none;"><header>
                <div><h2>Good course</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Azim mahmood, Technical Operations Manager at Adpepper Media<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0643"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good course. I got what I wanted from it and enjoyed it.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Sooji is awesome, best teacher in the universe. She is very good at breaking things down for us thick people. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0643">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>I really enjoyed the course  ...</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By Yasmin<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0644"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>I really enjoyed the course and am eager to learn more!
                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>SooJi is amazing, like her way of thinking and stuctured approach.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I hope my clients will enjoy their new websites!</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0644">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>The course was nice ...</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By mohammed<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0645"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was nice</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>She was fantastic</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>To improve my skeils</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0645">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Helpfull</h2>
                    <span class="review-date">04 Jul 2011</span></div>
                <div>
                    <div class="review-author">By lisa lebret<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0646"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Helpfull, interesting</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Perfect</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I learned about web design and want to learn more now.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0646">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Intensive and very high paced!</h2>
                    <span class="review-date">30 Jun 2011</span></div>
                <div>
                    <div class="review-author">By John Pilgrim, Web Developer at Hattrickmedia<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0648"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Intensive and very high paced! But covered everything from the ground upwards - and considering it has only been 4 days, a awful amount of development has been done in that time period. </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very knowledgeable and high paced. At the beginning I thought that he was going through two quickly but then I had my eureka moment and things started to drop into place. I think due to the high pace of the tuition it allowed me to think quicker at solving any of the problems in front of me. Would have like it if he could have stopped and perhaps even commented the code (for reference) so it was clear what certain bits did. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>It should do, has given me a good insight into doing things the right way, without this I would still be guessing at what I should be doing. Like with all things my knowledge should grow the more experience I get but this has set me on the right way I guess.... only time will tell! </p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/3_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0648">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Good!</h2>
                    <span class="review-date">30 Jun 2011</span></div>
                <div>
                    <div class="review-author">By Matthew Wyld, Web Analyst Programmer at Worcestershire County Council<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0649"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Good! Roman defiantly knows his stuff inside out. Better then other courses I have been on which is just follow along with a manual or book. Course should be 5 days to cover everything as a little over rated for what can be covered in 4.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced, no problem communicating (speaks better english then me!)</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>This course has filled a lot of gaps where I was getting stuck an will surely help me on my way. I will surely recommend Roman and Dot peak to colleagues at Worcestershire County Council.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0649">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>The course was very useful</h2>
                    <span class="review-date">30 Jun 2011</span></div>
                <div>
                    <div class="review-author">By Chris Terry, Development Team Leader at Inviron<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="/system/application/views/templates/images/4_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0650"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very useful, I now feel able to practise the skills I have learned in the course. At times the pace was a little slow, and i'm not sure all of the course aims were met.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Roman is a nice guy. At first I felt the language barrier might be a bit of an obstacle, and he seemed a bit nervous to begin with. However over time, he seemed to become more warm and once that happened it seemed much easier to communicate. He is clearly well versed in the topic, and I particularly like the fact that he was quite willing to admit when he didn't know something himself and would look it up (often I find trainers will *blag* their way through, he didn't do that).</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I really needed to be able to develop on iOS to move forward a Project, and I now feel confident I will be able to progress with this straight away.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/3_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="/system/application/views/templates/images/4_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0650">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>It moves very quickly so some background ...</h2>
                    <span class="review-date">30 Jun 2011</span></div>
                <div>
                    <div class="review-author">By Kevin Evans, none at none<br>on iPhone App Development Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0651"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>It moves very quickly so some background in programming is nearly essential.  The course covered a lot of material which has shown me where I need to focus.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Roman has great ability, a nice teaching style and made me feel very comfortable.  The pace is fast but he kept me with the class despite a bigger gap in my experience so I'm quite impressed with his ability.  </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>I'm hoping that this will give me the ability to learn how to produce my own app.  I expect that to either turn into a business itself (most likely part-time though) and having more of a basis in programming will help my career prospects.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0651">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Very interesting  ...</h2>
                    <span class="review-date">25 Jun 2011</span></div>
                <div>
                    <div class="review-author">By colette tasha, Customer development rep at Coca Cola Enterprises<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0653"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very interesting and learnt a lot of new stuff</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Very experienced, helpul, good knowledge on various softwares and what I admired most was his friendliness and patience as gavin was always willing  to simplify stuff to me  and others  which we found complex</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Yes</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0653">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>The overall content was great</h2>
                    <span class="review-date">25 Jun 2011</span></div>
                <div>
                    <div class="review-author">By vinh, MD at TEVI<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0654"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The overall content was great, i believe there should be different levels of teaching, beg, inter, experts 
                </p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Trainer demostrated great experience in his field, 
                    communication was second to none. </p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The skills i have learned will definately help me go into my career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0654">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>The course was very good</h2>
                    <span class="review-date">25 Jun 2011</span></div>
                <div>
                    <div class="review-author">By Rhys Huxley, Self at Self<br>on Graphic Design Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0655"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>The course was very good. It detailed all aspects of a number of programmes and detailed from beginners to advanced levels.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>The trainer was good, he explained everything slowly, and the more complicated aspects numerous times. He also gave individual as well as group support. He did sometimes get a little distracted.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Hopefully it will give me the skills needed to progress in a design career.</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0655">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Very good</h2>
                    <span class="review-date">09 Jun 2011</span></div>
                <div>
                    <div class="review-author">By Marcin Rymarz, data administrator at siemens-enterprise.com<br>on Web Design (HTML &amp; CSS) Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0661"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Very good, I was nicely supprised by having 2 trainers. Very practical course. You can see trainers are not only teaching, they are working with this. I like things like "I real live we do this...."</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Both of them very good</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>Helped a lot, as it was very practical</p><div class="user-ratings">
                    <div class="user-review-recommend">Will you recommend this course to others? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0661">Read more</a></div> <div class="review-wrap clearfix page12" style="display: none;"><header>
                <div><h2>Impression is good, friendly atmosphere</h2>
                    <span class="review-date">24 May 2011</span></div>
                <div>
                    <div class="review-author">By Dominic, Creative Director at Carayol<br>on WordPress Course</div><div class="review-rate">
                        <span class="pull-left" style="float: left;">Overall evaluation: </span>
                        <img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif">
                    </div> </div>
            </header><div class="review-js-wrap" id="review-item-0675"><h3 class="review-impression">In 2-3 sentences describe the overall impression about the course. </h3><p>Impression is good, friendly atmosphere. Large amount of knowledge to absorb but looking forward to try out what I have learnt.</p><h3 class="review-impression">What did you think about the trainer (experience, communication, skills etc)? </h3><p>Shahid was excellent, great knowledge and explanations. Sooji was seemingly unprepared and did not set up the relevance for her afternoon session.</p><h3 class="review-impression">How will the knowledge you gained during the course benefit you in your career?</h3><p>The knowledge will help me by adding a skill that I can offer others.</p><div class="user-ratings">
                    <div class="user-review-recommend">Can we use your feedback for promoting our courses? </div>
                    <div class="users-rating recommend">Yes</div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Did you enjoy the course? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's communication skills? </div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div><div class="user-ratings">
                    <div class="user-review-knowledge">Trainer's knowledge of the topic?</div>
                    <div class="users-rating"><img src="http://www.trainingdragon.co.uk/system/application/views/templates/images/5_star.gif"></div>
                </div></div><a href="" class="review-rm" id="item-0675">Read more</a></div>                        
        
    </section>




    <footer class="advpager"><ul class="pageNav"><li class="currentPage" pagenav1'=""><a rel="1" href="#">1</a></li><li class="pageNav2"><a rel="2" href="#">2</a></li><li class="pageNav3"><a rel="3" href="#">3</a></li><li class="pageNav4"><a rel="4" href="#">4</a></li><li class="pageNav5"><a rel="5" href="#">5</a></li><li class="pageNav6"><a rel="6" href="#">6</a></li><li class="pageNav7"><a rel="7" href="#">7</a></li><li class="pageNav8"><a rel="8" href="#">8</a></li><li class="pageNav9"><a rel="9" href="#">9</a></li><li class="pageNav10"><a rel="10" href="#">10</a></li><li class="pageNav11"><a rel="11" href="#">11</a></li><li class="pageNav12"><a rel="12" href="#">12</a></li></ul></footer>
</div>
