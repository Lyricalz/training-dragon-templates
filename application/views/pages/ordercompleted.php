<div id="text-wrap">
    <h1>Order Confirmation</h1>
    <div class="text">
        Please find below your order details.<br />
        <br />
        <p>Order Number: <span>{order_number}</span></p>
        <p>Order Date: <span>{order_date}</span></p>
        <p>Order Status: <span>{order_status}</span></p>
        <h3>Order Details</h3>
        <table class="table">
            <tbody>
                <tr>
                    <th style="width: 67%;"><strong>Address:</strong></th>
                    <th style="width: 33%;"><strong>Contact:</strong></th>
                </tr>
                <tr>
                    <td><span style="font-size: small;">{fname} {lname}<br />
                            {address}<br />
                            {city}<br />
                            {post_code}<br />
                            Great Britain  </span></td>
                    <td>
                        <span style="font-size: small;">Email: {email}<br />
                            Phone: {phone}
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3>Courses</h3>
        <div class="visible-desktop">
            <table class="table">
                <tbody>
                    <tr>
                        <th width="268" bgcolor="#e9e9e9"><strong>NAME</strong></th>
                        <th width="148" bgcolor="#e9e9e9"><strong>TOTAL COURSES</strong></th>
                        <th width="97" bgcolor="#e9e9e9"><strong>PRICE</strong></th>
                        <th width="85" bgcolor="#e9e9e9"><strong>TOTAL PRICE</strong></th>
                    </tr>
                    <tr>
                        <td valign="top" bgcolor="#f7f7f7">{courses_names}</td>
                        <td valign="top" bgcolor="#f7f7f7" style="text-align: center;"><strong>{quantity}</strong></td>
                        <td valign="top" bgcolor="#f7f7f7">
                            <div align="left">&pound; {total}</div>
                        </td>
                        <td valign="top" bgcolor="#f7f7f7">
                            <div align="right">&pound; {total}</div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>TOTAL:</strong></td>
                        <td>
                            <div align="right">&pound; {total}</div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td bgcolor="#e9e9e9" bordercolor="#e9e9e9"><strong>ORDER TOTAL:</strong></td>
                        <td bgcolor="#e9e9e9">
                            <div align="right">&pound; {total}</div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="mobile hidden-desktop">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <td>{courses_names}</td>
                </tr>
                <tr>
                    <th>Total Courses</th>
                    <td>{quantity}</td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td>£ {total}</td>
                </tr>
                <tr>
                    <th>Total Price</th>
                    <td>£ {total}</td>
                </tr>
                <tr>
                    <th>Total:</th>
                    <td>£ {total}</td>
                </tr>
                <tr>
                    <th>Order Total:</th>
                    <td>£ {total}</td>
                </tr>
            </table>
        </div>
    </div>
</div>