<div id="breadcrumb">
    <a href="/" title="Trainingdragon home">Home</a><span class="sprite-blueArrow"></span>Location
</div>
<div id="text-wrap">
    <h1>London Office</h1>
    <div class="text">
        <p>Get in Touch. We're never too busy that we can't stop for a chat. Plus we haven't even scratched the surface of what iWeb can offer.                     So just call us, e-mail us - even write an old school letter, if you like. You can find us at the offices below.                     We'll be delighted to hear from you.</p>
        <div id="map-wrap">
    <div id="map" style="width: 600px; height: 400px;"><!--<img src="http://trainingdragon.local/system/application/views/templates/images/map-directions.png" height="360" width="482" />-->
                <div id="map-canvas" style="width: 100%; height: 100%;"></div>
            </div>
            <div id="get-directions-wrap">
                <address>
                    <h3>TrainingDragon London</h3>
                    Kings Cross Business Centre,<br>
                    180-186 Kings Cross Road,<br>
                    London,<br>
                    WC1X 9DE</address>
                <div id="get-directions">
                    <header>
                        <h3>Get Directions</h3>
                    </header>
                    <form>
                        <label for="from">From:</label>                                 
                        <input type="text" id="from">                                 
                        <label for="to">To:</label>                                 
                        <input type="text" id="to" disabled="disabled" value="WC1X 9DE, Training Dragon">                                 <input type="button" value="Calculate" onclick="calcRoute();">
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>