<form id="content" method="post" action="/course/all_courses">

    <div class="top">
        <label>From: </label>
        <input type="text" name="datea" id="datea" value="" autocomplete = "off" />
        <label>To: </label>
        <input type="text" name="dateb" id="dateb" value="" autocomplete = "off" />
        <input type="text" name="search" id="search" value="" placeholder ="Search keyword"/>
        <input type="hidden" name="filter" value="1"/>
        <input type="submit" value="Search"/>
    </div>            

    <table class="managing" id="personal">
        <thead>
            <tr>
                <th scope="col" class="check"><input id="checkAll" name="testing" type="checkbox" class="checkbox masterCheck" /><label for="checkAll"></label></th>
                <th scope="col"><span>Course Name</span></th>
                <th scope="col"><span>Classes</span></th>
                <th scope="col" style="width: 109px;"><span>Session</span></th>
                <th scope="col">Seats</th>
                <th scope="col" style="width: 70px;">Start Date</th>
                <th scope="col">Availability</th>
                <th scope="col">View Users</th>
                <th scope="col">View Reviews</th>


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox1" class="checkbox" /><label for="checkbox1"></label></td>
                <td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-303)</td>
                <td style="text-align: center;">4</td>
                <td>10:00 - 17:00 Sat</td>
                <td>0</td>
                <td>01 Mar 2014</td>
                <td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1285);" style="font-size:12px"> 
                        <option value="Not_Decided">Not Decided</option>
                        <option value="Available">Available</option>
                        <option value="Not_Available" >Not Available</option>
                    </select></td>
                <td><a href="/course/bookings/894">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
    </tr><tr>
    <td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox2" class="checkbox" /><label for="checkbox2"></label></td>
    <td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-305)</td>
    <td style="text-align: center;">4</td>
    <td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
    <td>0</td>
    <td>27 Jan 2014</td>
    <td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1284);" style="font-size:12px"> 
            <option value="Not_Decided">Not Decided</option>
            <option value="Available">Available</option>
            <option value="Not_Available" >Not Available</option>
        </select></td>
    <td><a href="/course/bookings/893">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox3" class="checkbox" /><label for="checkbox3"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>25 Nov 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1283);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/892">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox4" class="checkbox" /><label for="checkbox4"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>16 Nov 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1328);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/944">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox5" class="checkbox" /><label for="checkbox5"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>29 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1318);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/929">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox6" class="checkbox" /><label for="checkbox6"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>28 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1764);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1357">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox7" class="checkbox" /><label for="checkbox7"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>19 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1507);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1112">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox8" class="checkbox" /><label for="checkbox8"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>19 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1730);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1325">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox9" class="checkbox" /><label for="checkbox9"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Fri</td>
<td>0</td>
<td>18 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1349);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/965">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox10" class="checkbox" /><label for="checkbox10"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>15 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1361);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/977">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox11" class="checkbox" /><label for="checkbox11"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1317);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/928">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox12" class="checkbox" /><label for="checkbox12"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1578);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1182">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox13" class="checkbox" /><label for="checkbox13"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1584);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1188">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox14" class="checkbox" /><label for="checkbox14"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1590);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1194">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox15" class="checkbox" /><label for="checkbox15"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a></td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1619);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1221">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox16" class="checkbox" /><label for="checkbox16"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1670);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1268">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox17" class="checkbox" /><label for="checkbox17"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-305)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>14 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1754);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1345">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox18" class="checkbox" /><label for="checkbox18"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>13 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1314);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/925">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox19" class="checkbox" /><label for="checkbox19"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1360);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/976">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="836" name="items[]" id="checkbox20" class="checkbox" /><label for="checkbox20"></label></td>
<td><a href="/premiere-pro-course-training">Adobe Premiere Pro</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1527);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1131">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="898" name="items[]" id="checkbox21" class="checkbox" /><label for="checkbox21"></label></td>
<td><a href="/exam-70-480-programming-in-html5-with-javascript-and-css3-course-training">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1541);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1145">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="910" name="items[]" id="checkbox22" class="checkbox" /><label for="checkbox22"></label></td>
<td><a href="/interconnecting-cisco-networking-devices-part-1-icnd1-course-training">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1546);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1150">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="48" name="items[]" id="checkbox23" class="checkbox" /><label for="checkbox23"></label></td>
<td><a href="/ccnp-certification-course-training">CCNP Certification</a>(KK-305)</td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1551);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1155">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox24" class="checkbox" /><label for="checkbox24"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1557);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1161">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="916" name="items[]" id="checkbox25" class="checkbox" /><label for="checkbox25"></label></td>
<td><a href="/mcts-microsoft-sql-server-2008-business-intelligence-development-and-maintenance1-course-training">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1562);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1166">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="100" name="items[]" id="checkbox26" class="checkbox" /><label for="checkbox26"></label></td>
<td><a href="/mysql-for-beginners-sql-4401-course-training">MySQL for Beginners SQL-4401</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1567);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1171">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="918" name="items[]" id="checkbox27" class="checkbox" /><label for="checkbox27"></label></td>
<td><a href="/mta-database-administration-fundamentals-exam-98-364-microsoft-technology-associate-certification-course-training">Exam 98-364: MTA Database Administration Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1572);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1176">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox28" class="checkbox" /><label for="checkbox28"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1577);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1181">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox29" class="checkbox" /><label for="checkbox29"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1583);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1187">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="844" name="items[]" id="checkbox30" class="checkbox" /><label for="checkbox30"></label></td>
<td><a href="/server-certification-course-training">Server+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1613);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1215">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox31" class="checkbox" /><label for="checkbox31"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1618);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1220">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="927" name="items[]" id="checkbox32" class="checkbox" /><label for="checkbox32"></label></td>
<td><a href="/rhcsa-certification-course-training">RHCSA Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1631);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1233">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="928" name="items[]" id="checkbox33" class="checkbox" /><label for="checkbox33"></label></td>
<td><a href="/rhcsa-certification-red-hat-system-administration-i-rh124-course-training">Red Hat System Administration I (RH124)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1636);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1238">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="929" name="items[]" id="checkbox34" class="checkbox" /><label for="checkbox34"></label></td>
<td><a href="/rhcsa-red-hat-system-administration-ii-rh135-course-training">Red Hat System Administration II (RH135)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1641);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1243">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="930" name="items[]" id="checkbox35" class="checkbox" /><label for="checkbox35"></label></td>
<td><a href="/rhce-red-hat-system-administration-iii-rh255-course-training">Red Hat System Administration III (RH255)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1646);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1248">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="901" name="items[]" id="checkbox36" class="checkbox" /><label for="checkbox36"></label></td>
<td><a href="/grasshopper-3d-course-training">Grasshopper 3d</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1679);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1278">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="881" name="items[]" id="checkbox37" class="checkbox" /><label for="checkbox37"></label></td>
<td><a href="/solidworks-course-training">SolidWorks</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>05 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1756);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1347">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox38" class="checkbox" /><label for="checkbox38"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>01 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1355);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/971">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox39" class="checkbox" /><label for="checkbox39"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">40</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>01 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1506);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1111">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox40" class="checkbox" /><label for="checkbox40"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>01 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1589);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1193">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="879" name="items[]" id="checkbox41" class="checkbox" /><label for="checkbox41"></label></td>
<td><a href="/vb-dot-net-course-training">VB.NET</a></td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>01 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1739);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1333">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox42" class="checkbox" /><label for="checkbox42"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-305)</td>
<td style="text-align: center;">40</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>01 Oct 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1753);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1344">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox43" class="checkbox" /><label for="checkbox43"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1308);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/919">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox44" class="checkbox" /><label for="checkbox44"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1313);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/924">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox45" class="checkbox" /><label for="checkbox45"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1316);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/927">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="97" name="items[]" id="checkbox46" class="checkbox" /><label for="checkbox46"></label></td>
<td><a href="/oracle-database-introduction-to-sql-course-training">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1489);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1095">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox47" class="checkbox" /><label for="checkbox47"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a></td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1719);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1315">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="43" name="items[]" id="checkbox48" class="checkbox" /><label for="checkbox48"></label></td>
<td><a href="/a-certification-course-training">A+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>30 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1763);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1355">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox49" class="checkbox" /><label for="checkbox49"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>29 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1348);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/964">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox50" class="checkbox" /><label for="checkbox50"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>29 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1408);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1025">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="917" name="items[]" id="checkbox51" class="checkbox" /><label for="checkbox51"></label></td>
<td><a href="/mta-networking-and-security-fundamentals-exams-98-366-98-367-microsoft-technology-associate-certification-course-training">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>29 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1595);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1199">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox52" class="checkbox" /><label for="checkbox52"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1315);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/926">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox53" class="checkbox" /><label for="checkbox53"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1354);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/970">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="7" name="items[]" id="checkbox54" class="checkbox" /><label for="checkbox54"></label></td>
<td><a href="/mcts-sql-server-2008-course-training">MCTS SQL Server 2008</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1528);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1132">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="875" name="items[]" id="checkbox55" class="checkbox" /><label for="checkbox55"></label></td>
<td><a href="/python-course-training">Python</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1691);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1290">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="108" name="items[]" id="checkbox56" class="checkbox" /><label for="checkbox56"></label></td>
<td><a href="/use-xml-db-oracle-database-11g-course-training">Use XML DB (Oracle Database 11g)</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1692);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1291">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox57" class="checkbox" /><label for="checkbox57"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-305)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>21 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1752);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1343">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="836" name="items[]" id="checkbox58" class="checkbox" /><label for="checkbox58"></label></td>
<td><a href="/premiere-pro-course-training">Adobe Premiere Pro</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1526);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1130">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="898" name="items[]" id="checkbox59" class="checkbox" /><label for="checkbox59"></label></td>
<td><a href="/exam-70-480-programming-in-html5-with-javascript-and-css3-course-training">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1540);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1144">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="910" name="items[]" id="checkbox60" class="checkbox" /><label for="checkbox60"></label></td>
<td><a href="/interconnecting-cisco-networking-devices-part-1-icnd1-course-training">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1545);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1149">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="48" name="items[]" id="checkbox61" class="checkbox" /><label for="checkbox61"></label></td>
<td><a href="/ccnp-certification-course-training">CCNP Certification</a>(KK-305)</td>
<td style="text-align: center;">30</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1550);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1154">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox62" class="checkbox" /><label for="checkbox62"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1556);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1160">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="916" name="items[]" id="checkbox63" class="checkbox" /><label for="checkbox63"></label></td>
<td><a href="/mcts-microsoft-sql-server-2008-business-intelligence-development-and-maintenance1-course-training">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1561);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1165">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="100" name="items[]" id="checkbox64" class="checkbox" /><label for="checkbox64"></label></td>
<td><a href="/mysql-for-beginners-sql-4401-course-training">MySQL for Beginners SQL-4401</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1566);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1170">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="918" name="items[]" id="checkbox65" class="checkbox" /><label for="checkbox65"></label></td>
<td><a href="/mta-database-administration-fundamentals-exam-98-364-microsoft-technology-associate-certification-course-training">Exam 98-364: MTA Database Administration Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">6</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1571);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1175">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox66" class="checkbox" /><label for="checkbox66"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1576);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1180">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox67" class="checkbox" /><label for="checkbox67"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1582);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1186">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="917" name="items[]" id="checkbox68" class="checkbox" /><label for="checkbox68"></label></td>
<td><a href="/mta-networking-and-security-fundamentals-exams-98-366-98-367-microsoft-technology-associate-certification-course-training">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1594);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1198">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox69" class="checkbox" /><label for="checkbox69"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a>(KK-301)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1607);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1209">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="901" name="items[]" id="checkbox70" class="checkbox" /><label for="checkbox70"></label></td>
<td><a href="/grasshopper-3d-course-training">Grasshopper 3d</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1678);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1277">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="881" name="items[]" id="checkbox71" class="checkbox" /><label for="checkbox71"></label></td>
<td><a href="/solidworks-course-training">SolidWorks</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>17 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1755);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1346">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox72" class="checkbox" /><label for="checkbox72"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1312);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/923">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox73" class="checkbox" /><label for="checkbox73"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1332);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/948">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox74" class="checkbox" /><label for="checkbox74"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1347);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/963">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox75" class="checkbox" /><label for="checkbox75"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1359);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/975">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox76" class="checkbox" /><label for="checkbox76"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1407);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1024">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="34" name="items[]" id="checkbox77" class="checkbox" /><label for="checkbox77"></label></td>
<td><a href="/mcitp-sql-server-2008-course-training">MCITP SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1435);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1050">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="40" name="items[]" id="checkbox78" class="checkbox" /><label for="checkbox78"></label></td>
<td><a href="/prince2-foundation-course-training">PRINCE2 Foundation</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1467);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1074">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="47" name="items[]" id="checkbox79" class="checkbox" /><label for="checkbox79"></label></td>
<td><a href="/ccna-certification-course-training">CCNA Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1505);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1110">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="844" name="items[]" id="checkbox80" class="checkbox" /><label for="checkbox80"></label></td>
<td><a href="/server-certification-course-training">Server+ Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1612);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1214">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox81" class="checkbox" /><label for="checkbox81"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1617);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1219">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="927" name="items[]" id="checkbox82" class="checkbox" /><label for="checkbox82"></label></td>
<td><a href="/rhcsa-certification-course-training">RHCSA Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1630);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1232">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="928" name="items[]" id="checkbox83" class="checkbox" /><label for="checkbox83"></label></td>
<td><a href="/rhcsa-certification-red-hat-system-administration-i-rh124-course-training">Red Hat System Administration I (RH124)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1635);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1237">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="929" name="items[]" id="checkbox84" class="checkbox" /><label for="checkbox84"></label></td>
<td><a href="/rhcsa-red-hat-system-administration-ii-rh135-course-training">Red Hat System Administration II (RH135)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1640);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1242">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="930" name="items[]" id="checkbox85" class="checkbox" /><label for="checkbox85"></label></td>
<td><a href="/rhce-red-hat-system-administration-iii-rh255-course-training">Red Hat System Administration III (RH255)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1645);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1247">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="879" name="items[]" id="checkbox86" class="checkbox" /><label for="checkbox86"></label></td>
<td><a href="/vb-dot-net-course-training">VB.NET</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>16 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1727);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1323">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox87" class="checkbox" /><label for="checkbox87"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1307);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/918">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="97" name="items[]" id="checkbox88" class="checkbox" /><label for="checkbox88"></label></td>
<td><a href="/oracle-database-introduction-to-sql-course-training">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1343);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/959">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="869" name="items[]" id="checkbox89" class="checkbox" /><label for="checkbox89"></label></td>
<td><a href="/revit-course-training">Revit</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1426);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1041">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox90" class="checkbox" /><label for="checkbox90"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a></td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1509);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1113">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox91" class="checkbox" /><label for="checkbox91"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1588);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1192">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox92" class="checkbox" /><label for="checkbox92"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1693);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1292">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="43" name="items[]" id="checkbox93" class="checkbox" /><label for="checkbox93"></label></td>
<td><a href="/a-certification-course-training">A+ Certification</a></td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1722);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1316">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox94" class="checkbox" /><label for="checkbox94"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>15 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1757);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1348">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox95" class="checkbox" /><label for="checkbox95"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>1</td>
<td>08 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1281);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/890">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="848" name="items[]" id="checkbox96" class="checkbox" /><label for="checkbox96"></label></td>
<td><a href="/autocad-3d-course-training">AutoCAD 3D</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1387);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/998">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="34" name="items[]" id="checkbox97" class="checkbox" /><label for="checkbox97"></label></td>
<td><a href="/mcitp-sql-server-2008-course-training">MCITP SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1434);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1049">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox98" class="checkbox" /><label for="checkbox98"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1519);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1123">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="917" name="items[]" id="checkbox99" class="checkbox" /><label for="checkbox99"></label></td>
<td><a href="/mta-networking-and-security-fundamentals-exams-98-366-98-367-microsoft-technology-associate-certification-course-training">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1593);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1197">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="43" name="items[]" id="checkbox100" class="checkbox" /><label for="checkbox100"></label></td>
<td><a href="/a-certification-course-training">A+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1651);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1253">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox101" class="checkbox" /><label for="checkbox101"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1656);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1256">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox102" class="checkbox" /><label for="checkbox102"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sat, Sun</td>
<td>5</td>
<td>07 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1705);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1302">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="866" name="items[]" id="checkbox103" class="checkbox" /><label for="checkbox103"></label></td>
<td><a href="/jquery-mobile-development-course-training">jQuery Mobile Development</a>(KK-301)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>06 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1199);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/774">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="863" name="items[]" id="checkbox104" class="checkbox" /><label for="checkbox104"></label></td>
<td><a href="/html-email-newsletter-course-training">HTML Email Newsletter</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>1</td>
<td>06 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1652);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1254">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="885" name="items[]" id="checkbox105" class="checkbox" /><label for="checkbox105"></label></td>
<td><a href="/rapidweaver-course-training">RapidWeaver</a></td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>03 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1196);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/771">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="871" name="items[]" id="checkbox106" class="checkbox" /><label for="checkbox106"></label></td>
<td><a href="/moodle-course-training">Moodle</a>(KK-302)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>03 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1202);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/777">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="827" name="items[]" id="checkbox107" class="checkbox" /><label for="checkbox107"></label></td>
<td><a href="/magento-course-training">Magento</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>03 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1214);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/790">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="831" name="items[]" id="checkbox108" class="checkbox" /><label for="checkbox108"></label></td>
<td><a href="/zen-cart-course-training">Zen Cart</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>03 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1220);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/796">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="902" name="items[]" id="checkbox109" class="checkbox" /><label for="checkbox109"></label></td>
<td><a href="/advanced-sugarcrm-administrators-course-training">SugarCRM for Administrators</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>03 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1704);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1301">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="830" name="items[]" id="checkbox110" class="checkbox" /><label for="checkbox110"></label></td>
<td><a href="/sugarcrm-course-training">SugarCRM</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1223);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/799">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="869" name="items[]" id="checkbox111" class="checkbox" /><label for="checkbox111"></label></td>
<td><a href="/revit-course-training">Revit</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1297);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/908">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="38" name="items[]" id="checkbox112" class="checkbox" /><label for="checkbox112"></label></td>
<td><a href="/autocad-certification-course-training">AutoCAD 2D</a></td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1298);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/909">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox113" class="checkbox" /><label for="checkbox113"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a>(KK-301)</td>
<td style="text-align: center;">16</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1306);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/917">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox114" class="checkbox" /><label for="checkbox114"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1311);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/922">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="97" name="items[]" id="checkbox115" class="checkbox" /><label for="checkbox115"></label></td>
<td><a href="/oracle-database-introduction-to-sql-course-training">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1342);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/958">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox116" class="checkbox" /><label for="checkbox116"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1346);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/962">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox117" class="checkbox" /><label for="checkbox117"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1352);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/968">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox118" class="checkbox" /><label for="checkbox118"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1358);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/974">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox119" class="checkbox" /><label for="checkbox119"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1406);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1023">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="906" name="items[]" id="checkbox120" class="checkbox" /><label for="checkbox120"></label></td>
<td><a href="/magento-theme-design-course-training">Magento Theme Design</a></td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1415);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1032">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox121" class="checkbox" /><label for="checkbox121"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1425);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1040">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="7" name="items[]" id="checkbox122" class="checkbox" /><label for="checkbox122"></label></td>
<td><a href="/mcts-sql-server-2008-course-training">MCTS SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1430);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1045">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="823" name="items[]" id="checkbox123" class="checkbox" /><label for="checkbox123"></label></td>
<td><a href="/after-effects-course-training">After Effects</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1459);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1066">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="42" name="items[]" id="checkbox124" class="checkbox" /><label for="checkbox124"></label></td>
<td><a href="/prince2-course-training">PRINCE2</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1462);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1069">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="40" name="items[]" id="checkbox125" class="checkbox" /><label for="checkbox125"></label></td>
<td><a href="/prince2-foundation-course-training">PRINCE2 Foundation</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1465);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1072">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="868" name="items[]" id="checkbox126" class="checkbox" /><label for="checkbox126"></label></td>
<td><a href="/oracle-java-ee6-developing-web-services-certification-course-training">Java EE 6 Developing Web Services Using Java Technology</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1470);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1077">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="894" name="items[]" id="checkbox127" class="checkbox" /><label for="checkbox127"></label></td>
<td><a href="/mcsa-2012-windows-server-certification-course-training">MCSA Windows Server 2012 Certification</a></td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1488);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1094">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox128" class="checkbox" /><label for="checkbox128"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1587);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1191">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="83" name="items[]" id="checkbox129" class="checkbox" /><label for="checkbox129"></label></td>
<td><a href="/social-media-marketing-course-training">Social Media Marketing</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1660);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1259">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="41" name="items[]" id="checkbox130" class="checkbox" /><label for="checkbox130"></label></td>
<td><a href="/prince2-practitioner-course-training">PRINCE2 Practitioner</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1680);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1279">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="107" name="items[]" id="checkbox131" class="checkbox" /><label for="checkbox131"></label></td>
<td><a href="/xml-fundamentals-oracle-11g-course-training">XML Fundamentals (Oracle 11g)</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1684);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1283">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="108" name="items[]" id="checkbox132" class="checkbox" /><label for="checkbox132"></label></td>
<td><a href="/use-xml-db-oracle-database-11g-course-training">Use XML DB (Oracle Database 11g)</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Mon, Tue, Wed</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1687);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1286">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="875" name="items[]" id="checkbox133" class="checkbox" /><label for="checkbox133"></label></td>
<td><a href="/python-course-training">Python</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1690);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1289">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="924" name="items[]" id="checkbox134" class="checkbox" /><label for="checkbox134"></label></td>
<td><a href="/introduction-to-programming-course-training">Introduction to Programming</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1695);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1293">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="881" name="items[]" id="checkbox135" class="checkbox" /><label for="checkbox135"></label></td>
<td><a href="/solidworks-course-training">SolidWorks</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1698);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1295">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="933" name="items[]" id="checkbox136" class="checkbox" /><label for="checkbox136"></label></td>
<td><a href="/zbrush-course-training">Zbrush</a>(KK-302)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Wed</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1708);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1305">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox137" class="checkbox" /><label for="checkbox137"></label></td>
<td><a href="/advanced-php-course-training">Advanced PHP</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1712);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1309">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="879" name="items[]" id="checkbox138" class="checkbox" /><label for="checkbox138"></label></td>
<td><a href="/vb-dot-net-course-training">VB.NET</a></td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1718);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1314">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="931" name="items[]" id="checkbox139" class="checkbox" /><label for="checkbox139"></label></td>
<td><a href="/axure-rp-course-training">Axure RP</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1729);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1324">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="817" name="items[]" id="checkbox140" class="checkbox" /><label for="checkbox140"></label></td>
<td><a href="/oracle-java-se7-programming-ocp-certification-course-training">Java SE7 Programming</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1735);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1329">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox141" class="checkbox" /><label for="checkbox141"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-302)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1738);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1332">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="937" name="items[]" id="checkbox142" class="checkbox" /><label for="checkbox142"></label></td>
<td><a href="/mcsa-sql-server-2012-certification-course-training">MCSA SQL Server 2012 Certification</a>(KK-305)</td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1748);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1339">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox143" class="checkbox" /><label for="checkbox143"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-305)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>02 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1751);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1342">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="832" name="items[]" id="checkbox144" class="checkbox" /><label for="checkbox144"></label></td>
<td><a href="/oscommerce-course-training">osCommerce</a>(KK-301)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1207);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/782">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox145" class="checkbox" /><label for="checkbox145"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1294);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/905">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="884" name="items[]" id="checkbox146" class="checkbox" /><label for="checkbox146"></label></td>
<td><a href="/sketchup-course-training">Google SketchUp</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1302);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/913">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox147" class="checkbox" /><label for="checkbox147"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1331);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/947">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="872" name="items[]" id="checkbox148" class="checkbox" /><label for="checkbox148"></label></td>
<td><a href="/drupal-course-training">Drupal</a></td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1417);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1034">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="874" name="items[]" id="checkbox149" class="checkbox" /><label for="checkbox149"></label></td>
<td><a href="/phonegap-course-training">PhoneGap</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1441);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1055">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="886" name="items[]" id="checkbox150" class="checkbox" /><label for="checkbox150"></label></td>
<td><a href="/advanced-drupal-course-training">Advanced Drupal</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1481);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1087">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="888" name="items[]" id="checkbox151" class="checkbox" /><label for="checkbox151"></label></td>
<td><a href="/drupal-theme-development-course-training">Drupal Theme Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1484);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1090">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="887" name="items[]" id="checkbox152" class="checkbox" /><label for="checkbox152"></label></td>
<td><a href="/drupal-module-programming-development-course-training">Drupal Module Programming</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1487);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1093">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="897" name="items[]" id="checkbox153" class="checkbox" /><label for="checkbox153"></label></td>
<td><a href="/mcsd-web-applications-certification-course-training">MCSD: Web Applications Certification</a>(KK-305)</td>
<td style="text-align: center;">12</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1501);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1106">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="47" name="items[]" id="checkbox154" class="checkbox" /><label for="checkbox154"></label></td>
<td><a href="/ccna-certification-course-training">CCNA Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1504);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1109">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="860" name="items[]" id="checkbox155" class="checkbox" /><label for="checkbox155"></label></td>
<td><a href="/mobile-user-experience-design-course-training">Mobile User Experience Design</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1522);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1126">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="836" name="items[]" id="checkbox156" class="checkbox" /><label for="checkbox156"></label></td>
<td><a href="/premiere-pro-course-training">Adobe Premiere Pro</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1525);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1129">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="56" name="items[]" id="checkbox157" class="checkbox" /><label for="checkbox157"></label></td>
<td><a href="/web-accessibility-usability-course-training">Web Accessibility & Usability</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1536);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1140">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="898" name="items[]" id="checkbox158" class="checkbox" /><label for="checkbox158"></label></td>
<td><a href="/exam-70-480-programming-in-html5-with-javascript-and-css3-course-training">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1539);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1143">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="910" name="items[]" id="checkbox159" class="checkbox" /><label for="checkbox159"></label></td>
<td><a href="/interconnecting-cisco-networking-devices-part-1-icnd1-course-training">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1544);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1148">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="48" name="items[]" id="checkbox160" class="checkbox" /><label for="checkbox160"></label></td>
<td><a href="/ccnp-certification-course-training">CCNP Certification</a>(KK-305)</td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1549);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1153">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox161" class="checkbox" /><label for="checkbox161"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1555);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1159">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="916" name="items[]" id="checkbox162" class="checkbox" /><label for="checkbox162"></label></td>
<td><a href="/mcts-microsoft-sql-server-2008-business-intelligence-development-and-maintenance1-course-training">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1560);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1164">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="100" name="items[]" id="checkbox163" class="checkbox" /><label for="checkbox163"></label></td>
<td><a href="/mysql-for-beginners-sql-4401-course-training">MySQL for Beginners SQL-4401</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1565);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1169">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="918" name="items[]" id="checkbox164" class="checkbox" /><label for="checkbox164"></label></td>
<td><a href="/mta-database-administration-fundamentals-exam-98-364-microsoft-technology-associate-certification-course-training">Exam 98-364: MTA Database Administration Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1570);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1174">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox165" class="checkbox" /><label for="checkbox165"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1575);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1179">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox166" class="checkbox" /><label for="checkbox166"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1581);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1185">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="923" name="items[]" id="checkbox167" class="checkbox" /><label for="checkbox167"></label></td>
<td><a href="/wordpress-theme-development-design-course-training">WordPress Theme Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1598);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1202">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="922" name="items[]" id="checkbox168" class="checkbox" /><label for="checkbox168"></label></td>
<td><a href="/wordpress-plugin-development-programming-course-training">WordPress Plugin Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1601);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1205">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="844" name="items[]" id="checkbox169" class="checkbox" /><label for="checkbox169"></label></td>
<td><a href="/server-certification-course-training">Server+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1611);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1213">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox170" class="checkbox" /><label for="checkbox170"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1616);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1218">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox171" class="checkbox" /><label for="checkbox171"></label></td>
<td><a href="/android-programming-course-training">Android Programming</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1621);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1223">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="927" name="items[]" id="checkbox172" class="checkbox" /><label for="checkbox172"></label></td>
<td><a href="/rhcsa-certification-course-training">RHCSA Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1629);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1231">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="928" name="items[]" id="checkbox173" class="checkbox" /><label for="checkbox173"></label></td>
<td><a href="/rhcsa-certification-red-hat-system-administration-i-rh124-course-training">Red Hat System Administration I (RH124)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1634);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1236">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="929" name="items[]" id="checkbox174" class="checkbox" /><label for="checkbox174"></label></td>
<td><a href="/rhcsa-red-hat-system-administration-ii-rh135-course-training">Red Hat System Administration II (RH135)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1639);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1241">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="930" name="items[]" id="checkbox175" class="checkbox" /><label for="checkbox175"></label></td>
<td><a href="/rhce-red-hat-system-administration-iii-rh255-course-training">Red Hat System Administration III (RH255)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1644);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1246">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="39" name="items[]" id="checkbox176" class="checkbox" /><label for="checkbox176"></label></td>
<td><a href="/3d-studio-max-course-training">3d Studio Max</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1671);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1269">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="901" name="items[]" id="checkbox177" class="checkbox" /><label for="checkbox177"></label></td>
<td><a href="/grasshopper-3d-course-training">Grasshopper 3d</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1677);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1276">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="816" name="items[]" id="checkbox178" class="checkbox" /><label for="checkbox178"></label></td>
<td><a href="/oracle-java-se7-fundamentals-oca-certification-course-training">Java SE7 Fundamentals</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1697);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1294">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="904" name="items[]" id="checkbox179" class="checkbox" /><label for="checkbox179"></label></td>
<td><a href="/advanced-salesforce-administrators-course-training">Salesforce for Administrators</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1702);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1299">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="903" name="items[]" id="checkbox180" class="checkbox" /><label for="checkbox180"></label></td>
<td><a href="/salesforce-course-training">Salesforce</a></td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1709);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1306">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="867" name="items[]" id="checkbox181" class="checkbox" /><label for="checkbox181"></label></td>
<td><a href="/oracle-java-ee6-jsp-web-development-certification-course-training">Java EE 6 Web Component Development with Servlets & JSPs</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>01 Sep 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1745);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1336">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox182" class="checkbox" /><label for="checkbox182"></label></td>
<td><a href="/android-programming-course-training">Android Programming</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>2</td>
<td>25 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1476);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1083">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="38" name="items[]" id="checkbox183" class="checkbox" /><label for="checkbox183"></label></td>
<td><a href="/autocad-certification-course-training">AutoCAD 2D</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>24 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 996);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/535">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="97" name="items[]" id="checkbox184" class="checkbox" /><label for="checkbox184"></label></td>
<td><a href="/oracle-database-introduction-to-sql-course-training">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>24 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1340);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/956">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox185" class="checkbox" /><label for="checkbox185"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>24 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1345);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/961">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="64" name="items[]" id="checkbox186" class="checkbox" /><label for="checkbox186"></label></td>
<td><a href="/flash-course-training">Adobe Flash</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>24 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1391);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1003">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="879" name="items[]" id="checkbox187" class="checkbox" /><label for="checkbox187"></label></td>
<td><a href="/vb-dot-net-course-training">VB.NET</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>24 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1533);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1137">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="832" name="items[]" id="checkbox188" class="checkbox" /><label for="checkbox188"></label></td>
<td><a href="/oscommerce-course-training">osCommerce</a>(KK-301)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>23 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1206);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/781">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="860" name="items[]" id="checkbox189" class="checkbox" /><label for="checkbox189"></label></td>
<td><a href="/mobile-user-experience-design-course-training">Mobile User Experience Design</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>23 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1521);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1125">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="56" name="items[]" id="checkbox190" class="checkbox" /><label for="checkbox190"></label></td>
<td><a href="/web-accessibility-usability-course-training">Web Accessibility & Usability</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>23 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1535);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1139">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="931" name="items[]" id="checkbox191" class="checkbox" /><label for="checkbox191"></label></td>
<td><a href="/axure-rp-course-training">Axure RP</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>23 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1650);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1252">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="831" name="items[]" id="checkbox192" class="checkbox" /><label for="checkbox192"></label></td>
<td><a href="/zen-cart-course-training">Zen Cart</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Thu, Fri</td>
<td>0</td>
<td>22 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1219);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/795">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="905" name="items[]" id="checkbox193" class="checkbox" /><label for="checkbox193"></label></td>
<td><a href="/magento-development-course-training">Magento development</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Wed, Thu, Fri</td>
<td>0</td>
<td>21 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1760);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1351">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="863" name="items[]" id="checkbox194" class="checkbox" /><label for="checkbox194"></label></td>
<td><a href="/html-email-newsletter-course-training">HTML Email Newsletter</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>20 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1226);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/802">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox195" class="checkbox" /><label for="checkbox195"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>20 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1405);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1022">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="903" name="items[]" id="checkbox196" class="checkbox" /><label for="checkbox196"></label></td>
<td><a href="/salesforce-course-training">Salesforce</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>20 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1701);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1298">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="828" name="items[]" id="checkbox197" class="checkbox" /><label for="checkbox197"></label></td>
<td><a href="/joomla-course-training">Joomla!</a></td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>20 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1710);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1307">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="816" name="items[]" id="checkbox198" class="checkbox" /><label for="checkbox198"></label></td>
<td><a href="/oracle-java-se7-fundamentals-oca-certification-course-training">Java SE7 Fundamentals</a></td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1288);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/899">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="817" name="items[]" id="checkbox199" class="checkbox" /><label for="checkbox199"></label></td>
<td><a href="/oracle-java-se7-programming-ocp-certification-course-training">Java SE7 Programming</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1291);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/902">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox200" class="checkbox" /><label for="checkbox200"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1293);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/904">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="869" name="items[]" id="checkbox201" class="checkbox" /><label for="checkbox201"></label></td>
<td><a href="/revit-course-training">Revit</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1296);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/907">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="848" name="items[]" id="checkbox202" class="checkbox" /><label for="checkbox202"></label></td>
<td><a href="/autocad-3d-course-training">AutoCAD 3D</a></td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1299);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/910">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="884" name="items[]" id="checkbox203" class="checkbox" /><label for="checkbox203"></label></td>
<td><a href="/sketchup-course-training">Google SketchUp</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1301);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/912">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox204" class="checkbox" /><label for="checkbox204"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a>(KK-301)</td>
<td style="text-align: center;">16</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1305);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/916">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox205" class="checkbox" /><label for="checkbox205"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1310);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/921">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox206" class="checkbox" /><label for="checkbox206"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1330);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/946">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="904" name="items[]" id="checkbox207" class="checkbox" /><label for="checkbox207"></label></td>
<td><a href="/advanced-salesforce-administrators-course-training">Salesforce for Administrators</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1377);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/996">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="872" name="items[]" id="checkbox208" class="checkbox" /><label for="checkbox208"></label></td>
<td><a href="/drupal-course-training">Drupal</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1416);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1033">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox209" class="checkbox" /><label for="checkbox209"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1424);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1039">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="7" name="items[]" id="checkbox210" class="checkbox" /><label for="checkbox210"></label></td>
<td><a href="/mcts-sql-server-2008-course-training">MCTS SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1429);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1044">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="34" name="items[]" id="checkbox211" class="checkbox" /><label for="checkbox211"></label></td>
<td><a href="/mcitp-sql-server-2008-course-training">MCITP SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">16</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1433);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1048">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="874" name="items[]" id="checkbox212" class="checkbox" /><label for="checkbox212"></label></td>
<td><a href="/phonegap-course-training">PhoneGap</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1440);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1054">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="43" name="items[]" id="checkbox213" class="checkbox" /><label for="checkbox213"></label></td>
<td><a href="/a-certification-course-training">A+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1453);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1060">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="823" name="items[]" id="checkbox214" class="checkbox" /><label for="checkbox214"></label></td>
<td><a href="/after-effects-course-training">After Effects</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1458);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1065">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="42" name="items[]" id="checkbox215" class="checkbox" /><label for="checkbox215"></label></td>
<td><a href="/prince2-course-training">PRINCE2</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1461);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1068">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="40" name="items[]" id="checkbox216" class="checkbox" /><label for="checkbox216"></label></td>
<td><a href="/prince2-foundation-course-training">PRINCE2 Foundation</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1464);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1071">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="41" name="items[]" id="checkbox217" class="checkbox" /><label for="checkbox217"></label></td>
<td><a href="/prince2-practitioner-course-training">PRINCE2 Practitioner</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1468);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1075">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="867" name="items[]" id="checkbox218" class="checkbox" /><label for="checkbox218"></label></td>
<td><a href="/oracle-java-ee6-jsp-web-development-certification-course-training">Java EE 6 Web Component Development with Servlets & JSPs</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1473);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1080">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="894" name="items[]" id="checkbox219" class="checkbox" /><label for="checkbox219"></label></td>
<td><a href="/mcsa-2012-windows-server-certification-course-training">MCSA Windows Server 2012 Certification</a>(KK-305)</td>
<td style="text-align: center;">30</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1477);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1084">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="886" name="items[]" id="checkbox220" class="checkbox" /><label for="checkbox220"></label></td>
<td><a href="/advanced-drupal-course-training">Advanced Drupal</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1480);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1086">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="888" name="items[]" id="checkbox221" class="checkbox" /><label for="checkbox221"></label></td>
<td><a href="/drupal-theme-development-course-training">Drupal Theme Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1483);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1089">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="887" name="items[]" id="checkbox222" class="checkbox" /><label for="checkbox222"></label></td>
<td><a href="/drupal-module-programming-development-course-training">Drupal Module Programming</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1486);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1092">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="39" name="items[]" id="checkbox223" class="checkbox" /><label for="checkbox223"></label></td>
<td><a href="/3d-studio-max-course-training">3d Studio Max</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1492);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1098">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="897" name="items[]" id="checkbox224" class="checkbox" /><label for="checkbox224"></label></td>
<td><a href="/mcsd-web-applications-certification-course-training">MCSD: Web Applications Certification</a>(KK-305)</td>
<td style="text-align: center;">12</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1500);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1105">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="47" name="items[]" id="checkbox225" class="checkbox" /><label for="checkbox225"></label></td>
<td><a href="/ccna-certification-course-training">CCNA Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1503);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1108">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox226" class="checkbox" /><label for="checkbox226"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1518);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1122">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="836" name="items[]" id="checkbox227" class="checkbox" /><label for="checkbox227"></label></td>
<td><a href="/premiere-pro-course-training">Adobe Premiere Pro</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1524);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1128">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="898" name="items[]" id="checkbox228" class="checkbox" /><label for="checkbox228"></label></td>
<td><a href="/exam-70-480-programming-in-html5-with-javascript-and-css3-course-training">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1538);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1142">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="910" name="items[]" id="checkbox229" class="checkbox" /><label for="checkbox229"></label></td>
<td><a href="/interconnecting-cisco-networking-devices-part-1-icnd1-course-training">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1543);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1147">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="48" name="items[]" id="checkbox230" class="checkbox" /><label for="checkbox230"></label></td>
<td><a href="/ccnp-certification-course-training">CCNP Certification</a>(KK-305)</td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1548);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1152">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox231" class="checkbox" /><label for="checkbox231"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1554);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1158">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="916" name="items[]" id="checkbox232" class="checkbox" /><label for="checkbox232"></label></td>
<td><a href="/mcts-microsoft-sql-server-2008-business-intelligence-development-and-maintenance1-course-training">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1559);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1163">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="100" name="items[]" id="checkbox233" class="checkbox" /><label for="checkbox233"></label></td>
<td><a href="/mysql-for-beginners-sql-4401-course-training">MySQL for Beginners SQL-4401</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1564);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1168">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="918" name="items[]" id="checkbox234" class="checkbox" /><label for="checkbox234"></label></td>
<td><a href="/mta-database-administration-fundamentals-exam-98-364-microsoft-technology-associate-certification-course-training">Exam 98-364: MTA Database Administration Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Mon, Tue, Wed</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1569);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1173">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox235" class="checkbox" /><label for="checkbox235"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1574);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1178">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox236" class="checkbox" /><label for="checkbox236"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1580);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1184">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox237" class="checkbox" /><label for="checkbox237"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1586);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1190">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="917" name="items[]" id="checkbox238" class="checkbox" /><label for="checkbox238"></label></td>
<td><a href="/mta-networking-and-security-fundamentals-exams-98-366-98-367-microsoft-technology-associate-certification-course-training">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1592);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1196">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="923" name="items[]" id="checkbox239" class="checkbox" /><label for="checkbox239"></label></td>
<td><a href="/wordpress-theme-development-design-course-training">WordPress Theme Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1597);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1201">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="922" name="items[]" id="checkbox240" class="checkbox" /><label for="checkbox240"></label></td>
<td><a href="/wordpress-plugin-development-programming-course-training">WordPress Plugin Development</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1600);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1204">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox241" class="checkbox" /><label for="checkbox241"></label></td>
<td><a href="/android-programming-course-training">Android Programming</a></td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1608);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1210">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="844" name="items[]" id="checkbox242" class="checkbox" /><label for="checkbox242"></label></td>
<td><a href="/server-certification-course-training">Server+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1610);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1212">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox243" class="checkbox" /><label for="checkbox243"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1615);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1217">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="927" name="items[]" id="checkbox244" class="checkbox" /><label for="checkbox244"></label></td>
<td><a href="/rhcsa-certification-course-training">RHCSA Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1628);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1230">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="928" name="items[]" id="checkbox245" class="checkbox" /><label for="checkbox245"></label></td>
<td><a href="/rhcsa-certification-red-hat-system-administration-i-rh124-course-training">Red Hat System Administration I (RH124)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1633);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1235">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="929" name="items[]" id="checkbox246" class="checkbox" /><label for="checkbox246"></label></td>
<td><a href="/rhcsa-red-hat-system-administration-ii-rh135-course-training">Red Hat System Administration II (RH135)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1638);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1240">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="930" name="items[]" id="checkbox247" class="checkbox" /><label for="checkbox247"></label></td>
<td><a href="/rhce-red-hat-system-administration-iii-rh255-course-training">Red Hat System Administration III (RH255)</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1643);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1245">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox248" class="checkbox" /><label for="checkbox248"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1655);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1255">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="901" name="items[]" id="checkbox249" class="checkbox" /><label for="checkbox249"></label></td>
<td><a href="/grasshopper-3d-course-training">Grasshopper 3d</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1676);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1275">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="107" name="items[]" id="checkbox250" class="checkbox" /><label for="checkbox250"></label></td>
<td><a href="/xml-fundamentals-oracle-11g-course-training">XML Fundamentals (Oracle 11g)</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1683);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1282">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="902" name="items[]" id="checkbox251" class="checkbox" /><label for="checkbox251"></label></td>
<td><a href="/advanced-sugarcrm-administrators-course-training">SugarCRM for Administrators</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1703);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1300">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="933" name="items[]" id="checkbox252" class="checkbox" /><label for="checkbox252"></label></td>
<td><a href="/zbrush-course-training">Zbrush</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1707);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1304">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="3" name="items[]" id="checkbox253" class="checkbox" /><label for="checkbox253"></label></td>
<td><a href="/php-course-training">PHP</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1728);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/696">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox254" class="checkbox" /><label for="checkbox254"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1737);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1331">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="937" name="items[]" id="checkbox255" class="checkbox" /><label for="checkbox255"></label></td>
<td><a href="/mcsa-sql-server-2012-certification-course-training">MCSA SQL Server 2012 Certification</a>(KK-305)</td>
<td style="text-align: center;">30</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>19 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1747);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1338">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="866" name="items[]" id="checkbox256" class="checkbox" /><label for="checkbox256"></label></td>
<td><a href="/jquery-mobile-development-course-training">jQuery Mobile Development</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1190);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/773">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="885" name="items[]" id="checkbox257" class="checkbox" /><label for="checkbox257"></label></td>
<td><a href="/rapidweaver-course-training">RapidWeaver</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1195);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/770">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="866" name="items[]" id="checkbox258" class="checkbox" /><label for="checkbox258"></label></td>
<td><a href="/jquery-mobile-development-course-training">jQuery Mobile Development</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1198);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/773">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="871" name="items[]" id="checkbox259" class="checkbox" /><label for="checkbox259"></label></td>
<td><a href="/moodle-course-training">Moodle</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1201);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/776">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="827" name="items[]" id="checkbox260" class="checkbox" /><label for="checkbox260"></label></td>
<td><a href="/magento-course-training">Magento</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1204);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/779">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="830" name="items[]" id="checkbox261" class="checkbox" /><label for="checkbox261"></label></td>
<td><a href="/sugarcrm-course-training">SugarCRM</a></td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1222);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/798">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox262" class="checkbox" /><label for="checkbox262"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1351);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/967">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox263" class="checkbox" /><label for="checkbox263"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1357);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/973">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="906" name="items[]" id="checkbox264" class="checkbox" /><label for="checkbox264"></label></td>
<td><a href="/magento-theme-design-course-training">Magento Theme Design</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1394);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1006">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="868" name="items[]" id="checkbox265" class="checkbox" /><label for="checkbox265"></label></td>
<td><a href="/oracle-java-ee6-developing-web-services-certification-course-training">Java EE 6 Developing Web Services Using Java Technology</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1469);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1076">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="881" name="items[]" id="checkbox266" class="checkbox" /><label for="checkbox266"></label></td>
<td><a href="/solidworks-course-training">SolidWorks</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1512);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1116">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="924" name="items[]" id="checkbox267" class="checkbox" /><label for="checkbox267"></label></td>
<td><a href="/introduction-to-programming-course-training">Introduction to Programming</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1604);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1208">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox268" class="checkbox" /><label for="checkbox268"></label></td>
<td><a href="/advanced-php-course-training">Advanced PHP</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1626);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1228">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="83" name="items[]" id="checkbox269" class="checkbox" /><label for="checkbox269"></label></td>
<td><a href="/social-media-marketing-course-training">Social Media Marketing</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1659);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1258">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="108" name="items[]" id="checkbox270" class="checkbox" /><label for="checkbox270"></label></td>
<td><a href="/use-xml-db-oracle-database-11g-course-training">Use XML DB (Oracle Database 11g)</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1686);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1285">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="875" name="items[]" id="checkbox271" class="checkbox" /><label for="checkbox271"></label></td>
<td><a href="/python-course-training">Python</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1689);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1288">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox272" class="checkbox" /><label for="checkbox272"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-301)</td>
<td style="text-align: center;">20</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>18 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1750);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1341">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="869" name="items[]" id="checkbox273" class="checkbox" /><label for="checkbox273"></label></td>
<td><a href="/revit-course-training">Revit</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>10 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1295);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/906">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="920" name="items[]" id="checkbox274" class="checkbox" /><label for="checkbox274"></label></td>
<td><a href="/mta-web-development-fundamentals-and-html5-application-development-fundamentals-exams-98-363--98-375-microsoft-technology-associate-certification-course-training">Exams 98-363 & 98-375: MTA Web Development Fundamentals and HTML5 Application Development Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>10 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1585);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1189">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="885" name="items[]" id="checkbox275" class="checkbox" /><label for="checkbox275"></label></td>
<td><a href="/rapidweaver-course-training">RapidWeaver</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1194);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/769">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="871" name="items[]" id="checkbox276" class="checkbox" /><label for="checkbox276"></label></td>
<td><a href="/moodle-course-training">Moodle</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1200);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/775">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="828" name="items[]" id="checkbox277" class="checkbox" /><label for="checkbox277"></label></td>
<td><a href="/joomla-course-training">Joomla!</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1216);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/792">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="37" name="items[]" id="checkbox278" class="checkbox" /><label for="checkbox278"></label></td>
<td><a href="/microsoft-office-specialist-course-training">Microsoft Office Specialist</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1350);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/966">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="882" name="items[]" id="checkbox279" class="checkbox" /><label for="checkbox279"></label></td>
<td><a href="/vectorworks-course-training">Vectorworks</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1356);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/972">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="859" name="items[]" id="checkbox280" class="checkbox" /><label for="checkbox280"></label></td>
<td><a href="/ms-microsoft-office-course-training">Microsoft Office for Beginners</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Fri</td>
<td>0</td>
<td>09 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1404);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1021">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="83" name="items[]" id="checkbox281" class="checkbox" /><label for="checkbox281"></label></td>
<td><a href="/social-media-marketing-course-training">Social Media Marketing</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Thu</td>
<td>0</td>
<td>08 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1658);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1257">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="866" name="items[]" id="checkbox282" class="checkbox" /><label for="checkbox282"></label></td>
<td><a href="/jquery-mobile-development-course-training">jQuery Mobile Development</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1197);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/772">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="832" name="items[]" id="checkbox283" class="checkbox" /><label for="checkbox283"></label></td>
<td><a href="/oscommerce-course-training">osCommerce</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1205);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/780">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox284" class="checkbox" /><label for="checkbox284"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1213);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/789">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="830" name="items[]" id="checkbox285" class="checkbox" /><label for="checkbox285"></label></td>
<td><a href="/sugarcrm-course-training">SugarCRM</a></td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1221);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/797">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="97" name="items[]" id="checkbox286" class="checkbox" /><label for="checkbox286"></label></td>
<td><a href="/oracle-database-introduction-to-sql-course-training">Oracle Database: SQL Certified Expert (Introduction to SQL) Certification</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1339);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/955">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="880" name="items[]" id="checkbox287" class="checkbox" /><label for="checkbox287"></label></td>
<td><a href="/rhino-course-training">Rhino</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1344);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/960">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="904" name="items[]" id="checkbox288" class="checkbox" /><label for="checkbox288"></label></td>
<td><a href="/advanced-salesforce-administrators-course-training">Salesforce for Administrators</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1375);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/994">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="906" name="items[]" id="checkbox289" class="checkbox" /><label for="checkbox289"></label></td>
<td><a href="/magento-theme-design-course-training">Magento Theme Design</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1392);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1004">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="874" name="items[]" id="checkbox290" class="checkbox" /><label for="checkbox290"></label></td>
<td><a href="/phonegap-course-training">PhoneGap</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1438);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1052">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="872" name="items[]" id="checkbox291" class="checkbox" /><label for="checkbox291"></label></td>
<td><a href="/drupal-course-training">Drupal</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1478);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/785">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="886" name="items[]" id="checkbox292" class="checkbox" /><label for="checkbox292"></label></td>
<td><a href="/advanced-drupal-course-training">Advanced Drupal</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1479);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1085">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="888" name="items[]" id="checkbox293" class="checkbox" /><label for="checkbox293"></label></td>
<td><a href="/drupal-theme-development-course-training">Drupal Theme Development</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1482);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1088">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="887" name="items[]" id="checkbox294" class="checkbox" /><label for="checkbox294"></label></td>
<td><a href="/drupal-module-programming-development-course-training">Drupal Module Programming</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1485);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1091">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="13" name="items[]" id="checkbox295" class="checkbox" /><label for="checkbox295"></label></td>
<td><a href="/mcpd-web-developer-course-training">MCPD Web Developer</a>(KK-305)</td>
<td style="text-align: center;">30</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1496);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1101">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="897" name="items[]" id="checkbox296" class="checkbox" /><label for="checkbox296"></label></td>
<td><a href="/mcsd-web-applications-certification-course-training">MCSD: Web Applications Certification</a>(KK-305)</td>
<td style="text-align: center;">24</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1499);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1104">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="47" name="items[]" id="checkbox297" class="checkbox" /><label for="checkbox297"></label></td>
<td><a href="/ccna-certification-course-training">CCNA Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1502);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1107">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="923" name="items[]" id="checkbox298" class="checkbox" /><label for="checkbox298"></label></td>
<td><a href="/wordpress-theme-development-design-course-training">WordPress Theme Development</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1596);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1200">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="922" name="items[]" id="checkbox299" class="checkbox" /><label for="checkbox299"></label></td>
<td><a href="/wordpress-plugin-development-programming-course-training">WordPress Plugin Development</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1599);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1203">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="844" name="items[]" id="checkbox300" class="checkbox" /><label for="checkbox300"></label></td>
<td><a href="/server-certification-course-training">Server+ Certification</a>(KK-301)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1609);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1211">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="44" name="items[]" id="checkbox301" class="checkbox" /><label for="checkbox301"></label></td>
<td><a href="/network-n-certification-course-training">Network+ (N+) Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1614);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1216">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="927" name="items[]" id="checkbox302" class="checkbox" /><label for="checkbox302"></label></td>
<td><a href="/rhcsa-certification-course-training">RHCSA Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1627);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1229">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="928" name="items[]" id="checkbox303" class="checkbox" /><label for="checkbox303"></label></td>
<td><a href="/rhcsa-certification-red-hat-system-administration-i-rh124-course-training">Red Hat System Administration I (RH124)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1632);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1234">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="929" name="items[]" id="checkbox304" class="checkbox" /><label for="checkbox304"></label></td>
<td><a href="/rhcsa-red-hat-system-administration-ii-rh135-course-training">Red Hat System Administration II (RH135)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1637);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1239">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="930" name="items[]" id="checkbox305" class="checkbox" /><label for="checkbox305"></label></td>
<td><a href="/rhce-red-hat-system-administration-iii-rh255-course-training">Red Hat System Administration III (RH255)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>10:00 - 17:00 Tue, Fri</td>
<td>0</td>
<td>06 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1642);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1244">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="38" name="items[]" id="checkbox306" class="checkbox" /><label for="checkbox306"></label></td>
<td><a href="/autocad-certification-course-training">AutoCAD 2D</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 995);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/534">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="827" name="items[]" id="checkbox307" class="checkbox" /><label for="checkbox307"></label></td>
<td><a href="/magento-course-training">Magento</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1203);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/778">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="848" name="items[]" id="checkbox308" class="checkbox" /><label for="checkbox308"></label></td>
<td><a href="/autocad-3d-course-training">AutoCAD 3D</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1276);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/885">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox309" class="checkbox" /><label for="checkbox309"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1282);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/891">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="816" name="items[]" id="checkbox310" class="checkbox" /><label for="checkbox310"></label></td>
<td><a href="/oracle-java-se7-fundamentals-oca-certification-course-training">Java SE7 Fundamentals</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1286);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/897">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox311" class="checkbox" /><label for="checkbox311"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a></td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1292);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/903">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="884" name="items[]" id="checkbox312" class="checkbox" /><label for="checkbox312"></label></td>
<td><a href="/sketchup-course-training">Google SketchUp</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1300);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/911">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="10" name="items[]" id="checkbox313" class="checkbox" /><label for="checkbox313"></label></td>
<td><a href="/mcse-server-infrastructure-2012-certification-course-training">MCSE Server Infrastructure 2012 Certification</a>(KK-301)</td>
<td style="text-align: center;">40</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1327);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/943">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="868" name="items[]" id="checkbox314" class="checkbox" /><label for="checkbox314"></label></td>
<td><a href="/oracle-java-ee6-developing-web-services-certification-course-training">Java EE 6 Developing Web Services Using Java Technology</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1400);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1012">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox315" class="checkbox" /><label for="checkbox315"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1423);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1038">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="43" name="items[]" id="checkbox316" class="checkbox" /><label for="checkbox316"></label></td>
<td><a href="/a-certification-course-training">A+ Certification</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1450);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1057">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="867" name="items[]" id="checkbox317" class="checkbox" /><label for="checkbox317"></label></td>
<td><a href="/oracle-java-ee6-jsp-web-development-certification-course-training">Java EE 6 Web Component Development with Servlets & JSPs</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1471);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1078">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox318" class="checkbox" /><label for="checkbox318"></label></td>
<td><a href="/android-programming-course-training">Android Programming</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1475);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1082">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="39" name="items[]" id="checkbox319" class="checkbox" /><label for="checkbox319"></label></td>
<td><a href="/3d-studio-max-course-training">3d Studio Max</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1490);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1096">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="881" name="items[]" id="checkbox320" class="checkbox" /><label for="checkbox320"></label></td>
<td><a href="/solidworks-course-training">SolidWorks</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1510);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1114">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="860" name="items[]" id="checkbox321" class="checkbox" /><label for="checkbox321"></label></td>
<td><a href="/mobile-user-experience-design-course-training">Mobile User Experience Design</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1520);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1124">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="836" name="items[]" id="checkbox322" class="checkbox" /><label for="checkbox322"></label></td>
<td><a href="/premiere-pro-course-training">Adobe Premiere Pro</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1523);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1127">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="56" name="items[]" id="checkbox323" class="checkbox" /><label for="checkbox323"></label></td>
<td><a href="/web-accessibility-usability-course-training">Web Accessibility & Usability</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1534);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1138">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="898" name="items[]" id="checkbox324" class="checkbox" /><label for="checkbox324"></label></td>
<td><a href="/exam-70-480-programming-in-html5-with-javascript-and-css3-course-training">Exam 70-480: Programming in HTML5 with JavaScript and CSS3</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1537);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1141">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="910" name="items[]" id="checkbox325" class="checkbox" /><label for="checkbox325"></label></td>
<td><a href="/interconnecting-cisco-networking-devices-part-1-icnd1-course-training">CCNET: Interconnecting Cisco Networking Devices Part 1 (ICND1)</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1542);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1146">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="48" name="items[]" id="checkbox326" class="checkbox" /><label for="checkbox326"></label></td>
<td><a href="/ccnp-certification-course-training">CCNP Certification</a>(KK-305)</td>
<td style="text-align: center;">30</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1547);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1151">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox327" class="checkbox" /><label for="checkbox327"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1553);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1157">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="916" name="items[]" id="checkbox328" class="checkbox" /><label for="checkbox328"></label></td>
<td><a href="/mcts-microsoft-sql-server-2008-business-intelligence-development-and-maintenance1-course-training">MCTS Microsoft SQL Server 2008, Business Intelligence Development and Maintenance</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1558);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1162">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="100" name="items[]" id="checkbox329" class="checkbox" /><label for="checkbox329"></label></td>
<td><a href="/mysql-for-beginners-sql-4401-course-training">MySQL for Beginners SQL-4401</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1563);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1167">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="918" name="items[]" id="checkbox330" class="checkbox" /><label for="checkbox330"></label></td>
<td><a href="/mta-database-administration-fundamentals-exam-98-364-microsoft-technology-associate-certification-course-training">Exam 98-364: MTA Database Administration Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">6</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1568);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1172">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="919" name="items[]" id="checkbox331" class="checkbox" /><label for="checkbox331"></label></td>
<td><a href="/mta-windows-operating-system-and-windows-server-fundamentals-exams-98-349--98-365-microsoft-technology-associate-certification-course-training">Exams 98-349 & 98-365: MTA Windows Operating System and Windows Server Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1573);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1177">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="921" name="items[]" id="checkbox332" class="checkbox" /><label for="checkbox332"></label></td>
<td><a href="/mta-software-development-fundamentals-with-net-exams-98-361-and-98-372-microsoft-technology-associate-certification-course-training">Exams 98-361 and 98-372: MTA Software Development Fundamentals with .NET</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1579);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1183">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="917" name="items[]" id="checkbox333" class="checkbox" /><label for="checkbox333"></label></td>
<td><a href="/mta-networking-and-security-fundamentals-exams-98-366-98-367-microsoft-technology-associate-certification-course-training">Exams 98-366 & 98-367: MTA Networking and Security Fundamentals</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1591);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1195">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="924" name="items[]" id="checkbox334" class="checkbox" /><label for="checkbox334"></label></td>
<td><a href="/introduction-to-programming-course-training">Introduction to Programming</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1603);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1207">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox335" class="checkbox" /><label for="checkbox335"></label></td>
<td><a href="/advanced-php-course-training">Advanced PHP</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1622);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1224">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="901" name="items[]" id="checkbox336" class="checkbox" /><label for="checkbox336"></label></td>
<td><a href="/grasshopper-3d-course-training">Grasshopper 3d</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1675);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1274">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="108" name="items[]" id="checkbox337" class="checkbox" /><label for="checkbox337"></label></td>
<td><a href="/use-xml-db-oracle-database-11g-course-training">Use XML DB (Oracle Database 11g)</a>(KK-305)</td>
<td style="text-align: center;">6</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1685);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1284">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="875" name="items[]" id="checkbox338" class="checkbox" /><label for="checkbox338"></label></td>
<td><a href="/python-course-training">Python</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1688);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1287">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="903" name="items[]" id="checkbox339" class="checkbox" /><label for="checkbox339"></label></td>
<td><a href="/salesforce-course-training">Salesforce</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1700);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1297">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="936" name="items[]" id="checkbox340" class="checkbox" /><label for="checkbox340"></label></td>
<td><a href="/mcse-sql-server-2012-data-platform-certification-course-training">MCSE SQL Server 2012 Data Platform Certification</a>(KK-305)</td>
<td style="text-align: center;">40</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>05 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1749);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1340">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="894" name="items[]" id="checkbox341" class="checkbox" /><label for="checkbox341"></label></td>
<td><a href="/mcsa-2012-windows-server-certification-course-training">MCSA Windows Server 2012 Certification</a></td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1192);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/767">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="831" name="items[]" id="checkbox342" class="checkbox" /><label for="checkbox342"></label></td>
<td><a href="/zen-cart-course-training">Zen Cart</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1218);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/794">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="863" name="items[]" id="checkbox343" class="checkbox" /><label for="checkbox343"></label></td>
<td><a href="/html-email-newsletter-course-training">HTML Email Newsletter</a></td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1224);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/800">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="817" name="items[]" id="checkbox344" class="checkbox" /><label for="checkbox344"></label></td>
<td><a href="/oracle-java-se7-programming-ocp-certification-course-training">Java SE7 Programming</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1289);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/900">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="31" name="items[]" id="checkbox345" class="checkbox" /><label for="checkbox345"></label></td>
<td><a href="/oca-oracle-certified-associate-11g-course-training">OCA Oracle Certified Associate 11g Database</a>(KK-301)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1304);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/915">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="32" name="items[]" id="checkbox346" class="checkbox" /><label for="checkbox346"></label></td>
<td><a href="/oracle-certified-professional-ocp-11g-course-training">Oracle Certified Professional (OCP) 11g Database</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1309);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/920">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="902" name="items[]" id="checkbox347" class="checkbox" /><label for="checkbox347"></label></td>
<td><a href="/advanced-sugarcrm-administrators-course-training">SugarCRM for Administrators</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1370);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/989">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="7" name="items[]" id="checkbox348" class="checkbox" /><label for="checkbox348"></label></td>
<td><a href="/mcts-sql-server-2008-course-training">MCTS SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1428);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1043">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="34" name="items[]" id="checkbox349" class="checkbox" /><label for="checkbox349"></label></td>
<td><a href="/mcitp-sql-server-2008-course-training">MCITP SQL Server 2008</a>(KK-305)</td>
<td style="text-align: center;">8</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1432);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1047">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="823" name="items[]" id="checkbox350" class="checkbox" /><label for="checkbox350"></label></td>
<td><a href="/after-effects-course-training">After Effects</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1457);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1064">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="42" name="items[]" id="checkbox351" class="checkbox" /><label for="checkbox351"></label></td>
<td><a href="/prince2-course-training">PRINCE2</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1460);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1067">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="40" name="items[]" id="checkbox352" class="checkbox" /><label for="checkbox352"></label></td>
<td><a href="/prince2-foundation-course-training">PRINCE2 Foundation</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1463);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1070">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="41" name="items[]" id="checkbox353" class="checkbox" /><label for="checkbox353"></label></td>
<td><a href="/prince2-practitioner-course-training">PRINCE2 Practitioner</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1466);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1073">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox354" class="checkbox" /><label for="checkbox354"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1517);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1121">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="879" name="items[]" id="checkbox355" class="checkbox" /><label for="checkbox355"></label></td>
<td><a href="/vb-dot-net-course-training">VB.NET</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1531);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1135">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="931" name="items[]" id="checkbox356" class="checkbox" /><label for="checkbox356"></label></td>
<td><a href="/axure-rp-course-training">Axure RP</a>(KK-305)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1649);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1251">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="107" name="items[]" id="checkbox357" class="checkbox" /><label for="checkbox357"></label></td>
<td><a href="/xml-fundamentals-oracle-11g-course-training">XML Fundamentals (Oracle 11g)</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1681);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1280">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="933" name="items[]" id="checkbox358" class="checkbox" /><label for="checkbox358"></label></td>
<td><a href="/zbrush-course-training">Zbrush</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1706);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1303">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox359" class="checkbox" /><label for="checkbox359"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1736);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1330">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="937" name="items[]" id="checkbox360" class="checkbox" /><label for="checkbox360"></label></td>
<td><a href="/mcsa-sql-server-2012-certification-course-training">MCSA SQL Server 2012 Certification</a>(KK-305)</td>
<td style="text-align: center;">15</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1746);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1337">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="905" name="items[]" id="checkbox361" class="checkbox" /><label for="checkbox361"></label></td>
<td><a href="/magento-development-course-training">Magento development</a>(KK-305)</td>
<td style="text-align: center;">6</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1758);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1349">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="905" name="items[]" id="checkbox362" class="checkbox" /><label for="checkbox362"></label></td>
<td><a href="/magento-development-course-training">Magento development</a>(KK-305)</td>
<td style="text-align: center;">3</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>04 Aug 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1759);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1350">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="817" name="items[]" id="checkbox363" class="checkbox" /><label for="checkbox363"></label></td>
<td><a href="/oracle-java-se7-programming-ocp-certification-course-training">Java SE7 Programming</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>1</td>
<td>29 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1290);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/901">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox364" class="checkbox" /><label for="checkbox364"></label></td>
<td><a href="/advanced-php-course-training">Advanced PHP</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>2</td>
<td>29 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1740);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1334">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="843" name="items[]" id="checkbox365" class="checkbox" /><label for="checkbox365"></label></td>
<td><a href="/security-certification-course-training">Security+ Certification</a>(KK-302)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>1</td>
<td>15 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1516);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1120">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="816" name="items[]" id="checkbox366" class="checkbox" /><label for="checkbox366"></label></td>
<td><a href="/oracle-java-se7-fundamentals-oca-certification-course-training">Java SE7 Fundamentals</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>1</td>
<td>14 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1287);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/898">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="931" name="items[]" id="checkbox367" class="checkbox" /><label for="checkbox367"></label></td>
<td><a href="/axure-rp-course-training">Axure RP</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>1</td>
<td>08 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1647);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1249">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="811" name="items[]" id="checkbox368" class="checkbox" /><label for="checkbox368"></label></td>
<td><a href="/android-programming-course-training">Android Programming</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>10</td>
<td>08 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1724);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1319">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox369" class="checkbox" /><label for="checkbox369"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>07 Jul 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1725);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1321">View</a></td><td style="text-align: center;"><a href="javascript:;" style="color:#ccc">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="891" name="items[]" id="checkbox370" class="checkbox" /><label for="checkbox370"></label></td>
<td><a href="/advanced-php-course-training">Advanced PHP</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>1</td>
<td>24 Jun 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1714);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1227">View</a></td><td style="text-align: center;"><a href="/course/reviews/1227">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="9" name="items[]" id="checkbox371" class="checkbox" /><label for="checkbox371"></label></td>
<td><a href="/iphone-app-development-course-training">iPhone App Development</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>17 Jun 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1495);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/586">View</a></td><td style="text-align: center;"><a href="/course/reviews/586">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="828" name="items[]" id="checkbox372" class="checkbox" /><label for="checkbox372"></label></td>
<td><a href="/joomla-course-training">Joomla!</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>11 Jun 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1215);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/791">View</a></td><td style="text-align: center;"><a href="/course/reviews/791">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="903" name="items[]" id="checkbox373" class="checkbox" /><label for="checkbox373"></label></td>
<td><a href="/salesforce-course-training">Salesforce</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>09 Jun 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1699);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1296">View</a></td><td style="text-align: center;"><a href="/course/reviews/1296">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox374" class="checkbox" /><label for="checkbox374"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>02 Jun 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1279);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/888">View</a></td><td style="text-align: center;"><a href="/course/reviews/888">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox375" class="checkbox" /><label for="checkbox375"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a></td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>26 May 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1212);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/788">View</a></td><td style="text-align: center;"><a href="/course/reviews/788">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="1" name="items[]" id="checkbox376" class="checkbox" /><label for="checkbox376"></label></td>
<td><a href="/web-design-course-training">Web Design (HTML & CSS)</a>(KK-303)</td>
<td style="text-align: center;">8</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>20 May 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1653);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/683">View</a></td><td style="text-align: center;"><a href="/course/reviews/683">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="3" name="items[]" id="checkbox377" class="checkbox" /><label for="checkbox377"></label></td>
<td><a href="/php-course-training">PHP</a>(KK-303)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>04 May 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1654);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/621">View</a></td><td style="text-align: center;"><a href="/course/reviews/621">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox378" class="checkbox" /><label for="checkbox378"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>29 Apr 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1280);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/889">View</a></td><td style="text-align: center;"><a href="/course/reviews/889">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="878" name="items[]" id="checkbox379" class="checkbox" /><label for="checkbox379"></label></td>
<td><a href="/c-sharp-dot-net-course-training">C#</a>(KK-305)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>24 Mar 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1422);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1037">View</a></td><td style="text-align: center;"><a href="/course/reviews/1037">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="846" name="items[]" id="checkbox380" class="checkbox" /><label for="checkbox380"></label></td>
<td><a href="/ipad-app-development-course-training">iPad app development</a>(KK-301)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>25 Feb 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1419);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/931">View</a></td><td style="text-align: center;"><a href="/course/reviews/931">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="906" name="items[]" id="checkbox381" class="checkbox" /><label for="checkbox381"></label></td>
<td><a href="/magento-theme-design-course-training">Magento Theme Design</a>(KK-304)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>18 Feb 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1393);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/1005">View</a></td><td style="text-align: center;"><a href="/course/reviews/1005">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="82" name="items[]" id="checkbox382" class="checkbox" /><label for="checkbox382"></label></td>
<td><a href="/digital-marketing-course-training">Digital Marketing</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>28 Jan 2013</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 1278);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/887">View</a></td><td style="text-align: center;"><a href="/course/reviews/887">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="8" name="items[]" id="checkbox383" class="checkbox" /><label for="checkbox383"></label></td>
<td><a href="/jquery-javascript-course-training">jQuery & JavaScript</a>(KK-303)</td>
<td style="text-align: center;">2</td>
<td>10:00 - 17:00 Mon, Tue</td>
<td>0</td>
<td>17 Dec 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 970);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/530">View</a></td><td style="text-align: center;"><a href="/course/reviews/530">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="818" name="items[]" id="checkbox384" class="checkbox" /><label for="checkbox384"></label></td>
<td><a href="/mobile-web-design-course-training">Mobile Web Design</a>(KK-301)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Fri</td>
<td>0</td>
<td>16 Nov 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 951);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/400">View</a></td><td style="text-align: center;"><a href="/course/reviews/400">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox385" class="checkbox" /><label for="checkbox385"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Tue, Fri</td>
<td>0</td>
<td>13 Nov 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 883);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/522">View</a></td><td style="text-align: center;"><a href="/course/reviews/522">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox386" class="checkbox" /><label for="checkbox386"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-305)</td>
<td style="text-align: center;">10</td>
<td>18:30 - 21:30 Mon, Thu</td>
<td>0</td>
<td>01 Nov 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 879);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/520">View</a></td><td style="text-align: center;"><a href="/course/reviews/520">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="9" name="items[]" id="checkbox387" class="checkbox" /><label for="checkbox387"></label></td>
<td><a href="/iphone-app-development-course-training">iPhone App Development</a>(KK-305)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>07 Oct 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 945);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/335">View</a></td><td style="text-align: center;"><a href="/course/reviews/335">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="821" name="items[]" id="checkbox388" class="checkbox" /><label for="checkbox388"></label></td>
<td><a href="/maxon-cinema-4d-course-training">Maxon Cinema 4d</a>(KK-304)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>30 Sep 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 986);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/518">View</a></td><td style="text-align: center;"><a href="/course/reviews/518">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox389" class="checkbox" /><label for="checkbox389"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-302)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>24 Sep 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 920);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/440">View</a></td><td style="text-align: center;"><a href="/course/reviews/440">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox390" class="checkbox" /><label for="checkbox390"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>17 Sep 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 884);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/523">View</a></td><td style="text-align: center;"><a href="/course/reviews/523">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="818" name="items[]" id="checkbox391" class="checkbox" /><label for="checkbox391"></label></td>
<td><a href="/mobile-web-design-course-training">Mobile Web Design</a>(KK-301)</td>
<td style="text-align: center;">1</td>
<td>10:00 - 17:00 Sat</td>
<td>0</td>
<td>01 Sep 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 949);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/398">View</a></td><td style="text-align: center;"><a href="/course/reviews/398">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="55" name="items[]" id="checkbox392" class="checkbox" /><label for="checkbox392"></label></td>
<td><a href="/ruby-on-rails-course-training">Ruby on Rails</a>(KK-302)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>13 Aug 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 855);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/499">View</a></td><td style="text-align: center;"><a href="/course/reviews/499">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="63" name="items[]" id="checkbox393" class="checkbox" /><label for="checkbox393"></label></td>
<td><a href="/dreamweaver-course-training">Adobe Dreamweaver</a>(KK-301)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>13 Aug 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 886);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/456">View</a></td><td style="text-align: center;"><a href="/course/reviews/456">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox394" class="checkbox" /><label for="checkbox394"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-305)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Sun</td>
<td>0</td>
<td>05 Aug 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 882);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/521">View</a></td><td style="text-align: center;"><a href="/course/reviews/521">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="824" name="items[]" id="checkbox395" class="checkbox" /><label for="checkbox395"></label></td>
<td><a href="/final-cut-pro-course-training">Final Cut Pro</a>(KK-301)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>16 Jul 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 856);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/410">View</a></td><td style="text-align: center;"><a href="/course/reviews/410">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="51" name="items[]" id="checkbox396" class="checkbox" /><label for="checkbox396"></label></td>
<td><a href="/maya-course-training">Maya</a>(KK-304)</td>
<td style="text-align: center;">5</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu, Fri</td>
<td>0</td>
<td>25 Jun 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 871);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/414">View</a></td><td style="text-align: center;"><a href="/course/reviews/414">View</a></td><input type="hidden" name="page" value="courses" />
</tr><tr>
<td class="check"><input type="checkbox" value ="19" name="items[]" id="checkbox397" class="checkbox" /><label for="checkbox397"></label></td>
<td><a href="/html5-css3-course-training">HTML5 & CSS3</a>(KK-301)</td>
<td style="text-align: center;">4</td>
<td>10:00 - 17:00 Mon, Tue, Wed, Thu</td>
<td>0</td>
<td>18 Jun 2012</td>
<td style="text-align: center;"> <select name="available" onchange="getStatus(this.value, 830);" style="font-size:12px"> 
        <option value="Not_Decided">Not Decided</option>
        <option value="Available">Available</option>
        <option value="Not_Available" >Not Available</option>
    </select></td>
<td><a href="/course/bookings/390">View</a></td><td style="text-align: center;"><a href="/course/reviews/390">View</a></td><input type="hidden" name="page" value="courses" />
</tr>
</tbody>

</table>
</form>
<?php include('includes/trainer-sidebar.php');