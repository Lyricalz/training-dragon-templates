<!DOCTYPE HTML>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js" lang="en"> <![endif]-->
<html class="no-js" lang="en" dir="ltr">
<?php include 'includes/meta.php'; ?>
<body id="<?= $content; ?>">
    <?php include 'includes/header.php'; ?>
    <?php
    if ($content == 'home') :
        include 'pages/home.php';
    else :
        ?>
        <div id="main" class="">
            <section id="contentWrap" class="container">
                <div class="gridRow">

                    <?php if ($sidebar) : ?>
                        <div id="content">
                            <?php $this->load->view('pages/'.$content); ?>
                        </div>
                        <?php
                        $this->load->view('includes/sidebar');
                    else :
                        ?>
                        <div id="content" class="fullWidth">
                            <?php $this->load->view('pages/'.$content); ?>
                        </div>
                    <?php
                    endif;
                endif;
                ?>
            </div>
        </section>
    </div>
    <?php include 'includes/footer.php'; ?>
    <div class="test" style="position: fixed; top: 200px; left: 10px;"></div>
    <?php include 'includes/footer-meta.php'; ?>
</body>
</html>