<?php

class Home extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->output->set_header('Content-type: text/html; charset=utf-8');
    }
    
    public function index($page = '', $sidebar = 'yes') {
        $data['content'] = empty($page) ? 'home' : $page;
        $data['sidebar'] = $sidebar == 'no' ? FALSE : TRUE;
        
        $this->load->view('index', $data);
    }
    
}