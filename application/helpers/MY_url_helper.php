<?php

function assets_url() {
    $CI = & get_instance();
    return $CI->config->item('assets_url');
}

function getVer($url) {
    
    if(ENVIRONMENT == 'local-dev') :
        return $url;
    endif;

    $file = $_SERVER['DOCUMENT_ROOT'] . '/static/trainingdragon/assets/' . $url;
    $final = filemtime($file);

    $test = substr($url, -2, 3);
    if ($test == 'ss') :
        return str_replace('.css', '-' . $final . '.css', $url);
    elseif ($test == 'js') :
        return str_replace('.js', '-' . $final . '.js', $url);
    endif;
}